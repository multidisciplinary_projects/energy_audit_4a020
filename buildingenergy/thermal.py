"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module transform a RC graphical representation into a state space model.
"""

import enum
from typing import Any, Dict, List, Tuple

import matplotlib.pyplot as plt
import networkx
import numpy
import numpy.linalg
import scipy.linalg
from pymor.models.iosys import LTIModel
from pymor.reductors.bt import BTReductor

STATE_MODEL = Dict[str, Any]

class CAUSALITY(enum.Enum):
    """
    Tags used for labeling causality of a node variable.

    :param enum: input (IN), used to say a variable is known
    :type enum: str
    """
    IN =  "IN"
    UNDEF = "-"
    OUT = "OUT"


def concatenate(*lists):
    """ Combine tuples of lists into a single list without duplicated values.

    :param lists: a tuple [of tuple of tuple] of list of elements
    :type lists: a tuple [of tuple of tuple] of list of elements
    :return: a concatenated list of elements
    :rtype: list (of str usually)
    """
    if lists is None:
        return list()
    while type(lists[0]) == tuple:
        lists = lists[0]
    returned_list = list()
    for a_list in lists:
        for element in a_list:
            if element not in returned_list:
                returned_list.append(element)
    return returned_list


def clean(selection_matrix: numpy.matrix) -> numpy.matrix:
    """
    Remove row full of zeros in a selection matrix (one value per row and at most one value per column).

    :param selection_matrix: a selection matrix
    :type selection_matrix: numpy.matrix
    :raises ValueError: triggered if the matrix given as argument is not a selection matrix
    :return: the provided selection but without rows full of zeros
    :rtype: Matrix
    """
    #selection_matrix: numpy.matrix = selection_matrix.copy()

    if selection_matrix.shape[0] > selection_matrix.shape[1]:
        raise ValueError('bar only operates on row matrices')
    i: int = 0
    while i < selection_matrix.shape[0]:
        number_of_non_null_elements: int = 0
        for j in range(selection_matrix.shape[1]):
            if selection_matrix[i,j] != 0:
                number_of_non_null_elements += 1
        if number_of_non_null_elements == 0:
            #selection_matrix.row_del(i)
            selection_matrix = numpy.delete(selection_matrix, i, axis=0)
        elif number_of_non_null_elements > 1:
            raise ValueError('bar only operates on selection matrices')
        else:
            i += 1
    return selection_matrix


def bar(matrix: numpy.matrix) -> numpy.matrix:
    """
    Compute the complementary matrix to the provided row matrix i.e bar_M such as [[M], [bar_M]] is invertible, bar_M.T * bar_M = I and M.T * bar_M = 0.

    A specific fast computation approach is used if M is a selection matrix.

    :param matrix: a row matrix, possibly a selection Matrix (one value per row and at most one value per column)
    :type matrix: Matrix
    :return: a complementary matrix
    :rtype: sympy.Matrix
    """
    try:
        i: int = 0
        while i < matrix.shape[0]:
            number_of_non_null_elements: int = 0
            for j in range(matrix.shape[1]):
                if matrix[i,j] != 0:
                    number_of_non_null_elements += 1
            if number_of_non_null_elements == 0:
                #matrix.row_del(i)
                matrix: numpy.matrix = numpy.delete(matrix, i, axis=0)
            elif number_of_non_null_elements > 1:
                pass
            else:
                i += 1
        n_rows, n_columns = matrix.shape
        complement_matrix: numpy.matrix = numpy.matrix(numpy.zeros((n_columns - n_rows, n_columns)))
        k: int = 0
        for i in range(n_columns):
            found: bool = False
            j: int =0
            while (not found) and j < n_rows:
                if matrix[j,i] != 0:
                    found = True
                j += 1
            if not found:
                complement_matrix[k,i] = 1
                k += 1
    except:
        #vectors = (matrix.T).nullspace()
        vectors: numpy.matrix = numpy.matrix(scipy.linalg.null_space(matrix.T))
        complement_matrix = vectors[0]
        for i in range(1, len(vectors)):
            #complement_matrix = complement_matrix.row_join(vectors[i])
            complement_matrix = numpy.concatenate((complement_matrix, vectors[i]), axis=1)
        complement_matrix = complement_matrix.T
    return complement_matrix


def pinv(matrix: numpy.matrix) -> numpy.matrix:
    """
    compute pseudo-inverse of a full column rank matrix

    :param matrix: a full column rank matrix
    :type matrix: sympy.Matrix
    :return: the pseudo inverse computed as (matrix*matrix.T).inv() * matrix
    :rtype: numpy.matrix
    """
    #return (matrix.T * matrix).inv() * matrix.T
    return numpy.linalg.pinv(matrix)


class NODE_TYPE(enum.Enum):
    """Tags used for labeling types of node variables .

    :param enum: heat flow or temperature
    :type enum: str
    """
    HEAT = "heat"
    TEMPERATURE = "temperature"


class ELEMENT_TYPE(enum.Enum):
    """
    Tags used for labeling of element corresponding to an edge.

    :param enum: thermal resistance (Rth), thermal capacitance (Cth) or heat source (heat)
    :type enum: str
    """
    R = "Rth"
    C = "Cth"
    P = "heat"


class AbstractThermalNetwork(networkx.DiGraph):
    """
    Digraph representing a thermal model, with nodes standing for variables (temperatures or heat flows) and edges for elements like thermal resistance, thermal capacitance and heat sources. The aim of this class is to generate state space model corresponding to a thermal network.

    This is the abstract class specialized in ThermalNetwork

    :param networkx: superclass digraph for networkx
    :type networkx: networkx.DiGraph
    """

    def __init__(self) -> None:
        """Initialize an empty thermal network
        """
        super().__init__(directed=True)
        self.Tref = self.T('TREF', CAUSALITY.IN)
        self.R_counter=0
        self.C_counter=0
        self.P_counter=0
        self.T_counter=1
        self.__select_elements = dict()
        self.__temperatures_in, self.__temperatures_state, self.__temperatures_remaining = None, None, None

    def _edge_attr_str_vals(self, edge: Tuple[str, str]):
        """Convert edge attributes into a printable string

        :param edge: edge whose attributes have to be converted
        :type node: (str, str)
        :return: a string representing the edge attributes with values
        :rtype: str
        """
        _attr_vals = list()
        for attr_name in self.edges[edge]:
            _attr_value = self.edges[edge][attr_name]
            if type(_attr_value) == str:
                _attr_vals.append(_attr_value)
            elif isinstance(_attr_value, ELEMENT_TYPE):
                _attr_vals.append(_attr_value.value)
        return _attr_vals

    def _node_attr_str_vals(self, node: str):
        """Convert node attributes into a printable string

        :param node: node whose attributes have to be converted
        :type node: str
        :return: a string representing the node attributes with values
        :rtype: str
        """
        _attr_vals = list()
        for attr_name in self.nodes[node]:
            _attr_value = self.nodes[node][attr_name]
            if type(_attr_value) == str:
                _attr_vals.append(_attr_value)
            elif isinstance(_attr_value, NODE_TYPE) or isinstance(_attr_value, CAUSALITY):
                _attr_vals.append(_attr_value.value)
        return _attr_vals

    def T(self, name: str=None, causality: str=CAUSALITY.UNDEF):
        """Create a temperature node.

        :param name: temperature node name (should conventionally start by T), defaults to None with name generated automatically
        :type name: str, optional
        :param causality: type of causality, defaults to NODE_CAUSALITY.UNDEF
        :type causality: NODE_CAUSALITY, optional
        :return: name of the temperature node
        :rtype: str
        """
        if name is None:
            name: str = 'T' + str(self.T_counter)
            self.T_counter += 1
        if name == 'TREF':
            symbol: int=0
        else:
            symbol=name
        self.add_node(name, causality=causality, ntype=NODE_TYPE.TEMPERATURE, symbol=symbol)
        return name

    def HEAT(self, T: str,  name: str=None):
        """Create a heat source node.

        :param T: temperature node where the heat source will inject its power
        :type T: str
        :param name: name of the heat source (should conventionally start by P), defaults to None with name generated automatically
        :type name: str, optional
        :return: name of the heat source node
        :rtype: str
        """
        if name is None:
            name = 'P'+str(self.P_counter)
            self.P_counter += 1
        self.add_node(name, causality=CAUSALITY.IN, ntype=NODE_TYPE.HEAT, symbol=name)
        self.add_edge(name, T, flow=name, element=ELEMENT_TYPE.P, symbol=name)
        return name

    def R(self, fromT: str=None, toT: str=None, name: str=None, heat_flow_name: str=None, value: float = None):
        """Create a resistance edge element between 2 temperature nodes, directed by the conventional heat flow.

        :param fromT: name of the temperature node where heat flow starts, defaults to None (0 reference temperature)
        :type fromT: str, optional
        :param toT: name of the temperature node where heat flow ends, defaults to None (0 reference temperature)
        :type toT: str, optional
        :param resistance_name: name of the thermal resistance, defaults to None with name generated automatically (RXXX)
        :type resistance_name: str, optional
        :param heat_flow_name: name of the heat_flow passing through thermal resistance, defaults to None with name generated automatically (PXXX)
        :type heat_flow_name: str, optional
        :raises ValueError: fromT and toT cannot be set to None simultaneously
        :return: thermal resistance name and heat flow name
        :rtype: tuple(str, str)
        """
        if fromT is None and toT is None:
            raise ValueError('At least one temperature must not be None')
        elif fromT is None:
            fromT: str = self.Tref
        elif fromT not in self.nodes:
            self.T(name=fromT)
        elif toT is None:
            toT: str = self.Tref
        elif toT not in self.nodes:
            self.T(name=toT)
        if name is None:
            if self.R_counter == 53:
                pass
            name: str = 'R'+str(self.R_counter)
            self.R_counter += 1
        if heat_flow_name is None:
            heat_flow_name: str = 'P'+str(self.P_counter)
        self.P_counter += 1
        if value is not None:
            self.add_edge(fromT, toT, name=name, flow=heat_flow_name, element=ELEMENT_TYPE.R, symbol=value)
        return name, heat_flow_name

    def C(self, toT: str, name: str=None, heat_flow_name: str=None, value: float=None):
        """Create a thermal capacitance edge element between 0 reference temperature and a temperature node, directed by the conventional heat flow to the specified temperature node.

        :param T: name of the temperature node where heat flow ends, defaults to None (0 reference temperature)
        :type toT: str, optional
        :param capacitance_name: name of the thermal resistance, defaults to None with name generated automatically (CXXX)
        :type capacitance_name: str, optional
        :param heat_flow_name: name of the heat_flow passing through thermal resistance, defaults to None with name generated automatically (PXXX)
        :type heat_flow_name: str, optional
        :return: thermal resistance name and heat flow name
        :rtype: tuple(str, str)
        """
        if toT not in self.nodes:
            self.T(name=toT)
        if name is None:
            name: str = 'C'+str(self.C_counter)
            self.C_counter += 1
        if heat_flow_name is None:
            heat_flow_name: str = 'P'+str(self.P_counter)
        self.P_counter += 1
        self.add_edge(self.Tref, toT, name=name, flow=heat_flow_name, element=ELEMENT_TYPE.C, symbol=float(value))
        return name, heat_flow_name

    def _select_elements(self, element_type: ELEMENT_TYPE=None):
        """Return a list of a specified type of elements

        :param element_type: type of elements ELEMENT_TYPE (ELEMENT_TYPE.R for resistance, ELEMENT_TYPE.C for capacitance and ELEMENT_TYPE.P for heat source), defaults to None for all edge elements
        :type element_type: ELEMENT_TYPE, optional
        :return: list of requested elements
        :rtype: list of Netwoekx edges i.e. list of tuples (str, str)
        **SLOW**
        """
        if element_type is None:
            return self.edges
        else:
            if element_type not in self.__select_elements:
                self.__select_elements[element_type] = list(filter(lambda edge: edge is not None and self.edges[edge]['element'] == element_type, self.edges))
            return self.__select_elements[element_type]

    def _select_nodes(self, node_type: NODE_TYPE, node_causality: CAUSALITY=None):
        """Return a list of a specified type of nodes

        :param node_type: type of nodes (NODE_TYPE.TEMPERATURE for temperature, NODE_TYPE.HEAT for heat source), defaults to None for all node types
        :type node_type: NODE_TYPE
        :param node_causality: causality of nodes (NODE_CAUSALITY.IN for input, NODE_CAUSALITY.OUT for output and NODE_CAUSALITY.UNDEF for intermediate), defaults to None for all causalities
        :type node_causality: NODE_CAUSALITY
        :return: list of requested nodes
        :rtype: list of Netwoekx nodes i.e. list of str
        **SLOW**
        """
        if node_type is None and node_causality is None:
            return  self.nodes
        elif node_type is None:
            return list(filter(lambda node: self.nodes[node]['causality'] == node_causality, self.nodes))
        elif node_causality is None:
            return list(filter(lambda node: self.nodes[node]['ntype'] == node_type, self.nodes))
        else:
            l1 = list(filter(lambda node: self.nodes[node]['ntype'] == node_type, self.nodes))
            l2 = list(filter(lambda node:self.nodes[node]['causality'] == node_causality, self.nodes))
            l3 = list(filter(lambda node: self.nodes[node]['ntype'] == node_type and self.nodes[node]['causality'] == node_causality, self.nodes))
            return list(filter(lambda node: self.nodes[node]['ntype'] == node_type and self.nodes[node]['causality'] == node_causality, self.nodes))

    def _causal_temperatures(self) -> tuple:
        """ Return a tuple of lists of categorized temperature names
        :return: input temperatures, temperatures appearing as derivative, intermediate temperatures and output temperatures
        :rtype: tuple of lists of str
        """
        if self.__temperatures_in is None:    
            temperatures_in = self._select_nodes(NODE_TYPE.TEMPERATURE, CAUSALITY.IN)
            temperatures_state = list()
            for Cedge in self._select_elements(ELEMENT_TYPE.C):
                if Cedge[0] != "TREF" and Cedge[0] not in temperatures_state:
                    temperatures_state.append(Cedge[0])
                if Cedge[1] != "TREF" and Cedge[1] not in temperatures_state:
                    temperatures_state.append(Cedge[1])
            temperatures_remaining = list()
            for temperature in self._select_nodes(NODE_TYPE.TEMPERATURE):
                if temperature not in temperatures_in and temperature not in temperatures_state:
                    temperatures_remaining.append(temperature)
            self.__temperatures_in, self.__temperatures_state, self.__temperatures_remaining = temperatures_in[1:], temperatures_state, temperatures_remaining
        return self.__temperatures_in[1:], self.__temperatures_state, self.__temperatures_remaining

    @property
    def temperatures_out(self) -> Tuple[str]:
        """
        Return the names of temperatures tagged with Causality.OUT. These variables are obtained either from the state vector, or from a combination of input variables and variables from state vector

        :return: temperature names requested for being simulated
        :rtype: Tuple[str]
        """
        return self._select_nodes(NODE_TYPE.TEMPERATURE, CAUSALITY.OUT)

    @property
    def all_temperatures(self):
        """
        Return the list of all the variables representing temperatures

        :return: list of temperature names
        :rtype: List[str]
        """
        return concatenate(*self._causal_temperatures())

    def _typed_heatflows(self):
        """Return the list of edge element heatflows by type. It returns 3 vectors of heat flows:
        - heatflows_R: the list of heatflows going through thermal resistances
        - heatflows_C: the list of heatflows going through thermal capacitances
        - heatflows_P: the list of heatflows sources

        :return: list of edge element heatflows by type: edges corresponding to resistance, then those corresponding to capacitance and finally those corresponding to power heatflows
        :rtype: Tuple[List[(str, str)]]
        """
        heatflows_R = [self.edges[edge]['flow'] for edge in self._select_elements(ELEMENT_TYPE.R)]
        heatflows_C = [self.edges[edge]['flow'] for edge in self._select_elements(ELEMENT_TYPE.C)]
        heatflows_P = [self.edges[edge]['flow'] for edge in self._select_elements(ELEMENT_TYPE.P)]
        return heatflows_R, heatflows_C, heatflows_P

    @property
    def heatflows_sources(self):
        """
        Return the list of heatflow sources

        :return: heatflow sources
        :rtype: List[str]
        """
        _, _, heatflows_P = self._typed_heatflows()
        return heatflows_P

    def _number_of_elements(self, type: ELEMENT_TYPE) -> int:
        """number of elements of the specidied type in the thermal network

        :param type: type of elements to be counted
        :type type: ELEMENT_TYPE
        :return: number of edges tags as resistances
        :rtype: int
        """
        return len(self._select_elements(type))

class ThermalNetwork(AbstractThermalNetwork):
    """
    Specialized main class for transforming RC graphs into State Space model

    :param AbstractThermalNetwork: abstract super class
    """

    def __init__(self) -> None:
        """
        See superclass AbstractThermalNetwork.
        """
        super().__init__()

    def _matrices_MR_MC(self) -> Tuple[numpy.matrix, numpy.matrix]:
        """
        incidence matrices (-1 for starting flow and +1 for ending) leading to delta temperatures for R and C elements from temperatures
        :return: matrices MR and MC
        :rtype: sympy.Matrix, sympy.Matrix
        """
        Redges = self._select_elements(ELEMENT_TYPE.R)
        Cedges = self._select_elements(ELEMENT_TYPE.C)
        MR: numpy.matrix = numpy.matrix(numpy.zeros((self._number_of_elements(ELEMENT_TYPE.R), len(self.all_temperatures))))
        MC: numpy.matrix = numpy.matrix(numpy.zeros((self._number_of_elements(ELEMENT_TYPE.C), len(self.all_temperatures))))
        components = []
        for i, Redge in enumerate(Redges):
            components.append(self.edges[Redge]['name'])
            for j,node in enumerate(Redge):
                if node in self.all_temperatures:
                    MR[i, self.all_temperatures.index(node)] = 1 - 2 * j
        for i, Cedge in enumerate(Cedges):
            components.append(self.edges[Cedge]['name'])
            for j,node in enumerate(Cedge):
                if node in self.all_temperatures:
                    MC[i, self.all_temperatures.index(node)] = 1 - 2* j
        return MR, MC

    def _matrix_R(self) -> numpy.matrix:
        """ Return a diagonal matrix with thermal resistance coefficient in the diagonal.

        :return: diagonal matrix R
        :rtype: sympy.matrix
        """
        Redges = self._select_elements(ELEMENT_TYPE.R)
        R_heat_flows = self._typed_heatflows()[0]
        matrix_R: numpy.matrix = numpy.matrix(numpy.zeros((len(R_heat_flows),len(R_heat_flows))))
        for Redge in Redges:
            i: int = R_heat_flows.index(self.edges[Redge]['flow'])
            matrix_R[i, i] = self.edges[Redge]['symbol']
        return matrix_R

    def _matrix_C(self) -> numpy.matrix:
        """" Return a diagonal matrix with thermal capacitance coefficient in the diagonal.

        :return: diagonal matrix C
        :rtype: sympy.matrix
        """
        Cedges = self._select_elements(ELEMENT_TYPE.C)
        C_heat_flows = self._typed_heatflows()[1]
        matrix_C: numpy.matrix = numpy.matrix(numpy.zeros((len(C_heat_flows),len(C_heat_flows))))
        for Cedge in Cedges:
            i: int = C_heat_flows.index(self.edges[Cedge]['flow'])
            matrix_C[i, i] = self.edges[Cedge]['symbol']
        return matrix_C

    @property
    def _matrices_Gamma_RCP(self) -> Tuple[numpy.matrix, numpy.matrix, numpy.matrix]:
        """ Return heatflow balances at each node as a set of 3 incidence (-1: node leaving flow and 1: node entering flow) matrices:

        heatflows_balance_R_matrix * R_heatflows + heatflows_balance_C_matrix * C_heatflows + heatflows_balance_P_matrix * P_heatflows = 0

        :return: matrices heatflows_balance_R_matrix (GammaR), heatflows_balance_C_matrix (GammaC), heatflows_balance_P_matrix (GammaP)
        :rtype: sympy.Matrix, sympy.Matrix, sympy.Matrix
        """
        heatflows_R, heatflows_C, heatflows_P = self._typed_heatflows()
        heatflows_R_matrix = list()
        heatflows_C_matrix = list()
        heatflows_P_matrix = list()
        number_of_heat_balances = 0
        for node in concatenate(self._causal_temperatures()):
            flows_in = [self.edges[edge]['flow'] for edge in self.in_edges(nbunch=node)]
            flows_out = [self.edges[edge]['flow'] for edge in self.out_edges(nbunch=node)]
            if node != 'TREF'  and len(flows_in) + len(flows_out) > 1:
                heatflows_R_matrix.append([0 for i in range(len(heatflows_R))])
                heatflows_C_matrix.append([0 for i in range(len(heatflows_C))])
                heatflows_P_matrix.append([0 for i in range(len(heatflows_P))])
                for flow in concatenate(flows_in, flows_out):
                    incidence = -1 if flow in flows_out else 1
                    flow_index = self.all_heatflows.index(flow)
                    if flow_index < len(heatflows_R):
                        heatflows_R_matrix[number_of_heat_balances][flow_index] = incidence
                    elif flow_index < len(heatflows_R) + len(heatflows_C):
                        heatflows_C_matrix[number_of_heat_balances][flow_index - len(heatflows_R)] = incidence
                    else:
                        heatflows_P_matrix[number_of_heat_balances][flow_index - len(heatflows_R) - len(heatflows_C)] = - incidence
                number_of_heat_balances += 1
        return numpy.matrix(heatflows_R_matrix), numpy.matrix(heatflows_C_matrix), numpy.matrix(heatflows_P_matrix)

    def _temperatures_selection_matrix(self, selected_temperatures: List[str], all_temperatures: List[str]=None):
        """Create a selection matrix for the temperature provided as inputs and considering all the temperature nodes

        :param selected_temperatures: list of temperatures for which the selection matrix is computer
        :type selected_temperatures: List[str]
        :return: the selection matrix
        :rtype: scipy.Matrix
        """
        if all_temperatures is None:
            all_temperatures: List[str] = self.all_temperatures
        selection_matrix: numpy.matrix = numpy.matrix(numpy.zeros((len(selected_temperatures), len(all_temperatures))))
        for i, temperature in enumerate(selected_temperatures):
            selection_matrix[i, all_temperatures.index(temperature)] = 1
        return selection_matrix

    def _Sselect(self) -> numpy.matrix:
        """
        Generate a selection matrix to get from all the temperatures except the input and the state ones, the ones that has been tagged with CAUSALITY.OUT

        :return: a selection matrix with 0 or 1 insides
        :rtype: Matrix
        """
        if self.temperatures_select is None or len(self.temperatures_select) == 0:
            selection_matrix: numpy.matrix = numpy.matrix(numpy.eye(len(self.temperatures_remaining)))
        else:
            selection_matrix: numpy.matrix = numpy.matrix(numpy.zeros((len(self.temperatures_select), len(self.temperatures_remaining))))
            for i, temperature in enumerate(self.temperatures_select):
                if temperature in  self.temperatures_remaining:
                    selection_matrix[i, self.temperatures_remaining.index(temperature)] = 1
        return selection_matrix

    @property
    def all_heatflows(self):
        """
        List 3 vectors of heat flows:
        - heatflows_R: the list of heatflows going to thermal resistances
        - heatflows_C: the list of heatflows going to thermal capacitances
        - heatflows_P: the list of heatflows sources

        :return: _description_
        :rtype: List[str,
        """
        heatflows_R, heatflows_C, P_heatflows = self._typed_heatflows()
        return concatenate(heatflows_R, heatflows_C, P_heatflows)

    def _heatflows_selection_matrix(self, selected_heat_flows: List[str]):
        """
        Internally used to generate a selection matrix for extracting some heatflows .

        :param selected_heat_flows: the heatflows that will correspond to the selection matrix
        :type selected_heat_flows: List[str]
        :return: a selection matrix S such as:

            selected_heat_flows = S * all_heatflows
        :rtype: List[str]
        """
        selection_matrix: numpy.matrix = numpy.matrix(numpy.zeros((len(selected_heat_flows), len(self.all_heatflows))))
        for i, heat_flow in enumerate(selected_heat_flows):
            selection_matrix[i, self.all_heatflows.index(heat_flow)] = 1
        return selection_matrix

    def _Sin(self):
        """
        Generate a selection matrix corresponding to the known temperatures temperatures_in used as inputs.

        :return: a full row rank selection Matrix that can be used to extract the known value variables i.e. variables in the vector temperatures_in
        :rtype: sympy.Matrix
        """
        temperature_in, _, _ = self._causal_temperatures()
        return self._temperatures_selection_matrix(temperature_in)

    @property
    def temperatures_in(self):
        """
        Vector containing the list of variables whose value are known

        :return: the known value temperatures
        :rtype: sympy.Matrix
        """
        temperatures_in, _, _ = self._causal_temperatures()
        return temperatures_in

    def _Sstate(self):
        """
        Generate a selection matrix corresponding to the temperatures with derivatives temperatures_state used as state vector.

        :return: a full row rank selection Matrix that can be used to extract the state variables
        :rtype: sympy.Matrix
        """
        _, temperatures_state, _ = self._causal_temperatures()
        Sstate = self._temperatures_selection_matrix(temperatures_state)
        return  Sstate

    @property
    def temperatures_state(self):
        """
        Vector containing the list of state variables

        :return: the temperatures corresponding to the state variables
        :rtype: sympy.Matrix
        """
        _, temperatures_state, _ = self._causal_temperatures()
        return  temperatures_state

    def _Sremaining(self):
        """
        Generate a selection matrix corresponding to the temperatures which are not belonging to the inputs and to the states.

        :return: a full row rank selection Matrix that can be used to extract the temperatures which are not belonging to the inputs and to the states.
        :rtype: sympy.Matrix
        """
        _, _, temperatures_remaining = self._causal_temperatures()
        Sremaining = self._temperatures_selection_matrix(temperatures_remaining)
        return  Sremaining

    def _Sout(self):
        """
        Return a selection matrix leading to output temperature variable from the vector of all the temperature variables

        :return: a selection matrix for extracting temperature variables, which have been tagged with Causality.OUT
        :rtype: Matrix
        """
        return self._temperatures_selection_matrix(self.temperatures_out())

    @property
    def temperatures_remaining(self):
        """
        Vector containing the list of temperatures which are not belonging to the inputs and to the states.

        :return: the temperatures corresponding to the temperatures which are not belonging to the inputs and to the states.
        :rtype: sympy.Matrix
        """
        _, _, temperatures_remaining = self._causal_temperatures()
        return  temperatures_remaining

    @property
    def temperatures_select(self) -> tuple:
        """
        Return the selected temperatures, tagged with CAUSALITY.OUT, to be at the output of the state space output equation except the temperatures belonging to the state ones

        :return: _description_
        :rtype: tuple
        """
        temperatures_sel = list()
        for temperature in self._select_nodes(NODE_TYPE.TEMPERATURE, CAUSALITY.OUT):
            if temperature in self.temperatures_remaining:
                temperatures_sel.append(temperature)
        return temperatures_sel

    def _static__matrices(self):
        """
        Return the matrices of the following linear static model:

        temperatures_out = D_temperatures_in temperatures_in + D_heatflow_sources heatflows_sources

        :return: Matrices D_temperatures_in and D_heatflow_sources
        :rtype: Tuple(sympy.Matrix, sympy.Matrix)
        :raises ValueError: if the graphical model contains capacitance
        """
        if len(self.temperatures_state) != 0:
            raise ValueError('A linear static model can\'t be obtained from a model with capacitance')
        GammaR, _, GammaP = self._matrices_Gamma_RCP
        MR, _ = self._matrices_MR_MC()
        Psi_R = GammaR * self._matrix_R().inv() * MR
        D_temperatures_in = -pinv(Psi_R * self._Sremaining().T) * GammaR * self._matrix_R().inv() * MR * self._Sin().T
        if len(self.heatflows_sources) == 0:
            D_heatflow_sources = GammaP
        else:
            D_heatflow_sources = -pinv(Psi_R * self._Sremaining().T) * GammaP
        return D_temperatures_in, D_heatflow_sources

    @property
    def CDstate_heatflows_matrices(self):
        """
        Return the matrices of the following linear static model:

        temperatures_out = D_temperatures_in temperatures_in + D_heatflow_sources heatflows_sources

        :return: Matrices D_temperatures_in and D_heatflow_sources
        :rtype: Tuple(sympy.Matrix, sympy.Matrix)
        :raises ValueError: if the graphical model contains capacitance
        """
        if len(self.temperatures_state) != 0:
            raise ValueError('A linear static model can\'t be obtained from a model with capacitance')
        GammaR, _, GammaP = self._matrices_Gamma_RCP
        MR, _ = self._matrices_MR_MC()
        Psi_R = numpy.linalg.inv(GammaR * self._matrix_R()) * MR
        D_Tin = numpy.linalg.inv(self._matrix_R()) * MR * ( numpy.eye(len(self.all_temperatures)) - self._Sremaining().T * pinv(Psi_R * self._Sremaining().T ) * Psi_R ) * self._Sin().T
        if len(self.heatflows_sources) == 0:
            D_heat = GammaP
        else:
            D_heat = numpy.linalg.inv(self._matrix_R()) * MR * self._Sremaining().T * pinv(Psi_R * self._Sremaining().T) * GammaP
        return D_Tin, D_heat

    def _diffeq_variables(self):
        """
        Compute internal matrices for state space model

        :return: internal matrices: GammaR, GammaC, GammaP, MR, MC, Psi_R, Psi_C, Phi, Pi
        :rtype: List[float]
        """
        GammaR, GammaC, GammaP = self._matrices_Gamma_RCP
        MR, MC = self._matrices_MR_MC()
        Psi_R = GammaR * numpy.linalg.inv(self._matrix_R()) * MR
        Psi_C = GammaC * self._matrix_C() * MC
        Phi = bar(self._Sstate() * Psi_C.T) * Psi_R * self._Sremaining().T
        Pi = pinv(Psi_C * self._Sstate().T) * ( numpy.eye(Psi_R.shape[0]) - Psi_R * self._Sremaining().T * pinv(Phi) * bar(self._Sstate()*Psi_C.T))
        return GammaR, GammaC, GammaP, MR, MC, Psi_R, Psi_C, Phi, Pi

    def _ABstate_matrices(self):
        """ Compute matrices A, B of the state space model corresponding to the thermal network

        d/dt temperatures_state = A temperatures_state + B_in temperatures_in + B_heat heat_sources

        :return: matrices of the state space representation corresponding to the thermal network with temperature corresponding to capacitance as state variables
        :rtype: List[sympy.matrix]
        """
        if len(self.temperatures_state) == 0:
            raise ValueError('A state space model can only be obtained for a model with capacitance')

        _, _, GammaP, _, _, Psi_R, _, _, Pi = self._diffeq_variables()
        A = - Pi * Psi_R * self._Sstate().T
        B_temperatures_in = - Pi * Psi_R * self._Sin().T
        if len(self.heatflows_sources) == 0:
            B_heatflow_sources = GammaP
        else:
            B_heatflow_sources = Pi * GammaP

        return A, B_temperatures_in, B_heatflow_sources

    def _CDstate_matrices(self):
        """
        Compute matrices C_rem, D_temperatures_in and D_heatflow_sources of the state space model corresponding to the thermal network where output temperature is corresponding to temperatures_in

        :return: matrices C_rem, D_temperatures_in and D_heatflow_source
        :rtype: _type_
        """
        _, _, GammaP, _, _, Psi_R, Psi_C, Phi, _ = self._diffeq_variables()
        C_rem = - pinv(Phi) * bar(self._Sstate() * Psi_C.T) * Psi_R * self._Sstate().T
        D_rem_temperatures_in = - pinv(Phi) * bar(self._Sstate() * Psi_C.T) * Psi_R * self._Sin().T
        if len(self.heatflows_sources) == 0:
            D_rem_heatflow_sources = GammaP
        else:
            D_rem_heatflow_sources = pinv(Phi) * bar(self._Sstate() * Psi_C.T) * GammaP
        if self.temperatures_select is None or self.temperatures_select == 0:
            return C_rem, D_rem_temperatures_in, D_rem_heatflow_sources
        return self._Sselect() * C_rem, self._Sselect() * D_rem_temperatures_in, self._Sselect() * D_rem_heatflow_sources

    @property
    def CDheatflows_matrices(self) -> Tuple[numpy.matrix]:
        """
        Compute matrices C and D leading to the estimations of heatflows going through resistances and capacitances

        :return: Matrices
        :rtype: Tuple[Matrix]
        """
        GammaR, GammaC, GammaP, MR, MC, Psi_R, Psi_C, Phi, Pi = self._diffeq_variables()
        Pi1 = numpy.linalg.inv(self._matrix_R()) * MR * (eye(len(self.all_temperatures)) + self._Sremaining().T * pinv(Phi) * bar(self._Sstate() * Psi_C.T) * Psi_R)
        C_R = Pi1 * self._Sstate().T
        D_R_in = Pi1 * self._Sin().T
        if len(self.heatflows_sources) == 0:
            D_R_varphi_P = GammaP
        else:
            D_R_varphi_P = -numpy.linalg.inv(self._matrix_R()) * MR * self._Sremaining().T * pinv(Phi) * bar(self._Sstate() * Psi_C.T) * GammaP
        Pi2 = - self._matrix_C() * MC * self._Sstate().T * Pi * Psi_R
        C_C = Pi2 * self._Sstate().T
        D_C_in = Pi2 * self._Sin().T
        if len(self.heatflows_sources) == 0:
            D_C_varphi_P = GammaP
        else:
            D_C_varphi_P = self._matrix_C() * MC * self._Sstate().T * Pi * GammaP
        C = numpy.concatenate((C_R, C_C), axis=0)
        D_in = numpy.concatenate((D_R_in, D_C_in), axis=0)
        D_varphi_P = numpy.concatenate((D_R_varphi_P, D_C_varphi_P), axis=0)
        return C, D_in, D_varphi_P

    def draw(self):
        """
        plot the thermal network
        """
        pos=networkx.shell_layout(self)
        node_colors = list()
        for node in self.nodes():
            if node[0] == 'P':
                node_colors.append('yellow')
            elif node == 'TREF':
                node_colors.append('blue')
            else:
                if self.nodes[node]['causality'] == CAUSALITY.IN:
                    node_colors.append('cyan')
                else:
                    node_colors.append('pink')
        edge_colors = list()
        for edge in self.edges():
            if edge in self._select_elements(ELEMENT_TYPE.R):
                edge_colors.append('blue')
            elif edge in self._select_elements(ELEMENT_TYPE.C):
                edge_colors.append('red')
            else:
                edge_colors.append('black')

        plt.figure()
        networkx.draw(self, pos, with_labels=True, edge_color=edge_colors, width=1, linewidths=1, node_size=500, font_size='medium', node_color=node_colors, alpha=1)
        networkx.drawing.draw_networkx_labels(self, pos,  font_size='x-small', verticalalignment='top', labels={node:'\n'+','.join(self._node_attr_str_vals(node)) for node in self.nodes})
        networkx.draw_networkx_edge_labels(self, pos, font_size='x-small', font_color='r', edge_labels={edge:','.join(self._edge_attr_str_vals(edge)) for edge in self.edges})


    def _full_thermal_state_model(self) -> STATE_MODEL:
        """
        Return the matrices of state space model (with capacitance) or the static model (no capacitance)

        :return: list of matrices A, B_Tin, B_heat, C, D_Tin, D_heat with variables Y, X, U_Tin, U_heat. Additionnally, the type 'differential' or 'static' is given depending on wether there is at least one capacitance or not.
        :rtype: Tuple[Matrix, Matrix, Matrix, Matrix, Matrix, Matrix, Matrix, List[float], List[float], List[float], List[float], str]
        """

        temperatures_out_state = list()
        for temperature in self.temperatures_out:
            if temperature in self.temperatures_state:
                temperatures_out_state.append(temperature)
        if len(self.temperatures_state) > 0:  # dynamic model
            X = self.temperatures_state
            A, B_Tin, B_heat = self._ABstate_matrices()
            _Sout: numpy.matrix = self._temperatures_selection_matrix(temperatures_out_state, X)
            C_state: numpy.matrix = _Sout
            D_state_Tin: numpy.matrix = numpy.matrix(numpy.zeros((len(temperatures_out_state), len(self.temperatures_in))))
            D_state_heat: numpy.matrix = numpy.matrix(numpy.zeros((len(temperatures_out_state), len(self.heatflows_sources))))

            C_select, D_select_Tin, D_select_heat = None, None, None
            temperatures_out_select = list()
            for temperature in self.temperatures_out:
                if temperature in self.temperatures_select and temperature not in self.temperatures_state:
                    temperatures_out_select.append(temperature)
            if len(temperatures_out_select) > 0:
                _Sout = self._temperatures_selection_matrix(temperatures_out_select, self.temperatures_select)
                C_select, D_select_Tin, D_select_heat = self._CDstate_matrices()
                C_select = _Sout * C_select
                D_select_Tin = _Sout * D_select_Tin
                D_select_heat = _Sout * D_select_heat

            if C_state is not None and C_select is None:
                state_model = {'A': A, 'B_Tin': B_Tin, 'B_heat': B_heat, 'C': C_state, 'D_Tin': D_state_Tin, 'D_heat': D_state_heat, 'Y': temperatures_out_state, 'X': X, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'differential'}
            elif C_state is None and C_select is not None:
                state_model = {'A': A, 'B_Tin': B_Tin, 'B_heat': B_heat, 'C': C_select, 'D_Tin': D_select_Tin, 'D_heat': D_select_heat, 'Y': temperatures_out_select, 'X': X, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'differential'}
            elif C_state is not None and C_select is not None:
                temperatures_out_state.extend(temperatures_out_select)
                state_model = {'A': A, 'B_Tin': B_Tin, 'B_heat': B_heat, 'C': numpy.concatenate((C_state, C_select), axis=0), 'D_Tin': numpy.concatenate((D_state_Tin, D_select_Tin), axis=0), 'D_heat': numpy.concatenate((D_state_heat, D_select_heat), axis=0), 'Y': temperatures_out_state, 'X': X, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'differential'}
            else:
                C: numpy.matrix = numpy.matrix(numpy.eye(len(self.temperatures_state)))
                D_Tin: numpy.matrix = numpy.matrix(numpy.zeros((len(self.temperatures_state), len(self.temperatures_in))))
                D_heat: numpy.matrix = numpy.matrix(numpy.zeros((len(self.temperatures_state), len(self.heatflows_sources))))
                Y = X
                state_model = {'A': A, 'B_Tin': B_Tin, 'B_heat': B_heat, 'C': C, 'D_Tin': D_Tin, 'D_heat': D_heat, 'Y': Y, 'X': X, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'differential'}
        else:
            D_temperatures_in, D_heatflow_sources = self._static__matrices()
            state_model = {'A': None, 'B_Tin': None, 'B_heat': None, 'C': None, 'D_Tin': D_temperatures_in, 'D_heat':  D_heatflow_sources, 'Y': self.temperatures_remaining, 'X': None, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'static'}
        return state_model

    def state_model(self, order: int=None) -> STATE_MODEL:
        """
        Use a balanced truncature method to decrease the order of the state space system.

        :param state_system: Matrices and vectors of the state space model to be reduced
        :type state_system: STATE_MODEL
        :param order: reduced order for the approximated state model
        :type order: int
        :return: a reduced order state model close to the provided state model
        :rtype: STATE_MODEL
        """
        _state_model = self._full_thermal_state_model()
        B_Tin = _state_model['B_Tin']
        B_heat = _state_model['B_heat']
        D_Tin = _state_model['D_Tin']
        D_heat = _state_model['D_heat']
        n_Tin = len(_state_model['U_Tin'])
        no_power_input = B_heat is None or B_heat.shape[0] == 0
        if _state_model['type'] == 'differential' and order is not None:
            A: numpy.array = numpy.array(_state_model['A'])
            if no_power_input:
                B: numpy.array = numpy.array(B_Tin)
            else:
                B: numpy.array = numpy.array(numpy.concatenate((B_Tin, B_heat), axis=1))
            C: numpy.array = numpy.array(_state_model['C'])
            if no_power_input:
                D: numpy.array = numpy.array(D_Tin)
            else:
                D = numpy.array(numpy.concatenate((D_Tin, D_heat), axis=1))
            lti_model = LTIModel.from_matrices(A, B, C, D=D, E=None, sampling_time=0, presets=None, solver_options=None, error_estimator=None, visualizer=None, name=None)
            reductor = BTReductor(lti_model)
            reduced_order_model = reductor.reduce(r=order, projection='sr')
            A, B, C, D, E = reduced_order_model.to_matrices()
            # A = Matrix(A)
            # B = Matrix(B)
            # C = Matrix(C)
            # D = Matrix(D)
            n = A.shape[0]
            if no_power_input:
                B_Tin = B
                B_heat = numpy.array()
                D_Tin = D
                D_heat = numpy.array()
            else:
                B_Tin = B[:,:n_Tin]
                B_heat = B[:,n_Tin:]
                D_Tin = D[:,:n_Tin]
                D_heat = D[:,n_Tin:]
            X = ['X%i' % i for i in range(n)]
            reduced_order_state_system = {'A': A, 'B_Tin': B_Tin, 'B_heat': B_heat, 'C': C, 'D_Tin': D_Tin, 'D_heat': D_heat, 'Y': _state_model['Y'], 'X': X, 'U_Tin': self.temperatures_in, 'U_heat': self.heatflows_sources, 'type': 'differential'}
            return reduced_order_state_system
        else:
            return _state_model
