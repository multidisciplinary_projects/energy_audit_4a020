""""
A module that contains a data container of on-site measurements.

Author:  stephane.ploix@g-scop.grenoble-inp.fr

The module is a container for 1h sampled measurement data loaded from a csv file, and from any
open weather file related to the site under study. It can be connected to the solar module to the
generate data for solar irradiation taking into account weather (mostly cloudiness) and masks.

The module aims at generating useful data for the other modules like total heat gain or occupancy.
"""

import configparser
import tkinter as tk
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict, List, Tuple, TypeVar

import pandas
import plotly.express

from buildingenergy.parameters import ParameterSet
from buildingenergy import timemg
from buildingenergy.openweather import SiteWeatherData, OpenWeatherMapJsonReader

pandas.options.plotting.backend = "plotly"
config = configparser.ConfigParser()
config.read('setup.ini')

Data = TypeVar('Data')
ParameterizedData = TypeVar('ParameterizedData')


class SetpointGenerator:
    """
    This class is a setpoint generator. It first generates a constant setpoint and proposes different 
    methods to intersect with the original signal. The value None means no setpoint i.e. control
    is off. 
    """

    def __init__(self, reference_setpoint: float, datetimes: List[datetime]):
        """
        Initialize the setpoint generator.

        :param reference_setpoint: the constant maximum setpoint value
        :type reference_setpoint: float
        :param datetimes: the list of dates (hours) corresponding to samples (that should be identical to
        to a Data object for integration)
        :type datetimes: List[datetime]
        """
        self.reference_setpoint = reference_setpoint
        self.datetimes: List[datetime] = datetimes
        self.setpoints: List[float] = [reference_setpoint for _ in range(len(datetimes))]

    @property
    def values(self) -> List[float]:
        """
        Return the setpoint values

        :return: setpoint values
        :rtype: List[float]
        """
        return self.setpoints

    def _intersect(self, setpoints: List[float]):
        """
        Internal method used to intersect (take the minimum of the current setpoints and the provided ones)
        another list of setpoints

        :param setpoints: setpoints that will be compared to the current setpoint values: the minimum is
        computed for each sample and replace the current setpoint value. None is the minimum of all the possible values
        :type setpoints: List[float]
        """
        for i in range(len(self.datetimes)):
            if self.setpoints[i] is not None:
                if setpoints[i] is None:
                    self.setpoints[i] = None
                else:
                    self.setpoints[i] = min(self.setpoints[i], setpoints[i])

    def seasonal(self, summer_period_start: str = '15/03', summer_period_end: str = '15/10'):
        """
        Generate setpoints corresponding to seasonal start and stop of the HVAC system for instance.

        :param summer_period_start: starting date for the summer period (signal starts to be off), defaults to '15/03'
        :type summer_period_start: str, optional
        :param summer_period_end: ending date for the summer period (end of the off values), defaults to '15/10'
        :type summer_period_end: str, optional
        """
        summer_period_start = tuple([int(v) for v in summer_period_start.split('/')])
        summer_period_end = tuple([int(v) for v in summer_period_end.split('/')])
        setpoints = list()
        for datetime in self.datetimes:
            start_summer_day, end_summer_day = summer_period_start[0], summer_period_end[0]
            start_summer_month, end_summer_month = summer_period_start[1], summer_period_end[1]

            summer = None
            if start_summer_month < datetime.month < end_summer_month:
                summer = True
            elif (start_summer_month == datetime.month and datetime.day >= start_summer_day) and (end_summer_month == datetime.month and datetime.day < end_summer_day):
                summer = True
            else:
                summer = False
            if summer:
                setpoints.append(None)
            else:
                setpoints.append(self.reference_setpoint)
        self._intersect(setpoints)

    def daily(self, weekdays: List[int], low_setpoint: float, triggers: List[int], initial_on_state: bool = False):
        """
        Generate a setpoint corresponding to daily hours for selected days of the week.

        :param weekdays: list of the days of the week (0: Monday,... 6: Sunday) concerned by the setpoints
        :type weekdays: List[int]
        :param low_setpoint: low setpoint values used for computing the intersection with the current signal
        :type low_setpoint: float
        :param triggers: hours where the signal will switch from reference setpoint value to the low setpoint
        value and conversely, defaults to List[int]
        :type triggers: List[int], optional
        :param initial_on_state: initial setpoint value is low setpoint if False and reference setpoint value if True, defaults to False
        :type initial_on_state: bool, optional

        """
        current_state = initial_on_state
        on_triggers = dict()
        for trigger in triggers:
            on_triggers[trigger] = current_state
            current_state = not current_state

        if type(weekdays) is int:
            weekdays = [weekdays]
        setpoints = list()
        profile = list()
        on_state = False
        for trigger_index in range(on_triggers[0]):
            profile.append(self.reference_setpoint if on_triggers[0] else low_setpoint)
        for trigger_index in on_triggers:
            while len(profile) < trigger_index:
                if on_state:
                    profile.append(self.reference_setpoint)
                else:
                    profile.append(low_setpoint)
            on_state = on_triggers[trigger_index]
        for _ in range(trigger_index, 24):
            profile.append(self.reference_setpoint if on_triggers[trigger_index] else low_setpoint)

        for datetime in self.datetimes:
            if datetime.weekday() in weekdays:
                setpoints.append(profile[datetime.hour])
            else:
                setpoints.append(self.reference_setpoint)
        self._intersect(setpoints)

    def long_absence(self, long_absence_setpoint: float, number_of_days: int, presence: List[float]):
        """
        Detect long absences for setting the setpoints to off.

        :param long_absence_setpoint: setpoint value in case of long absence detected
        :type long_absence_setpoint: float
        :param number_of_days: number of days over which a long absence is detected
        :type number_of_days: int
        :param presence: list of hours with presence (>0) and absence (=0)
        :type presence: List[float]
        """
        long_absence_start = None
        long_absence_counter: int = 0
        setpoints: list = list()
        for i in range(len(self.datetimes)):
            if presence[i] > 0:
                if long_absence_start is not None and long_absence_start + long_absence_counter > number_of_days * 24:
                    for i in range(long_absence_start, long_absence_counter):
                        setpoints.append(long_absence_setpoint)
                else:
                    for i in range(long_absence_start, long_absence_counter):
                        setpoints.append(self.reference_setpoint)
                long_absence_counter = 0
                setpoints.append(self.reference_setpoint)
            else:
                if long_absence_start is None:
                    long_absence_counter = 1
                    long_absence_start = i
                else:
                    long_absence_counter += 1
        for i in range(len(setpoints), len(self.datetimes)):
            setpoints.append(self.reference_setpoint)
        self._intersect(setpoints)

    def opening(self, opening_setpoint: List[float], opening_threshold: float, openings: List[float]):
        """
        Modify setpoint values on the occurrence of setpoint (window opening ratio for instance) values of an extra-signal passing a threshold.

        :param opening_setpoint: setpoint value to be used in case of the extra signal pass a threshold
        :type opening_setpoint: List[float]
        :param opening_threshold: threshold over which the opening threshold value will be applied.
        :type opening_threshold: float
        :param openings: extra-signal whose values will trigger the right setpoint
        :type openings: List[float]
        """
        setpoints: list = list()
        for i in range(len(self.datetimes)):
            setpoints.append(opening_setpoint if openings[i] >= opening_threshold else self.reference_setpoint)
        self._intersect(setpoints)


class Data:
    """It gathers measurement data corresponding to dates with a regular sample time. A Data container gives an easy access to them and make it possible to plot or save them. Indeed, data whose length is equal to the number of hours of the data container, can be added at any time and analyzed at the end. Data can come from measurements or deduced from a parameter-dependent formula applying to measurements"""

    def __init__(self, csv_measurement_filename: str, json_openweather_filename: str, starting_stringdate: str = None, ending_stringdate: str = None, sea_level_in_meter: float=290, albedo: float=.1, pollution: float=0.1, location:str=None, deleted_variables: Tuple[str] = ()):
        """Create a data container by collecting data from a csv file.

        :param csv_measurement_filename: name of the csv file containing measurement data with different format of date in the 3 first columns (string like '15/02/2015 00:00:00' for the first one, epochtime in ms for the second one, and datetime.datetime for the 3rd one). The first row is used as name for the data of the related column. The file with the provided name will be search in the data folder defined in the setup.ini file in the project root folder. Data must be organized in ascending order.
        :type csv_measurement_filename: str
        :param initial_string_date: initial date in format 'dd/mm/YYYY' or None. If None the first date of the file starting at 0:00:00 time will be selected, default to None, optional
        :type initial_string_date: str
        :param final_string_date: final date in format 'dd/mm/YYYY' or None. If None the latest date of the file starting at 23:00:00 time will be selected, default to None, optional
        :type final_string_date: str
        """

        self._dataframe = pandas.read_csv(config['folders']['data'] + csv_measurement_filename, dialect='excel', parse_dates=['datetime'])
        self._sample_time_in_secs: int = 3600
        self._site_weather_data: None = None
        self._parameterized_data: Dict[str, ParameterizedData] = dict()

        self._dataframe = self._dataframe.astype({'epochtime': 'int64', 'stringtime': 'str', 'datetime': 'datetime64[ns]'})
        self._dataframe = self._dataframe.astype({v: float for v in self._dataframe.columns[3:]})

        print('variables read from file measurement file:')
        for column_name in self._dataframe.columns:
            print(column_name, end=', ')
        print()
        if deleted_variables is not None:
            for column_name in self._dataframe.columns:
                if column_name in deleted_variables and column_name not in ('stringtime', 'epochtime', 'datetime'):
                    self._dataframe.drop(column_name, inplace=True, axis=1)
                    print('Variable %s has been removed' % column_name)

        if starting_stringdate is not None:
            self.starting_stringdatetime = starting_stringdate + " 00:00:00"
            starting_datetime = timemg.stringdate_to_datetime(self.starting_stringdatetime, date_format='%d/%m/%Y %H:%M:%S')
            self._dataframe.drop(self._dataframe[self._dataframe['datetime'] < starting_datetime].index, inplace=True)
        else:
            i: int = 0
            while self._dataframe['stringtime'][i].split(' ')[1] != '00:00:00':
                i += 1
            if i != 0:
                self._dataframe: Dataframe = self._dataframe[i:]

        if ending_stringdate is not None:
            self.ending_stringdatetime = ending_stringdate + " 23:00:00"
            ending_datetime: datetime = timemg.stringdate_to_datetime(self.ending_stringdatetime, date_format='%d/%m/%Y %H:%M:%S')
            self._dataframe.drop(self._dataframe[self._dataframe['datetime'] > ending_datetime].index, inplace=True)
        else:
            i: int = len(self._dataframe)-1
            while self._dataframe['stringtime'][i].split(' ')[1] != '23:00:00':
                i -= 1
            if i != len(self._dataframe)-1:
                self._dataframe = self._dataframe[:i+1]
        self._dataframe.index = self._dataframe['datetime']

        self._dataframe['day_of_year'] = self._dataframe['datetime'].dt.day_of_year
        self._dataframe['day_of_week'] = self._dataframe['datetime'].dt.day_of_week
        self._dataframe['hour'] = self._dataframe['datetime'].dt.hour
        self._dataframe['year'] = self._dataframe['datetime'].dt.year

        self.day_counter: int = -1
        _current_day: int = -1
        day_index: List[int] = list()
        hour_index: List[int] = list()
        for i in range(self.number_of_hours):
            hour_index.append(i)
            if self._dataframe['day_of_year'][i] != _current_day:
                self.day_counter += 1
                _current_day: int = self._dataframe['day_of_year'][i]
            day_index.append(self.day_counter)
        self.add_external_variable('hour_index', hour_index)
        self.add_external_variable('day_index', day_index)

        self.starting_stringdatetime: str = self._dataframe['stringtime'][0]
        self.ending_stringdatetime: str = self._dataframe['stringtime'][len(self._dataframe)-1]

        openweather_reader: OpenWeatherMapJsonReader = OpenWeatherMapJsonReader(json_openweather_filename, self.starting_stringdatetime, self.ending_stringdatetime, sea_level_in_meter, albedo, pollution, location)
        self._site_weather_data = openweather_reader.site_weather_data
        print('Weather data variables:')
        for variable_name in self._site_weather_data.variable_names:
            if variable_name not in deleted_variables:
                self.add_external_variable('weather_' + variable_name, self._site_weather_data.get(variable_name))
                print('weather_' + variable_name, end=', ')
        print()

    def add_parameterized_data(self, parameterized_data: ParameterizedData):
        """
        Add a class inheriting from buildingenergy.runner.ParameterizedData embedding a formula containing parameters (value can changed along simulation) and data. This data will be accessible as any other but is not stored for data saving or plotting.

        :param parameterized_data: class inheriting from buildingenergy.runner.ParameterizedData, containing a formula
        :type parameterized_data: buildingenergy.runner.ParameterizedData
        """
        self._parameterized_data[parameterized_data._name] = parameterized_data

    def include_invariant_data(self, data_name: str) -> bool:
        """
        Test whether an invariant (static) data, referenced by its name, belongs to the datacontainer.

        :param data_name: name of the invariant data
        :type data_name: str
        :return: True if present, False otherwise
        :rtype: bool
        """
        return data_name in self._dataframe.columns

    def include_parameterized_data(self, data_name: str) -> bool:
        """
        Test whether an parameterized (dynamic) data, referenced by its name, belongs to the datacontainer.

        :param data_name: name of the parameterized data
        :type data_name: str
        :return: True if present, False otherwise
        :rtype: bool
        """
        return data_name in self._parameterized_data

    @property
    def starting_stringdate(self) -> str:
        """
        Return the starting date i.e. the date of the data at first position, as a string

        :return: a date like "dd/mm/YYYY HH:MM:SS"
        :rtype: str
        """
        return self.starting_stringdatetime.split(' ')[0]

    @property
    def ending_stringdate(self) -> str:
        """
        Return the ending date i.e. the date of the latest data, as a string

        :return: a date like "dd/mm/YYYY HH:MM:SS"
        :rtype: str
        """
        return self.ending_stringdatetime.split(' ')[0]

    @property
    def number_of_invariant_variables(self) -> int:
        """
        Return the number of invariant variable (time is not included)

        :return: number of invariant variables
        :rtype: int
        """
        return len(self._dataframe.columns) - 3
    
    @property
    def invariant_variable_names(self) -> List[str]:
        """
        Return the names of the invariant variables

        :return: invariant variables
        :rtype: list[str]
        """
        return [self._dataframe.columns[i] for i in range(3, len(self._dataframe.columns))]

    @property
    def number_of_parameterized_variables(self) -> int:
        """
        Return the number of parameterized variables

        :return: number of parameterized variable
        :rtype: int
        """
        return len(self._parameterized_data)
    
    @property
    def parameterized_variable_names(self) -> List[str]:
        """
        Return the names of parameterized variables

        :return: names of parameterized variables
        :rtype: list[str]
        """
        return list(self._parameterized_data.keys())

    def add_external_variable(self, name: str, values: List[float]):
        """Use to add a invariant series of values to the container. It will appears as any other measurements. The number of values must correspond to the number of hours for the data container.

        :param label: name of the series of timed values (each value corresponds to 1 hour)
        :type label: str
        :param values: series of values but it must be compatible with the times which are common to all series
        :type values: list[float]
        """
        self._dataframe[name] = values
        self._dataframe = self._dataframe.astype({name:float})

    @property
    def weather_data(self) -> SiteWeatherData:
        """
        Return an SiteWeatherData object from the openweather module describing the site (location, latitude,...)

        :return: characteristics of the site
        :rtype: SiteWeatherData
        """
        return self._site_weather_data

    def _plot_selection(self, int_vars: list):
        """Use to plot curve using plotly library

        :param int_vars: reference to the variable to be plotted
        :type: list[int]
        """
        plots: list = int_vars
        #variable_to_plot = plots.to_json
        title: str = 'from %s to %s' % (self.starting_stringdatetime, self.ending_stringdatetime)
        variable_to_plot = list()
        for i in range(len(int_vars)):
            if int_vars[i].get():
                variable_to_plot.append(self._dataframe.columns[i + 3])
                int_vars[i].set(0)
        fig = plotly.express.line(self._dataframe, x="datetime", y=variable_to_plot, title=title).show()

    def plot(self):
        """Display a series selector to plot the different time series."""
        tk_variables = list()
        tk_window = tk.Tk()
        tk_window.wm_title('variable plotter')
        tk.Button(tk_window, text='plot', command=lambda: self._plot_selection(tk_variables)).grid(row=0, column=0, sticky=tk.W + tk.E)
        frame = tk.Frame(tk_window).grid(row=1, column=0, sticky=tk.N + tk.S)
        vertical_scrollbar = tk.Scrollbar(frame, orient=tk.VERTICAL)
        vertical_scrollbar.grid(row=1, column=1, sticky=tk.N + tk.S)
        canvas = tk.Canvas(frame, width=400, yscrollcommand=vertical_scrollbar.set)
        tk_window.grid_rowconfigure(1, weight=1)
        canvas.grid(row=1, column=0, sticky='news')
        vertical_scrollbar.config(command=canvas.yview)
        checkboxes_frame = tk.Frame(canvas)
        checkboxes_frame.rowconfigure(1, weight=1)
        # if value[  _ in range(self.number_of_hours):
        for i in range(3, len(self._dataframe.columns)):
            tk_variable = tk.IntVar()
            tk_variables.append(tk_variable)
            tk.Checkbutton(checkboxes_frame, text=self._dataframe.columns[i], variable=tk_variable, offvalue=0).grid(row=(i - 3), sticky=tk.W)
        canvas.create_window(0, 0, window=checkboxes_frame)
        checkboxes_frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox('all'))
        tk_window.geometry(str(tk_window.winfo_width()) + "x" + str(tk_window.winfo_screenheight()))
        tk_window.mainloop()

    @property
    def number_of_hours(self):
        """Return the recorded date size in hours.

        :return: the data size in hours
        :rtype: int
        """
        return len(self._dataframe)

    @property
    def number_of_days(self):
        """Return the recorded date size in days.

        :return: the data size in days
        :rtype: int
        """
        return self.day_counter

    def add_constant(self, name: str, value: float):
        """
        Add an constant invariant variable

        :param name: name of the data
        :type name: str
        :param value: value of the date
        :type value: float
        """        
        values = [value for _ in range(self.number_of_hours)]
        self.add_external_variable(name, values)

    def __call__(self, variable_name: str, hour_index: int = None, day_index: int = None) -> List[float]:
        """
        Functor used to get data defined by its name that could be:
        - for invariant data: for a single hour, for a single day (24 values) or all the data in the container (the length is therefore equal to the number of hours)
        - for parameterized data: the only option is to request for a single hour

        :param variable_name: name of the data
        :type variable_name: str
        :param hour_index: index of the hour in the data series (get the priority on day index if provided), defaults to None
        :type hour_index: int, optional
        :param day_index: index of the day return 24 values corresponding to the day index but only if no hour index are given, defaults to None
        :type day_index: int, optional
        :raises ValueError: error raised when the data name is not in the list
        :return: the requested values
        :param number_of_invariant_variables: 
        :rtype: dict[str]
        """        
        if hour_index is None and day_index is None:
            if variable_name in self._dataframe.columns:
                if variable_name == "datetime":
                    __data = pandas.to_datetime(self._dataframe[variable_name]).tolist()
                    return __data
                return self._dataframe[variable_name].values.tolist()
            elif self.include_parameterized_data(variable_name):
                return self.parameterized_data[variable_name].series
        if hour_index is not None:
            if variable_name in self._dataframe.columns:
                return self._dataframe[variable_name][self._dataframe['hour_index'] == hour_index].values.tolist()[0]
            elif variable_name in self._parameterized_data:
                return self._parameterized_data[variable_name](hour_index)
        if day_index is not None:
            if variable_name in self._dataframe.columns:
                return self._dataframe[variable_name][self._dataframe['day_index'] == day_index].values.tolist()
            elif variable_name in self._parameterized_data:
                series: List[float] = list()
                for k in range(self.number_of_hours):
                    if k == day_index:
                        series.append(self.dataframe[datetime][k])
                return series
                    
        raise ValueError('Unknown data "%s"' % variable_name)

    def __str__(self):
        """Make it possible to print a data container.

        :return: description of the datacontainer
        :rtype: str
        """
        string = 'Data cover period from %s to %s with time period: %d seconds\nRegistered database:\n' % (
            self.starting_stringdatetime, self.ending_stringdatetime, self.sample_time)
        for database in self.registered_databases:
            string += '- %s \n' % database
        string += 'Available variables:\n'
        for variable_name in self.extracted_variables:
            string += '- %s \n' % variable_name
        return string

    def save(self, file_name: str = 'results.csv', selected_variables: List[str] = None):
        if selected_variables is None:
            _selected_variables = self._dataframe.columns
        else:
            _selected_variables = ['epochtime', 'stringtime', 'datetime']
            _selected_variables.extend(selected_variables)
        file_name = config['folders']['results'] + file_name
        self._dataframe[_selected_variables].to_csv(file_name, index=False)
        print('Following variables have been saved into file "%s": ' % file_name, end='')
        for selected_variable in _selected_variables:
            print(selected_variable, end=', ')
        print()


class ParameterizedData(ABC):

    def __init__(self, data: Data, parameter_set: ParameterSet, name: str, parameter_names: List[str], data_names: List[str]):
        """
        :param data: data container of the problem
        :type data: Data
        :param param: parameter object of the problem
        :type param: Param
        - name of the parameterized data
        - parameter used
        - data used
        """  
        self._name: str = name
        self._data: Data = data
        self._parameter_set: ParameterSet = parameter_set
        if type(parameter_names) == str:
            self.parameter_names = [parameter_names]
        else:
            self.parameter_names = parameter_names
        if type(data_names) == str:
            self.data_names = [data_names]
        else:
            self.data_names = data_names
        self._data.add_parameterized_data(self)

    def __call__(self, k: int) -> float:
        """
        return the updated result of the formula taking into account the current data and parameters' values.

        :param k: hour index
        :type k: int
        :return: result of the formula
        :rtype: float
        """
        return self.formula({parameter_name: self._parameter_set(parameter_name) for parameter_name in self.parameter_names}, {data_name: self._data(data_name, k) for data_name in self.data_names})
    
    @property
    def series(self) -> List[float]:
        """
        Return a vector containing the values of the Parameterized data

        :return: vector of hourly values consistent with the measurement Data 
        :rtype: list(float)
        """
        time_series: List[float] = list()
        for k in range(self._data.number_of_hours):
            time_series.append(self.__call__(k))
        return time_series

    @abstractmethod
    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        """
        formula of the parameterized data

        :param param: dictionary associating values to parameter names
        :type param: Dict[str, float]
        :param data: dictionary associating values to data names
        :type data: Dict[str, float]
        :return: result of the formula
        :rtype: float
        """
        raise NotImplementedError
