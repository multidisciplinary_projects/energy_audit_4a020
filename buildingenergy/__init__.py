"""This library contains a set of helpers to support the different steps of the design of energy management strategies.

Author: stephane.ploix@grenoble-inp.fr
"""
__all__ = ["data", "openweather", "solar", "solar", "timemg", "runner", "thermal", "thermics", "model", "model_old", "linreg", "dynprog", "thermalstatespace", "building", "lambdahouse", "physics"]
