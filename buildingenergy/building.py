"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module is for the easy design of multi-zone buildings.
It models both the thermal behavior and the evolution of the CO2 concentration.
The thermal part is using thermal module
"""
from enum import Enum
from typing import Dict, List, Tuple, TypeVar

import numpy, numpy.linalg
import control
import networkx
#from sympy import Matrix, pprint, re, zeros, eye

from buildingenergy import timemg
from buildingenergy.physics import Library
from buildingenergy.thermal import CAUSALITY, ThermalNetwork
import matplotlib.pyplot as plt

StateModel = TypeVar('StateModel')  # Dict[str, numpy.matrix|List[float]|str]


class InterfaceType(Enum):
    """
    TYpes of interface between 2 zones where negative value means horizontal, positive means vertical.
    """
    HEATING_FLOOR = -6
    CANOPY = -5
    VELUX = -4
    GROUND = -3
    ROOF = -2
    SLAB = -1
    BRIDGE = 0
    WALL = 1
    DOOR = 2
    GLAZING = 3
    CUPBOARD = 4


class Kind(Enum):
    """
    Location for a zone (wall or floor)
    """
    OUTDOOR = 0
    INDOOR = 1
    INFINITE = -1


class Direction(Enum):
    """
    Direction for a interface (wall or floor)
    """
    HORIZONTAL = 1
    VERTICAL = 2


Zone = TypeVar('Zone')

class Airflow:
    """
    It represents an directional air flow between 2 zones. It can propagate permanent airflow from an input zone to an output one accordingly to a propagation weight.
    It can be used for bi-directional propagation by creating another airflow in the opposite direction.
    """

    def __init__(self, from_zone: Zone, to_zone: Zone, weight: float = 1):
        """
        Initialize an air flow. The weight is leading to a distribution ratio taking into account the sum of all the air flow weights leaving a zone.

        :param from_zone: Zone from which the air flow is coming from
        :type from_zone: Zone
        :param to_zone: Zone to which the air flow is coming in
        :type to_zone: Zone
        :param weight: defined the relative quantity of this air flow relatively to the sum of all the air flow weights leaving a zone, defaults to 1
        :type weight: float, optional
        """
        self._from_zone = from_zone
        self._to_zone = to_zone
        self._weight = weight
        self._propagated_airflows = dict()
        self._is_input = False
        self._from_zone._connected_out_airflows.append(self)
        self._to_zone._connected_in_airflows.append(self)

    @property
    def is_input(self) -> bool:
        """
        Return true if this air flow has to be known (measured) to be able to solve the permanent air flows

        :return: True if it must be known, False otherwise
        :rtype: bool
        """
        return self._is_input

    @property
    def is_valued(self) -> bool:
        """
        Return true if air flows have already been propagated from one zone to another

        :return: True if already propagated, False otherwise
        :rtype: bool
        """
        return len(self._propagated_airflows) > 0

    @property
    def weight(self) -> float:
        """
        Return the relative weight of the air flow: it should be assumed or learnt.

        :return: a value relative to the sum of all the weights getting out of the _from_zone
        :rtype: float
        """
        return self._weight

    def _solve_causality(self):
        """
        Set the condition on the _from_zone to be able to propagate the air flow.
        If the input air flow of the outgoing zone is not known and the incoming zone is not simulated, then
        it has to be known and consequently the current air flow becomes an input flow whose value has to be provided.
        """
        if not self._to_zone._is_known and not self._from_zone.is_simulated: # and self._to_zone.is_simulated
            self._from_zone._is_known = True
            self._is_input = True
            self._from_zone._propagated_airflow[self._from_zone.airflow_name] = 1


    def _propagate(self, total_zone_weight: float=1) -> bool:
        """
        Propagate the known input air flows throughout the air flows and the zones, accordingly to
        the weights defined for the output air flows going out of each zone.

        :param total_zone_weight: some of the weights of the air flows coming out the _from_zone, defaults to 1
        :type total_zone_weight: float, optional
        :return: True if propagation could be done, False otherwise
        :rtype: bool
        """
        if self._to_zone._is_known:
            return True
        elif self._from_zone.is_valued:
            for core_airflow_name in self._from_zone._propagated_airflow:
                if core_airflow_name not in self._to_zone._propagated_airflow:
                    self._to_zone._propagated_airflow[core_airflow_name] = 0
                if total_zone_weight > 0:
                    self._propagated_airflows[core_airflow_name] = self._from_zone._propagated_airflow[core_airflow_name] * self.weight / total_zone_weight
                    self._to_zone._propagated_airflow[core_airflow_name] +=  self._propagated_airflows[core_airflow_name]
            return True
        else:
            return False

    def __str__(self) -> str:
        """
        Representation of the airflow by a string

        :return: description of the air flow
        :rtype: str
        """
        return 'airflow coming from zone "%s" injected into zone "%s" with weight %f. Propagated quantities are: %s (is_input: %r)' % (self._from_zone.airflow_name, self._to_zone.name, self._weight, self._propagated_airflows, self._is_input)


class Zone:
    """
    A zone are core elements for a building: it is composed of indoor zones and of an outdoor zone
    """

    def __init__(self, name: str, kind: Kind = Kind.INDOOR):
        """
        Create a zone

        :param name: name of the zone (it will be trimmed and space will be replaced by '_'
        :type name: str
        :param location: kind of zone, defaults to Kind.Indoor
        :type location: Location, optional
        """
        self._name: str = name.strip().replace(" ", "_")
        self._kind: str = kind
        self._air_temperature_name: str = 'TZ' + self._name
        self._power_gain_name: str = 'PZ' + self._name
        self._air_capacitance_name: str = 'CZ' + self._name
        self._CO2_concentration_name: str = 'CCO2_' + self._name
        self._CO2_production_name: str = 'PCO2_' + self._name
        self._connected_in_airflows: List[Airflow] = list()
        self._connected_out_airflows: List[Airflow] = list()
        self._propagated_airflow: Dict[str, float] = dict()
        self._is_known = False
        self._volume = None

    @property
    def name(self):
        """
        Return the name of the zone

        :return: name of the zone
        :rtype: str
        """
        return self._name

    @property
    def has_no_zone_to_feed(self) -> bool:
        """
        Return whether there outgoing air flows or not

        :return: True if there is no outgoing air flows, False elsewhere
        :rtype: bool
        """
        return len(self._connected_out_airflows)  == 0

    @property
    def output_connected_zones(self) -> List[Zone]:
        """
        List of connected zones linked by outgoing air flows to the current zone?

        :return: List of zones
        :rtype: List[Zone]
        """
        _output_connected_zones = list()
        for _connected_out_airflow in self._connected_out_airflows:
            _output_connected_zones.append(_connected_out_airflow._to_zone)
        return _output_connected_zones

    @property
    def input_connected_zones(self) -> List[Zone]:
        """
        List of connected zones linked by incoming air flows to the current zone?

        :return: List of zones
        :rtype: List[Zone]
        """
        _input_connected_zones = list()
        for _connected_in_airflow in self._connected_in_airflows:
            _input_connected_zones.append(_connected_in_airflow._from_zone)
        return _input_connected_zones

    @property
    def is_valued(self) -> bool:
        """
        Determine whether the input flows have been propagated to this zone

        :return: True if zone contains propagated input flows
        :rtype: bool
        """
        return len(self._propagated_airflow) != 0

    @property
    def is_simulated(self) -> bool:
        """
        True is the current zone total incoming (or outgoing) air flow has to be simulated: it gets this status by setting a volume to the current zone.

        :return: True if it has to be simulated, False elsewhere;
        :rtype: bool
        """
        return self.volume is not None

    @property
    def is_known(self) -> bool:
        """
        _summary_

        :return: _description_
        :rtype: bool
        """
        return self._is_known

    @property
    def volume(self):
        return self._volume

    @volume.setter
    def volume(self, _volume):
        self._volume = _volume

    @property
    def airflow_name(self) -> str:
        """
        Generate an input airflow name for the zone

        :return: airflow name: Q*zone_name*
        :rtype: str
        """
        self.airflow_input_name: str = 'Q' + self._name
        return self.airflow_input_name

    def connect_airflow(self, to_zone: Zone, weight: float=1):
        return Airflow(self, to_zone, weight)

    def propagate(self):
        succeeded = True
        for airflow in self._connected_out_airflows:
            succeeded = succeeded and airflow._propagate(sum([airflow.weight for airflow in self._connected_out_airflows]))
        return succeeded

    @property
    def T(self) -> str:
        """
        return the air temperature name of the current zone

        :return: the air temperature name of the current zone
        :rtype: str
        """
        return self._air_temperature_name

    @property
    def C(self) -> str:
        """
        return the air capacitance name of the current zone

        :return: the air capacitance name of the current zone
        :rtype: str
        """
        return self._air_capacitance_name

    @property
    def HEAT(self) -> str:
        """
        return the heat flow injected into the zone

        :return: heat flow name
        :rtype: str
        """
        return self._power_gain_name


    def __str__(self) -> str:
        string='zone "%s" with an temperature "%s", and airflows quantities: "%s" (is_known: %r, is_simulated: %r)' % (self._name, self._air_temperature_name, self._propagated_airflow, self._is_known, self.is_simulated)
        if self._kind == Kind.INDOOR:
            string += ' and power gain "%s"' % (self._power_gain_name,)
        string += '\n   with incoming air flows:\n'
        for air_flow in self._connected_in_airflows:
            string += air_flow.__str__() + '\n'
        string += '\n   and outgoing air flows:\n'
        for air_flow in self._connected_out_airflows:
            string += air_flow.__str__() + '\n'
        return string


class LayeredInterface:
    """
    A layered interface is a set of air layers, where both extreme layers cannot be air layers. Additionally, 2 air layers cannot be stacked consecutively.
    """
    def __init__(self, zone1: Zone, zone2: Zone, interface_type: InterfaceType, surface: float):
        """
        Initialize a layered interface

        :param library: library containing physical properties
        :type library: Library
        :param zone1: zone next to side 1
        :type zone1: Zone
        :param zone2: zone next to side 2
        :type zone2: Zone
        :param interface_type: type interface corresponding to layered interface
        :type interface_type: Interface
        :param surface: surface of the interface
        :type surface: float
        """
        self.zone1, self.zone2=zone1, zone2
        self.surface=surface
        self.interface_type=interface_type
        self.first_layer_indoor=not (zone1._kind == Kind.OUTDOOR)
        self.last_layer_indoor=not (zone2._kind == Kind.OUTDOOR)
        if self.interface_type.value > 0:
            self.direction=Direction.VERTICAL
        else:
            self.direction=Direction.HORIZONTAL
        self._layers: List[Dict[str, float]]=list()

    def add_layer(self, material: str, thickness: float):
        """
        add a layer to the interface

        :param material: short name of the thermal
        :type material: str
        :param thickness: _description_
        :type thickness: float
        """
        if material in Site.library:
            self._layers.append({'material': material, 'thickness': thickness})

    @property
    def layers(self) -> List[Dict[str, float]]:
        """
        return the existing layers

        :return: list of layers (material, thickness)
        :rtype: List[str, float]
        """
        return self._layers

    @property
    def thermal_capacitances(self) -> List[float]:
        """
        Return the list of capacitances, one for each layer of an interface

        :return: list of capacitances, one for each layer of an interface
        :rtype: List[float]
        """
        capacitances=list()
        for layer in self.layers:
            density=Site.library.get(layer['material'])['density']
            Cp=Site.library.get(layer['material'])['Cp']
            capacitances.append(layer['thickness']*self.surface*density*Cp)
        return capacitances

    @property
    def thermal_resistances(self) -> List[float]:
        """
        Return the list of resistances, one for each layer of an interface, plus 2 surface resistances, one at each extremity

        :return: list of resistances
        :rtype: List[float]
        """
        resistances=list()
        if self.interface_type.value == InterfaceType.BRIDGE:
            raise ValueError('A bridge cannot appear in an interface')
        if self._layers[0]['material'] == 'air' or self._layers[-1]['material'] == 'air':
            raise ValueError('An air layer cannot appear in an external layer.')
        for i in range(1, len(self._layers)-1):
            if self._layers[i]['material'] == 'air' and self._layers[i+1]['material'] == 'air':
                raise ValueError('Consecutive air layer are prohibited: gather air layers.')
        if self.first_layer_indoor:
            resistances.append(Site.library.indoor_surface_resistance(self._layers[0]['material'], self.direction) / self.surface)
        else:
            resistances.append(Site.library.outdoor_surface_resistance(self._layers[0]['material'], self.direction) / self.surface)

        for i in range(len(self._layers)):
            if self._layers[i]['material'] != 'air':
                resistances.append(Site.library.conduction_resistance(self._layers[i]['material'], self._layers[i]['thickness']) / self.surface)
            else:
                resistances.append(Site.library.thermal_air_gap_resistance(self._layers[i-1]['material'],
                                   self._layers[i+1]['material'], self._layers[i]['thickness'], self.direction) / self.surface)

        if self.last_layer_indoor:
            resistances.append(Site.library.indoor_surface_resistance(self._layers[-1]['material'], self.direction) / self.surface)
        else:
            resistances.append(Site.library.outdoor_surface_resistance(self._layers[-1]['material'], self.direction) / self.surface)
        return resistances

    @property
    def thermal_resistance(self) -> float:
        """
        global thermal resistance for the interface

        :return: global thermal resistance in K/W
        :rtype: float
        """
        _thermal_resistance = None 
        for Rth in self.thermal_resistances:
            if Rth != None:
                if _thermal_resistance is None:
                    _thermal_resistance = Rth
                else:
                    _thermal_resistance += Rth
        return _thermal_resistance

    @property
    def thermal_loss(self) -> float:
        """
        return global thermal loss

        :return: global thermal loss in K/W
        :rtype: float
        """
        return 1 / self.thermal_resistance

    def __str__(self) -> str:
        """
        :return: string depicting the layered interface
        :rtype: str
        """
        string='layered interface (%s, %s) type: %s surface: %.3fm2 with losses at %.2fW/K composed of ' % (self.zone1._name,
                             self.zone2._name, self.interface_type, self.surface, 1/self.thermal_resistance)
        US=[1/R for R in self.thermal_resistances]
        for i in range(len(self._layers)):
            string += '(%s, %.3fm > %.2f W/K) ' % (self._layers[i]['material'], self._layers[i]['thickness'], US[i])
        return string


class ComponentInterface:
    """
    A component interface is an interface depicted only by a global thermal resistance
    """

    def __init__(self, zone1: Zone, zone2: Zone, interface_type: InterfaceType, heat_transmission_coefficient: float, surface: float=1):
        """
        Initialize a component interface

        :param zone1: zone next to side 1
        :type zone1: Zone
        :param zone2: zone next to side 2
        :type zone2: Zone
        :param interface_type: type interface corresponding to layered interface
        :type interface_type: Interface
        :param heat_transmission_coefficient: heat transmission coefficient in W/m2/K or in W/K if the surface is 1
        :type heat_transmission_coefficient: float
        :param surface: surface of the interface, default to 1
        :type surface: float, optional
        """
        self.zone1, self.zone2=zone1, zone2
        self.US=surface * heat_transmission_coefficient
        self.interface_type=interface_type

    @ property
    def thermal_resistance(self) -> float:
        """
        global resistance of the component interface

        :return: resistance value in K/W
        :rtype: float
        """
        return 1 / self.US if self.US != 0 else None

    @ property
    def thermal_loss(self) -> float:
        """
        global thermal loss (U*S) of the component interface

        :return: thermal loss in W/K
        :rtype: float
        """
        return self.US

    def __str__(self) -> str:
        """
        :return: string depicting the component interface
        :rtype: str
        """
        string='component interface (%s, %s) type: %s with losses at %f W/K \n' % (self.zone1._name, self.zone2._name, self.interface_type, self.US)
        return string


class Site:
    """
    This class represents a whole building with its zones, its layered facets composing interfaces separating 2 zones.
    """
    library: Library = Library()

    def __init__(self, *zone_names: Tuple[str]):
        """
        Initialize a site

        :param zone_names: names of the zones except for 'outdoor', which is automatically created
        :type zone_names: Tuple[str]
        :param args: set the order of the resulting reduced order thermal state model with order=integer value. If the value is set to None, there won't be order reduction of the thermal state model, default to None
        :type args: Dict[str, float], optional
        """
        self._thermal_network: ThermalNetwork = ThermalNetwork()
        self._airflow_network: networkx.DiGraph = networkx.DiGraph()
        self._zones: Dict[str, Zone]=dict()
        for zone_name in zone_names:
            self._zones[zone_name] = Zone(zone_name)
            self._airflow_network.add_node(zone_name)
        self.layered_facets: Dict[Tuple[str, str], List[LayeredInterface]]=dict()
        self._component_interfaces: Dict[Tuple[str, str], List[ComponentInterface]]=dict()
        self.simulated_zone_names: List[str] = list()
        self.thermal_state_system_order = None
        self.has_been_propagated = False
        self._input_airflows: List[Airflow] = list()
        zone = Zone('outdoor', Kind.OUTDOOR)
        self._zones['outdoor'] = zone
        self._airflow_network.add_node('outdoor')
        
    def layered_facets(self) -> Dict[Tuple[str, str], List[LayeredInterface]]:
        """
        return for each interface the list of layers

        :return: a dictionary of interfaces with corresponding layers
        :rtype: Dict[Tuple[str, str], List[LayeredInterface]]
        """
        return self.layered_facets
        
    def zones(self):
        _zones = dict()
        for zone_name in self._zones:
            zone = self._zones[zone_name]
            _zones[zone_name] = {'kind': zone._kind, 'CO2_concentration': zone._CO2_concentration_name, 'CO2_production': zone._CO2_production_name, 'simulated': zone.is_simulated, 'power_gain': zone._power_gain_name, 'air_temperature': zone._air_temperature_name}
        return _zones
        
            
    def simulated_zone(self, zone_name: str, zone_volume: float):
        """
        define a zone as for being simulated regarding CO2 concentration

        :param zone_name: name of the zone
        :type zone_name: str
        :param zone_volume: volume of the zone
        :type zone_volume: float
        """
        self._thermal_network.T(self._zones[zone_name].T, CAUSALITY.OUT)
        self._zones[zone_name].volume = zone_volume

    def connect_airflow(self, from_zone_name: str, to_zone_name: str, weight: float=1):
        """
        create an directed air flow between 2 zones

        :param from_zone_name: zone name of the origin of the air flow
        :type from_zone_name: str
        :param to_zone_name: zone name of the destination of the air flow
        :type to_zone_name: str
        :param division: fraction (or weight) of the air flow that going from a zone to another, defaults to 1
        :type division: float, optional
        """
        from_zone = self._zones[from_zone_name]
        from_zone.connect_airflow(to_zone=self._zones[to_zone_name], weight=weight)
        self._airflow_network.add_edge(from_zone_name, to_zone_name)

    @property
    def thermal_network(self) -> ThermalNetwork:
        """
        provide a manual access to the thermal RC model

        :return: the related thermal RC model
        :rtype: ThermalNetwork
        """
        return self._thermal_network

    def add_layered_interface(self, zone1_name: str, zone2_name: str, interface_type: InterfaceType, surface: float) -> LayeredInterface:
        """
        add a layered interface between 2 zones

        :param zone1_name: first zone name
        :type zone1_name: str
        :param zone2_name: second zone name
        :type zone2_name: str
        :param interface_type: type of interface
        :type interface_type: InterfaceType
        :param surface: surface of the interface in m2
        :type surface: float
        :return: the layered interface to which the layers can be added from zone1 to zone2
        :rtype: LayeredInterface
        """
        zone_names=[zone1_name, zone2_name]
        zone_names.sort()
        zone1_name, zone2_name=zone_names
        if (zone1_name, zone2_name) not in self.layered_facets:
            self.layered_facets[(zone1_name, zone2_name)]=list()
        layered_interface=LayeredInterface(self._zones[zone1_name], self._zones[zone2_name], interface_type, surface)
        self.layered_facets[(zone1_name, zone2_name)].append(layered_interface)
        return layered_interface
    
    def interface_all_facets_thermal_resistances(self, zone1_name: str, zone2_name: str) -> List[List[float]]:
        """
        return the resistance values for all the layers of the facets of an interface

        :param zone1_name: first zone name specifying the interface
        :type zone1_name: str
        :param zone2_name: second zone name specifying the interface
        :type zone2_name: str
        :return: the list of resistance values of each layer of a facet of the specified interface
        :rtype: list[float]
        """
        _interface_all_thermal_resistances = dict()
        for facet in self.layered_facets[(zone1_name, zone2_name)]:
            _interface_all_thermal_resistances[facet] = facet.thermal_resistances
        return _interface_all_thermal_resistances
        

    def add_component_interface(self, zone1_name: str, zone2_name: str, interface_type: InterfaceType, heat_transmission_coefficient: float, surface: float=1) -> ComponentInterface:
        """
        add a component interface between 2 zones

        :param zone1_name: first zone name
        :type zone1_name: str
        :param zone2_name: second zone name
        :type zone2_name: str
        :param interface_type: type of interface
        :type interface_type: InterfaceType
        :param heat_transmission_coefficient: heat transmission coefficient U in W/m2.K or in W/K if the surface is kept to 1
        :type heat_transmission_coefficient: float
        :param surface: surface of the interface, defaults to 1
        :type surface: float, optional
        :return: the component interface
        :rtype: ComponentInterface
        """
        zone_names=[zone1_name, zone2_name]
        zone_names.sort()
        zone1_name, zone2_name=zone_names
        if (zone1_name, zone2_name) not in self._component_interfaces:
            self._component_interfaces[(zone1_name, zone2_name)]=list()
        component=ComponentInterface(self._zones[zone1_name], self._zones[zone2_name], interface_type, heat_transmission_coefficient, surface)
        self._component_interfaces[(zone1_name, zone2_name)].append(component)
        return component

    def interfaces_thermal_losses(self) -> Dict[Tuple[str, str], float]:
        """
        Return the thermal losses per interface between 2 adjacent zones

        :return: thermal losses in W/K
        :rtype: Dict[Tuple[str, str], float]
        """
        interfaces_losses: Dict[Tuple(str, str), float]=dict()
        facet_refs=set(self.layered_facets.keys()).union(set(self._component_interfaces.keys()))
        for facet_ref in facet_refs:
            interfaces_losses[facet_ref]=0
            if facet_ref in self.layered_facets:
                for composition in self.layered_facets[facet_ref]:
                    interfaces_losses[facet_ref] += composition.thermal_loss
            if facet_ref in self._component_interfaces:
                for component in self._component_interfaces[facet_ref]:
                    interfaces_losses[facet_ref] += component.thermal_loss
        return interfaces_losses

    def interfaces_thermal_resistances(self):
        """
        Return the total thermal resistance for each interface

        :return: all interfaces with corresponding thermal resistances in K/W
        :rtype: Dict[Tuple[str, str], float]
        """
        thermal_resistances_dict=dict()
        thermal_losses=self.interfaces_thermal_losses()
        for interface_ref in thermal_losses:
            thermal_resistances_dict[interface_ref] = 1 / thermal_losses[interface_ref]
        return thermal_resistances_dict
    
    def interface_thermal_resistance(self,  zone_name1: str, zone_name2: str):
        """
        _summary_

        :param zone_name1: _description_
        :type zone_name1: str
        :param zone_name2: _description_
        :type zone_name2: str
        :return: _description_
        :rtype: _type_
        """
        interface_ref = (zone_name1, zone_name2)
        thermal_losses = self.interfaces_thermal_losses()
        return 1 / thermal_losses[interface_ref]

    def interface_all_facets_thermal_capacitances(self, zone_name1: str, zone_name2: str) -> Dict[LayeredInterface, List[float]]:
        """
        Return the thermal capacitances related to layered interface for the facet defined by 2 zones

        :param zone_name1: name of zone1
        :type zone_name1: str
        :param zone_name2: name of zone2
        :type zone_name2: str
        :return: all thermal capacitances related to layered interface ordered from zone1 to zone2
        :rtype: Dict[LayeredInterface, List[float]]
        """
        layer_interface_capacitances: Dict[LayeredInterface, List[float]] = dict()
        layered_facets = self.layered_facets[(zone_name1, zone_name2)]
        for layered_facet in layered_facets:
            layer_interface_capacitances[layered_facet] = layered_facet.thermal_capacitances
        return layer_interface_capacitances
    
    def first_facet_interface_thermal_capacitance(self, zone_name1: str, zone_name2: str) -> float:
        """
        Return the first thermal capacitance related to layered interface for the facet defined by 2 zones

        :param zone_name1: name of zone1
        :type zone_name1: str
        :param zone_name2: name of zone2
        :type zone_name2: str
        :return: the thermal capacitance closest to zone1
        :rtype: float
        """
        return list(self.interface_all_facets_thermal_capacitances(zone_name1, zone_name2).items())[0][1]

    def _generate_RC_interfaces_mesh(self, thermal_network: ThermalNetwork):
        """
        generate RC model for each layered interface

        :param net: _description_
        :type net: ThermalNetwork
        """
        for facet_ref in self.layered_facets:
            from_zone_name, to_zone_name = facet_ref
            from_zone = self._zones[from_zone_name]
            to_zone = self._zones[to_zone_name]
            for j, layered_interface in enumerate(self.layered_facets[facet_ref]):
                thermal_resistances=layered_interface.thermal_resistances
                thermal_capacitances=layered_interface.thermal_capacitances
                thermal_network.R(fromT=from_zone.T, toT='T'+facet_ref[0]+str(j)+'_'+str(0), value=thermal_resistances[0])
                for i, layer in enumerate(layered_interface.layers):
                    if layer['material'] == 'air':
                        thermal_network.R(fromT='T'+facet_ref[0]+str(j)+'_'+str(i), toT='T'+facet_ref[0]+str(j)+'_'+str(i+1), value=thermal_resistances[i])
                    else:
                        thermal_network.R(fromT='T'+facet_ref[0]+str(j)+'_'+str(i), toT='T'+facet_ref[0]+str(j)+'_'+str(i+1)+'m', value=thermal_resistances[i]/2)
                        thermal_network.R(fromT='T'+facet_ref[0]+str(j)+'_'+str(i+1)+'m', toT='T'+facet_ref[0]+str(j)+'_'+str(i+1), value=thermal_resistances[i]/2)
                        thermal_network.C(toT='T'+facet_ref[0]+str(j)+'_'+str(i+1)+'m', value=thermal_capacitances[i])
                thermal_network.R(fromT='T'+facet_ref[0]+str(j)+'_'+str(len(layered_interface.layers)), toT=to_zone.T, value=thermal_resistances[len(layered_interface.layers)])
        for facet_ref in self._component_interfaces:
            for component_interface in self._component_interfaces[facet_ref]:
                thermal_resistance=component_interface.thermal_resistance
                thermal_network.R(fromT=from_zone.T, toT=to_zone.T, value=thermal_resistance)

    def make(self, order: int, air_flows: Dict[str, float]) -> Tuple[StateModel, StateModel]:
        """
        generate the thermal and CO2 state models

        :param input_zone_airflows: dictionary containing the permanent values of input air flows for each input zone.
        :type input_airflows: Dict[str, float]
        :result: the optionally reduced order thermal state model and the CO2 state model
        :rtype: Tuple[STATE_MODEL, STATE_MODEL]
        """

        if not self.has_been_propagated:
            for zone_name in self._zones:
                zone = self._zones[zone_name]
                for airflow in zone._connected_in_airflows:
                    airflow._solve_causality()

            selected_zone_names = list()
            for zone_name in self._airflow_network.nodes:
                if not self._zones[zone_name].is_known:
                    selected_zone_names.append(zone_name)
            subgraph = self._airflow_network.subgraph(selected_zone_names)
            if len([cycle for cycle in networkx.simple_cycles(subgraph)]):
                raise ValueError('Airflows lead to loop')

            zones_airflows = []
            for zone_name in self._zones:
                zone = self._zones[zone_name]
                if zone.is_simulated or zone.is_known:
                    zones_airflows.append(zone)
            zones_airflows_to_be_propagated = zones_airflows.copy()
            while len(zones_airflows_to_be_propagated) > 0:
                    zone = zones_airflows_to_be_propagated.pop(-1)
                    if not zone.propagate():
                        zones_airflows_to_be_propagated.insert(0, zone)
            self.has_been_propagated = True
            
            self._nlis = list()
            for zone_name in self._zones:
                zone = self._zones[zone_name]
                if zone.is_simulated:
                    for nli in zone._propagated_airflow:
                        if nli not in self._nlis:
                            self._nlis.append(nli)
        
        air_properties = Site.library.get('air')
        rhoCp_air = air_properties['density'] * air_properties['Cp']
        for zone_name in self._zones: # create variables for thermal zones
            zone = self._zones[zone_name]
            if not zone.is_simulated:  # zone_name not in self.simulated_zone_names: # Not simulated zone
                self._thermal_network.T(name=zone.T, causality=CAUSALITY.IN)  # create a temperature node
            else: # simulated zone
                self._thermal_network.T(zone.T, causality=CAUSALITY.OUT)
                self._thermal_network.HEAT(T=zone.T, name=zone.HEAT)
                Cair = rhoCp_air * zone._volume
                self._thermal_network.C(toT=zone.T, name=zone.C,  value=Cair)  # create the Cair capacitance for each thermal zone
        self._generate_RC_interfaces_mesh(self._thermal_network)

        for zone_name in self._zones:
            zone = self._zones[zone_name]
            for connected_zone in zone.output_connected_zones:
                Q = 0
                for airflow in zone._propagated_airflow:
                    if airflow not in self._nlis:
                        raise ValueError('airflow %s must be provided' % airflow)
                    if airflow in connected_zone._propagated_airflow:
                        Q += connected_zone._propagated_airflow[airflow] * air_flows[airflow]
                if Q > 0:
                    self.thermal_network.R(fromT= zone._air_temperature_name, toT=connected_zone._air_temperature_name, name='Rv%s_%s' % (zone_name, connected_zone._name),value=rhoCp_air * Q)

        return  self.thermal_network.state_model(order), self.CO2_state_model(air_flows)

    def assemble(self, thermal_state_model: StateModel,  co2_state_model: StateModel) -> StateModel:
        n_th = len(thermal_state_model['X'])
        m_th = len(thermal_state_model['U_Tin']) + len(thermal_state_model['U_heat'])
        p_th = len(thermal_state_model['Y'])
        n_CO2 = len(co2_state_model['X'])
        m_CO2 = len(co2_state_model['U_CO2']) + len(co2_state_model['U_prod'])
        p_CO2 = len(co2_state_model['Y'])
        X = []
        X.extend(thermal_state_model['X'])
        X.extend(co2_state_model['X'])
        U = []
        U.extend(thermal_state_model['U_Tin'])
        U.extend(thermal_state_model['U_heat'])
        U.extend(co2_state_model['U_CO2'])
        U.extend(co2_state_model['U_prod'])
        Y = []
        Y.extend(thermal_state_model['Y'])
        Y.extend(co2_state_model['Y'])
        B_th = numpy.concatenate((thermal_state_model['B_Tin'], thermal_state_model['B_heat']), axis=1)
        D_th = numpy.concatenate((thermal_state_model['D_Tin'], thermal_state_model['D_heat']), axis=1)
        B_CO2 = numpy.concatenate((co2_state_model['B_CO2'], co2_state_model['B_prod']), axis=1)
        D_CO2 = numpy.concatenate((co2_state_model['D_CO2'], co2_state_model['D_prod']), axis=1)
        A_th = thermal_state_model['A']
        A_CO2 = co2_state_model['A']
        A = numpy.concatenate((numpy.concatenate((A_th, numpy.zeros((n_th, n_CO2))), axis=1), numpy.concatenate((numpy.zeros((n_CO2, n_th)), A_CO2), axis=1)), axis=0)
        B = numpy.concatenate((numpy.concatenate((B_th, numpy.zeros((n_th, m_CO2))), axis=1), numpy.concatenate((numpy.zeros((n_CO2, m_th)), B_CO2), axis=1)), axis=0)
        C_th = thermal_state_model['C']
        C_CO2 = co2_state_model['C']
        C = numpy.concatenate((numpy.concatenate((C_th, numpy.zeros((p_th, n_CO2))), axis=1), numpy.concatenate((numpy.zeros((p_CO2, n_th)), C_CO2), axis=1)), axis=0)
        D = numpy.concatenate((numpy.concatenate((D_th, numpy.zeros((p_th, m_CO2))), axis=1), numpy.concatenate((numpy.zeros((p_CO2, m_th)), D_CO2), axis=1)), axis=0)
        UNL = self._nlis
        return {'A': A, 'B': B, 'C': C, 'D': D, 'Y': Y, 'X': X, 'U': U, 'UNL': UNL, 'type': 'differential'}

    def CO2_state_model(self, provided_input_airflow_values: Dict[str, float]) -> StateModel:
        """
        Generate the state model representing the CO2 evolution

        :param provided_input_airflow_values: input permanent airflow values of the site
        :type provided_input_airflow_values: Dict[str, float]
        :return: State space model for the CO2
        :rtype: STATE_MODEL
        """
        zone_CO2_state_models: StateModel = dict()
        CO2_productions = list()
        for zone_name in self._zones:
            zone = self._zones[zone_name]
            if zone.is_simulated:  #_CO2_zone:
                CO2_productions.append(zone._CO2_production_name)  # breath
                zone_CO2_state_models[zone._CO2_concentration_name] = dict()  # CO2 model for the current zone
                zone_CO2_state_models[zone._CO2_concentration_name][zone._CO2_concentration_name] = 0
                for airflow_connected_input_zone in zone.input_connected_zones:
                    #in_between_zone_airflow = 0
                    for resulting_airflow_name in zone._propagated_airflow: #[airflow_connected_input_zone.airflow_name]:
                        in_between_zone_airflow = provided_input_airflow_values[resulting_airflow_name] / zone._volume
                    zone_CO2_state_models[zone._CO2_concentration_name][airflow_connected_input_zone._CO2_concentration_name] = in_between_zone_airflow
                
                for airflow_connected_output_zone in zone.output_connected_zones:
                    in_between_zone_airflow = 0
                    for resulting_airflow_name in airflow_connected_output_zone._propagated_airflow: #[zone.airflow_name]:
                        #print('>', airflow_connected_output_zone, resulting_airflow_name)
                        in_between_zone_airflow += provided_input_airflow_values[resulting_airflow_name] / zone._volume
                        #print(in_between_zone_airflow)
                    zone_CO2_state_models[zone._CO2_concentration_name][zone._CO2_concentration_name] -= in_between_zone_airflow 
                    zone_CO2_state_models[zone._CO2_concentration_name][zone._CO2_production_name] = 1/zone._volume
                CO2_state_variables = [zone_CO2_concentration for zone_CO2_concentration in zone_CO2_state_models]
                CO2_input_variables = []
                for zone_name in self._zones:
                    zone = self._zones[zone_name]
                    if zone.is_known:  #.has_CO2_concentration_to_be_known:
                        CO2_input_variables.append(zone._CO2_concentration_name)
                A = numpy.zeros((len(CO2_state_variables), len(CO2_state_variables)))
                B_CO2 = numpy.zeros((len(CO2_state_variables), len(CO2_input_variables)))
                B_prod = numpy.zeros((len(CO2_state_variables), len(CO2_productions)))
                C = numpy.eye(len(CO2_state_variables))
                D_CO2 = numpy.zeros((len(CO2_state_variables), len(CO2_input_variables)))
                D_prod = numpy.zeros((len(CO2_state_variables), len(CO2_productions)))
                for concentration in zone_CO2_state_models:
                    i = CO2_state_variables.index(concentration)
                    for variable in zone_CO2_state_models[concentration]:
                        if variable in CO2_state_variables:
                            j = CO2_state_variables.index(variable)
                            A[i,j] += zone_CO2_state_models[concentration][variable]
                        elif variable in CO2_input_variables:
                            j = CO2_input_variables.index(variable)
                            B_CO2[i,j] = zone_CO2_state_models[concentration][variable]
                        elif variable in CO2_productions:
                            j = CO2_productions.index(variable)
                            B_prod[i,j] = zone_CO2_state_models[concentration][variable]
        return {'A': A, 'B_CO2': B_CO2, 'B_prod': B_prod, 'C': C, 'D_CO2': D_CO2, 'D_prod': D_prod, 'Y': CO2_state_variables, 'X': CO2_state_variables, 'U_CO2': CO2_input_variables, 'U_prod': CO2_productions, 'type': 'differential'}

    def draw_thermal_net(self):
        """
        draw digraph of the thermal network (use matplotlib.show() to display)
        """
        self._thermal_network.draw()

    def print_state_model(self, state_model: StateModel):
        """
        Print on screen the state space model or the static model (no capacitance)
        :param state_model: the state model to be printed
        :type state_model: STATE_MODEL
        """
        print('State Model:')
        print('d/dt', state_model['X'],'='), print(state_model['A']), print(state_model['X'], '+')
        if 'U_CO2' in state_model:
            print(state_model['B_CO2']), print(state_model['U_CO2'], '+')
            print(state_model['B_prod']), print(state_model['U_prod'])
        else:
            print(state_model['B_Tin']), print(state_model['U_Tin'], '+')
            print(state_model['B_heat']), print(state_model['U_heat'])
        print()
        print('Output equation')
        print(state_model['Y'], '='), print(state_model['C']), print(state_model['X'], '+')
        if 'U_CO2' in state_model:
            print(state_model['D_CO2']), print(state_model['U_CO2'], '+')
            print(state_model['D_prod']), print(state_model['U_prod'])
        else:
            print(state_model['D_Tin']), print(state_model['U_Tin'], '+')
            print(state_model['D_heat']), print(state_model['U_heat'])

    def print_CO2_characteristics(self, CO2_state_model: StateModel):
        """
        print the characteristics of the provided CO2 state model
        :param CO2_state_model: the CO2 state model
        :type CO2_state_model: STATE_MODEL
        """
        time_constants_in_hours = [numpy.real(-1 / l) for l in numpy.linalg.eigvals(CO2_state_model['A'])]
        time_constants_in_hours.sort(reverse=True)
        print('* Time constants:')
        print("\n".join([timemg.time_from_seconds_day_hours_minutes(t) for t in time_constants_in_hours]))
        print('* Static gains:')
        print(CO2_state_model['Y'])
        print('=')
        print(-CO2_state_model['C']*numpy.linalg.inv(CO2_state_model['A'])*CO2_state_model['B_prod']+CO2_state_model['D_prod'])
        print('*')
        print(CO2_state_model['U_prod'])
        print('+')
        print(-CO2_state_model['C']*numpy.linalg.inv(CO2_state_model['A'])*CO2_state_model['B_CO2']+CO2_state_model['D_CO2'])
        print('*')
        print(CO2_state_model['U_CO2'])

    def print_thermal_characteristics(self, thermal_state_model: StateModel):
        """
        print the characteristics of the provided thermal state model
        :param thermal_state_model: the thermal state model
        :type thermal_state_model: STATE_MODEL
        """
        time_constants_in_hours = [numpy.real(-1 / l) for l in numpy.linalg.eigvals(thermal_state_model['A'])]
        time_constants_in_hours.sort(reverse=True)
        print('* Time constants:')
        print("\n".join([timemg.time_from_seconds_day_hours_minutes(t) for t in time_constants_in_hours]))
        print('* Static gains:')
        print(thermal_state_model['Y'])
        print('=')
        alpha_coef = -thermal_state_model['C']*numpy.linalg.inv(thermal_state_model['A'])*thermal_state_model['B_heat']+thermal_state_model['D_heat'] 
        print(alpha_coef)
        print('*')
        print(thermal_state_model['U_heat'])
        print('+')
        beta_i_coefs = -thermal_state_model['C']*numpy.linalg.inv(thermal_state_model['A'])*thermal_state_model['B_Tin']+thermal_state_model['D_Tin']
        print(beta_i_coefs)
        print('*')
        print(thermal_state_model['U_Tin'])
        print('* transmission coefficients:')
        for i in range(len(thermal_state_model['U_Tin'])):
            print(thermal_state_model['U_Tin'][i], ': %.2f W/K' % (beta_i_coefs[i] / alpha_coef[0]))

        A = numpy.array(thermal_state_model['A']).astype(numpy.float64)
        B = numpy.array(numpy.hstack((thermal_state_model['B_Tin'], thermal_state_model['B_heat']))).astype(numpy.float64)
        C = numpy.array(thermal_state_model['C']).astype(numpy.float64)
        D = numpy.array(numpy.hstack((thermal_state_model['D_Tin'], thermal_state_model['D_heat']))).astype(numpy.float64)
        U = thermal_state_model['U_Tin'].copy()
        U.extend(thermal_state_model['U_heat'])
        control_state_space = control.StateSpace(A, B, C, D)
        time_response_data = control.step_response(control_state_space)
        for i, output_name in enumerate(thermal_state_model['Y']):
            temperatures = []
            powers = []
            _, axis = plt.subplots(2)
            for j in range(len(U)):
                if U[j][0] == 'T':
                    figure_number = 0
                    temperatures.append('Delta' + U[j])
                else:
                    figure_number = 1
                    powers.append('Delta' + U[j])
                if len(time_response_data.outputs.shape)==1:
                    axis[figure_number].plot([t/3600 for t in time_response_data.time], time_response_data.outputs)
                else:    
                    axis[figure_number].plot([t/3600 for t in time_response_data.time], time_response_data.outputs[i][j])
            axis[1].set_xlabel('time in hours')
            axis[0].set_ylabel('temperature %s'%(output_name))
            axis[1].set_ylabel('power %s'%(output_name))
            axis[0].legend(temperatures)
            axis[1].legend(powers)

    def draw_airflow_net(self):
        """
        draw digraph of the airflow network (use matplotlib.show() to display)
        """

        pos=networkx.shell_layout(self._airflow_network)
        node_colors = list()
        for zone_name in self._zones:
            if self._zones[zone_name].is_known:
                node_colors.append('blue')
            elif self._zones[zone_name].is_simulated:
                node_colors.append('pink')
            else:
              node_colors.append('yellow')
        labels = dict()
        for node in self._airflow_network.nodes:
            label = '\n' + str(self._zones[node]._propagated_airflow)
            labels[node] = label
        plt.figure()
        networkx.draw(self._airflow_network, pos, with_labels=True, edge_color='black', width=1, linewidths=1, node_size=500, font_size='medium', node_color=node_colors, alpha=1)
        networkx.drawing.draw_networkx_labels(self, pos,  font_size='x-small', verticalalignment='top', labels=labels)

    def __str__(self) -> str:
        """
        :return: string depicting the site
        :rtype: str
        """
        string='Site has been propagated: %r with thermal state space order: %i\n' % (self.has_been_propagated, self.thermal_state_system_order if self.thermal_state_system_order is not None else -1)
        for zone in self._zones:
            string += self._zones[zone].__str__()
        facet_refs=set(self.layered_facets.keys()).union(set(self._component_interfaces.keys()))
        for facet_ref in facet_refs:
            string += '\nside (%s, %s > %.2fW/K):\n' % (facet_ref[0], facet_ref[1], self.interfaces_thermal_losses()[facet_ref])
            if facet_ref in self.layered_facets:
                for composition in self.layered_facets[facet_ref]:
                    string += '\t- ' + composition.__str__() + '\n'
            if facet_ref in self._component_interfaces:
                for component in self._component_interfaces[facet_ref]:
                    string += '\t- ' + component.__str__() + '\n'
        return string
