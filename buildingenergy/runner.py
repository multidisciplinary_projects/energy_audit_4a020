"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module is a top level module in the way it connects measurement data, parameters and models.
"""


# from abc import ABC, abstractmethod
# from operator import matmul
from typing import Dict, List, Tuple, TypeVar
import numpy
import numpy.linalg
import time
import statistics
import prettytable
import SALib.sample.morris
import SALib.analyze.morris
import plotly.express
import scipy.optimize

from buildingenergy.data import Data
from buildingenergy.model import Model
from buildingenergy.parameters import ParameterSet
from buildingenergy import timemg


StateModel = TypeVar('StateModel')  # dict[str, str | List[str] | numpy.array]


def avg_error(values: List[float], ref_values: List[float]) -> float:
    return sum([abs(values[0][i]-ref_values[i]) for i in range(len(ref_values))]) / len(values)

class Runner:
    """
    Runner is the main class for simulating a site model: it connects measurement data, parameters and models.
    """

    def __init__(self,model_data_bindings: 'Tuple[str, str | List[str]]', parameter_set: ParameterSet=None, data: Data=None, model: Model=None):
        """
        Initialize a runner i.e. a runnable simulator with the design of the following objects:
        - a parameter set, containing the names og nonlinear inputs (some airflows) with their bounds, to approximate the state model
        - a (state) model, with identified nonlinear input variables 
        - a data set, which contains all the data bounded to yield the state
        :param model_data_bindings: connect a model variable to one or several data. If several data are provided, they are summed up. Instead of data, parameterized data can be provided.
        :type model_data_bindings: Tuple[str, str | List[str]])
        """        
        super().__init__()
        self.Us = None
        self.Ys = None
        self._model_data_bindings = dict()  # dict[str, List[str] | str]

        for binding in model_data_bindings:
            if type(binding[1]) == str:
                self._model_data_bindings[binding[0]] = [binding[1]]
            else:
                self._model_data_bindings[binding[0]] = binding[1]

        if parameter_set is not None:
            self.parameter_set = parameter_set
        else:
            self.parameter_set = self.make_parameter_set()
        if data is not None:
            self.data = data
        else:
            self.data = self.make_invariant_data()
        self.make_parameterized_data(self.parameter_set, self.data)

        if model is not None:
            self.model = model
        else:
            self.model = self.make_model(self.parameter_set)
        print('*', end='')

        try:
            self.linear_input_names = self.model.linear_input_names
        except:
            self.linear_input_names = list()
        self.uncontrollable_linear_input_names = []
        for nli_name in self.linear_input_names:
            if nli_name in self._model_data_bindings:
                self.uncontrollable_linear_input_names.append(nli_name)
        self.controllable_linear_input_names = []
        for nli_name in self.linear_input_names:
            if nli_name not in self.uncontrollable_linear_input_names:
                self.controllable_linear_input_names.append(nli_name)

        self.nonlinear_input_names = self.model.time_varying_parameter_names
        self.uncontrollable_nonlinear_input_names = []
        for nli_name in self.nonlinear_input_names:
            if nli_name in self._model_data_bindings:
                self.uncontrollable_nonlinear_input_names.append(nli_name)
        self.controllable_nonlinear_inputs_names = []
        for nli_name in self.nonlinear_input_names:
            if nli_name not in self.uncontrollable_nonlinear_input_names:
                self.controllable_nonlinear_inputs_names.append(nli_name)

        self.output_names = self.model.output_names

        print('Linear inputs')
        print('- uncontrollable: ', end='')
        print(', '.join([v for v in self.uncontrollable_linear_input_names]))
        print('- controllable: ', end='')
        print(', '.join([v for v in self.controllable_linear_input_names]))
        print('Nonlinear inputs')
        print('- uncontrollable: ', end='')
        print(', '.join([v for v in self.uncontrollable_nonlinear_input_names]))
        print('- controllable: ', end='')
        print(', '.join([v for v in self.controllable_nonlinear_inputs_names]))
        print('Outputs:', end='')
        print(', '.join([v for v in self.output_names]))

    def _get_model_variable_value(self, model_variable_name: str, k: int) -> float:
        """
        Return from data at hour k, to the specified model variable. It's achieved thanks to the bindings declared in the initializer.

        :param model_variable_name: name of the model
        :type model_variable_name: str
        :param k: hour index
        :type k: int
        :return: the model variable value at hour k deduce from data bindings with model variables
        :rtype: float
        """
        bound_data = self._model_data_bindings[model_variable_name]
        if len(bound_data) == 1:
            return self.data(bound_data[0], k)
        else:
            return sum([self.data(data, k) for data in bound_data])

    def update_nli(self, param: ParameterSet, k: int) -> ParameterSet:
        """
        Modify the values of linear inputs at each hour k according to the binding between nonlinear inputs and (possibly Parameterized) data

        :param param: param_nli_set of the model
        :type param: ParamNliSet
        :param k: hour index
        :type k: int
        :return: the updated param_nli_set
        :rtype: ParamNliSet
        """
        nonlinear_inputs = {name: self._get_model_variable_value(name, k) for name in self.nonlinear_input_names}
        return param.update(nonlinear_inputs)

    def X_0(self, sampled_state_model: StateModel, U: numpy.array):
        """
        Compute the initial state for getting a permanent behavior at start i.e. X_{k=1} = X_{k=0} = X_0

        :param sampled_state_model: the current sampled state model
        :type sampled_state_model: StateModel
        :param U: the input values at hour 0
        :type U: numpy.array
        :return: a vector of values corresponding to U, as a numpy.array matrix (shaped as a vector)
        :rtype: numpy.array
        """
        A, B, I = sampled_state_model['A'], sampled_state_model['B'], numpy.eye(len(sampled_state_model['X']))
        return numpy.matmul(numpy.matmul(numpy.linalg.inv(I - A), B), U)

    def get_uncontrolled_linear_inputs(self, k: int) -> Dict[str, float]:
        """
        Return the uncontrolled linear inputs: a vector of data must then be bound to this model variables

        :param k: hour index
        :type k: int
        :return: the uncontrolled linear input values at time k
        :rtype: Dict[str, float]
        """
        uncontrolled_linear_inputs = dict()
        for input in self.uncontrollable_linear_input_names:
            uncontrolled_linear_inputs[input] = self._get_model_variable_value(input, k)
        return uncontrolled_linear_inputs

    def get_uncontrolled_nli(self, k: int) -> Dict[str, float]:
        """
        Return the uncontrolled nonlinear input value at time k

        :param k: hour index
        :type k: int
        :return: the uncontrolled nonlinear input values at time k, organized in a vector corresponding to the U vector
        :rtype: Dict[str, float]
        """
        uncontrolled_nli = dict()
        for input in self.uncontrollable_nonlinear_input_names:
            uncontrolled_nli[input] = self._get_model_variable_value(input, k)
        return uncontrolled_nli

    def simulate(self, from_hour: int=0, to_hour:int=None, from_hour_state=None) -> Tuple[Dict[str, List[float]], numpy.array]:
        """
        Simulate the model with the provided data and parameters for all the hours
        """
        Us: numpy.array = None
        Xs: numpy.array = None
        Ys: numpy.array = None
        k = from_hour
        if to_hour is None:
            to_hour = self.data.number_of_hours

        self.parameter_set.update(self.get_uncontrolled_nli(k))
        _sampled_state_model = self.model.make_sampled_state_model(self.parameter_set)
        _U = self.U(k, _sampled_state_model)
        if from_hour_state is not None:
            _X = from_hour_state
        else:
            _X = self.X_0(_sampled_state_model, _U)
        _Y = self.Y(k, _sampled_state_model, _X,  _U)
        Us = _U
        #Xs = _X
        Ys = _Y

        for k in range(k+1, to_hour):
            _X = self.X_kp1(k + 1, _sampled_state_model, _X, _U)
            self.parameter_set.update(self.get_uncontrolled_nli(k))
            _sampled_state_model = self.model.make_sampled_state_model(self.parameter_set)
            _U = self.U(k, _sampled_state_model)
            _Y = self.Y(k, _sampled_state_model, _X, _U)
            Us = numpy.hstack((Us, _U))
            #Xs = numpy.hstack((Xs, _X))
            Ys = numpy.hstack((Ys, _Y))
            self.Us = Us
            self.Ys = Ys
        print()
        return {self.output_names[i]: Ys[i,:].tolist()[0] for i in range(len(self.output_names))}, _X
    
    def memorize(self, suffix: str, inputs=False):
        if inputs and self.Us is not None:
            for i in range(len(self.linear_input_names)):
                __Ui = self.Us[i,:].tolist()[0]
                self.data.add_external_variable(self.linear_input_names[i] + suffix, __Ui)

        if self.Ys is not None:
            for i in range(len(self.output_names)):
                __Yi = self.Ys[i,:].tolist()[0]
                self.data.add_external_variable(self.output_names[i] + suffix, __Yi)

    def U(self, k: int, state_model: StateModel) -> numpy.array:
        """
        Return the vector U of the state model as a numpy.array matrix (shaped as a vector)

        :param k: hour index
        :type k: int
        :param state_model: the sampled state model
        :type state_model: StateModel
        :return: the U vector of the state model
        :rtype: numpy.array
        """
        U = list()
        uncontrollable_linear_inputs = self.get_controlled_linear_inputs(k, self.uncontrollable_linear_input_names)
        for input in self.linear_input_names:
            if input in self.uncontrollable_linear_input_names:
                U.append(self._get_model_variable_value(input, k))
            else:
                U.append(uncontrollable_linear_inputs[input])
        _result = numpy.array([self._get_model_variable_value(name, k) for name in self.linear_input_names])
        return _result.reshape(len(U), 1)

    def X_kp1(self, k: int, state_model: StateModel, X_k: numpy.array, U_k: numpy.array):
        """
        Compute the state vector for the next hour, thanks to the state model

        :param k: hour index
        :type k: int
        :param state_model: the state model (X_kp1 = A X_k + B U_k)
        :type state_model: StateModel
        :param X_k: the state vector at hour k
        :type X_k: numpy.array
        :param U_k: the input vector at hour k
        :type U_k: numpy.array
        :return: the state vector for the next hour
        :rtype: numpy.array shaped as a vector
        """
        return numpy.matmul(state_model['A'], X_k) + numpy.matmul(state_model['B'], U_k)

    def Y(self, k: int, state_model: StateModel, X_k: numpy.array, U_k: numpy.array):
        return numpy.matmul(state_model['C'], X_k) + numpy.matmul(state_model['D'], U_k)

    def plot(self):
        """
        plot the data
        """
        self.data.plot()

    def save(self, file_name: str = 'results.csv', selected_variables: List[str] = None):
        """
        save the selected data in a csv file

        :param file_name: name of the csv file, defaults to 'results.csv' saved in the 'results' folder specified in the setup.ini file
        :type file_name: str, optional
        :param selected_variables: list of the variable names to be saved (None for all), defaults to None
        :type selected_variables: List[str], optional
        """
        self.data.save(file_name, selected_variables)
        
    def sensitivity(self, number_of_trajectories: int=10, number_of_levels: int=4, order: int=None):
        """Perform a Morris sensitivity analysis for average simulation error both for indoor temperature and CO2 concentration. It returns 2 plots related to each output variable. mu_star axis deals with the simulation variation bias, and sigma for standard deviation of the simulation variations wrt to each parameter.

        :param number_of_trajectories: [description], defaults to 100
        :type number_of_trajectories: int, optional
        :param number_of_levels: [description], defaults to 4
        :type number_of_levels: int, optional
        :return: a dictionary with the output variables as key and another dictionary as values. It admits 'names', 'mu', 'mu_star', 'sigma', 'mu_star_conf' as keys and corresponding values as lists
        :rtype: dict[str,dict[str,list[float|str]]]
        """
        
        problem = dict()
        adjustable_regular_parameters = self.parameter_set.adjustables_names
        problem['num_vars'] = len(adjustable_regular_parameters)
        problem['names'] = []
        problem['bounds'] = []
        for parameter_name in adjustable_regular_parameters:
            problem['names'].append(parameter_name)
            problem['bounds'].append(self.parameter_set.bounds(parameter_name))

        print('# Generating parameter values')
        parameter_value_sets = SALib.sample.morris.sample(problem, number_of_trajectories, num_levels=number_of_levels)
        
        outputs_value_sets: Dict[str, List[List[float]]] = {output_name: [] for output_name in self.output_names}
        
        for i, parameter_set in enumerate(parameter_value_sets):
            print('%i/%i'%(i, len(parameter_value_sets)-1) )
            print('.', end='')
            parameter_values = {adjustable_regular_parameters[i]: parameter_set[i] for i in range(len(adjustable_regular_parameters))}
            self.parameter_set.update(parameter_values)
            output_name_values, X = self.simulate()
            for output_name in self.output_names:
                outputs_value_sets[output_name].append(Runner.avg_error(output_name_values[output_name], self.data(self._model_data_bindings[output_name][0])))
        print()
        print('Analyzing simulation results')
        results = dict()
        for output_name in self.output_names:
            print('\n* %s' % output_name)
            results[output_name] = SALib.analyze.morris.analyze(problem, parameter_value_sets, numpy.array(outputs_value_sets[output_name], dtype=float), conf_level=0.95, print_to_console=True, num_levels=number_of_levels)
            fig = plotly.express.scatter(results[output_name], x='mu_star', y='sigma', text=adjustable_regular_parameters, title=output_name)
            fig.show()
        return results
    
    def fit(self, learning_ratio: float=2./3., suffix: str='*', strategy: int='best1bin', maxiter: int=100, popsize: int=50, tol: float=0.01, mutation: Tuple[float, float]=(0.5, 1), recombination: float=0.7, seed: int=None, callback: float=None, updating='deferred', polish=True, workers=-1, init: str='latinhypercube') -> dict:
        """Search for best parameter values taking into account parameter bounds specified in the model.

        :param strategy: see https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.optimize.differential_evolution.html, defaults to 'best1bin'
        :type strategy: int, optional
        :return: result with 'name', 'bounds', 'initial', 'initial error', 'final', 'final error', 'duration' (and 'Tins_mu/sigma', 'Cins_mu/sigma' if sensitivity analysis is performed) as keys and values according to parameter names
        :rtype: dict[str,float|dict[str,list[str|float]]]
        """
    
        results = dict()
        resolution = self.parameter_set.resolution
        results['name'] = self.parameter_set.adjustables_names
        adjustables_bounds = [self.parameter_set.bounds(p) for p in self.parameter_set.adjustables_names]
        adjustables_bounds_str = ['(%.5f,%.5f)' % t for t in adjustables_bounds]
        results['bounds'] = adjustables_bounds
        initial_parameter_values = [self.parameter_set(p) for p in self.parameter_set.adjustables_names]
        results['initial values'] = initial_parameter_values
        k_validation = int(self.data.number_of_hours * learning_ratio)
        
        # output_name_values = {output_name: self._data(self._model_data_bindings[output_name][0]) for output_name in self.output_names}
        # stdev_output_values: Dict[str, float] = {output_name: statistics.stdev(output_name_values[output_name]) for output_name in self.output_names}
        
        simulation_error_learning = SimulationError(self, 0, k_validation)
        simulation_error_global = SimulationError(self, 0, self.data.number_of_hours)
        
        results['initial error'] = simulation_error_learning.error(initial_parameter_values)
        
        init_time = time.time()
        # see https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.optimize.differential_evolution.html
        result = scipy.optimize.differential_evolution(simulation_error_learning.error, [(0, resolution) for _ in range(len(results['name']))],
                                                       strategy=strategy, maxiter=maxiter, popsize=popsize, tol=tol, mutation=mutation, recombination=recombination, seed=seed, callback=callback, disp=True, updating=updating, polish=polish, init=init, workers=workers, x0=self.parameter_set.adjustables_fingerprint, integrality= [True for _ in range(len(results['name']))]) #adjustables_bounds
        #(func, bounds, args=(), strategy='best1bin', maxiter=1000, popsize=15, tol=0.01, mutation=(0.5, 1), recombination=0.7, seed=None, callback=None, disp=False, polish=True, init='latinhypercube', atol=0, updating='immediate', workers=1, constraints=(), x0=None, *, integrality=None, vectorized=False)[source]#
        best_parameter_levels = tuple(result.x)
        self.parameter_set.adjustables_fingerprint = best_parameter_levels
        results['duration'] = time.time() - init_time
        results['final values'] =  self.parameter_set.adjustables_values
        #self.param_nli_set.update({self.param_nli_set.adjustables[i]: best_parameter_levels[i] for i in range(len(self.param_nli_set.adjustables))})
        results['learning error'] = simulation_error_learning.error(best_parameter_levels)
        results['validation error'] = simulation_error_global.error(best_parameter_levels)
        
        contact = []
        for parameter_name in self.parameter_set.adjustables_names:
            if abs(self.parameter_set(parameter_name)-self.parameter_set.bounds(parameter_name)[0]) < 1e-2 * abs(self.parameter_set.bounds(parameter_name)[0]):
                contact.append('<')
            elif abs(self.parameter_set(parameter_name)-self.parameter_set.bounds(parameter_name)[1]) < 1e-2 * abs(self.parameter_set.bounds(parameter_name)[1]):
                contact.append('>')
            else:
                contact.append('-')
        
        column_names = ['name', 'initial values', 'final values', 'bounds', 'contact']
        pretty_table = prettytable.PrettyTable()
        pretty_table.set_style(prettytable.MSWORD_FRIENDLY)
        for column_name in column_names:
            if column_name == "bounds":
                pretty_table.add_column('bounds', adjustables_bounds_str)
            elif column_name == 'contact':
                pretty_table.add_column('contact', contact)
            else:
                pretty_table.add_column(column_name, [results[column_name][i] for i in range(len(self.parameter_set.adjustables_names))])
            pretty_table.float_format[column_name] = ".4"
            
        print(pretty_table)
        print('Error from %f to %f (duration %s seconds), with validation error %f' % (results['initial error'], results['learning error'], results['duration'], results['validation error']))

        output_name_values, X = self.simulate()
        for output_name in output_name_values:
            self.data.add_external_variable(output_name+suffix, output_name_values[output_name])
        filename = 'parameters_order%i_date%s' % (len(X), timemg.current_stringdate(date_format='%d-%m-%Y_%H:%M:%S'))
        self.parameter_set.save(filename)
        return results

    def make_parameter_set(self) -> ParameterSet:
        """
        An abstract method that must be implemented: it has to generate a parameter set.

        :return: a parameter set
        :rtype: Param
        """
        pass

    def make_invariant_data(self) -> Data:
        """
        An abstract method that must be implemented: it has to generate an invariant set of data i.e. not parameterized.

        :return: a data container
        :rtype: Data
        """
        pass

    def make_parameterized_data(self, data: Data, parameter_set: ParameterSet):
        """
        An abstract method that must be implemented: it has to instantiate the relevant ParameterizedData objects.

        :param data: a data container
        :type data: Data
        :param param: a parameter set
        :type param: Param
        """
        pass

    def make_model(self, parameter_set: ParameterSet) -> Model:
        pass

    def get_controlled_linear_inputs(self, k: int, linear_input_names: List[str]) -> Dict[str, float]:
        pass

    def get_controlled_nonlinear_inputs(self, k: int, nonlinear_input_names: List[str]) -> Dict[str, float]:
        pass


class ExtraRunner(Runner):

    def __init__(self, bindings: 'Tuple[str, str | List[str]]', data: Data, parameter_set: ParameterSet, model: Model):
        super().__init__(bindings, parameter_set, data, model)

    def make_parameter_set(self) -> ParameterSet:
        return self.parameter_set

    def make_invariant_data(self) -> Data:
        return self._data

    def make_parameterized_data(self, parameter_set, data):
        pass

    def make_model(self, inparam_set: ParameterSet) -> Model:
        pass 
    
    def get_controlled_linear_inputs(self, k: int, linear_input_names: List[str]) -> Dict[str, float]:
        pass

    def get_controlled_nonlinear_inputs(self, k: int, nonlinear_input_names: List[str]) -> Dict[str, float]:
        pass


class SimulationError:
    
    def __init__(self, runner: Runner, k_min: int, k_max: int):
        self.runner = runner
        self.k_min = k_min
        self.k_max = k_max
        self.parameter_names = runner.parameter_set.adjustables_names
        self.output_data = dict()
        self.stdev_output_values = dict()
        for output_name in runner.output_names:
            self.output_data[output_name] = runner.data(runner._model_data_bindings[output_name][0])[k_min:k_max]
            self.stdev_output_values[output_name] = statistics.stdev(self.output_data[output_name]) 
            
    def error(self, regular_parameter_levels: List[float]):
        self.runner.parameter_set.adjustables_fingerprint = regular_parameter_levels
        #self.runner.param_nli_set.update({self.parameter_names[i]: regular_parameter_values[i] for i in range(len(self.runner.param_nli_set.adjustables))})
        output_name_values, X = self.runner.simulate(self.k_min, self.k_max, None)
        #print(self.runner.parameter_set)
        error = 0
        for output_name in self.runner.output_names:
            error += avg_error(output_name_values[output_name], self.output_data[output_name]) / self.stdev_output_values[output_name]
        return error