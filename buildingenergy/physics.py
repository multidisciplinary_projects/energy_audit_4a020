"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

Common physical properties used in buildings collected from https://help.iesve.com/ve2021/ and https://hvac-eng.com/materials-thermal-properties-database/

Properties are extracted from an Microsoft Excel file named: propertiesDB.xslx located in the data folder specified in the setup.ini file.
"""
from typing import TypeVar
import openpyxl
from typing import Dict
from enum import Enum
from numpy import interp
from scipy.constants import sigma
import configparser


Library = TypeVar('Library')

class Direction(Enum):
    """
    Flag dealing with the direction of a side. It can be vertical (walls > 60°): Direction.VERTICAL, horizontal (standard floor < 60°): Direction.HORIZONTAL and horizontal with ascending flow in case of heating floor: Direction.HORIZONTAL_ASCENDING
    """
    VERTICAL = 0  # walls > 60°
    HORIZONTAL = 1  # implicitely descending most situation with floors and ceilings
    HORIZONTAL_ASCENDING = 2  # in case of heating floors


class Library:
    """
    Library of material properties loaded from the file propertiesDB.xlsx located in the data folder whose location is given by the config file setup.ini at the project root.
    It also contains common conduction, convection and radiaton models for sides (floor, roof or wall) composed of material and air layers.
    Selected material data from the file propertiesDB.xlsx are loaded locally: there made available to use.

    :raises ValueError: Error when 2 materials with the same are loaded into the local database.
    """

    @staticmethod
    def _indoor_surface_convection_hi(direction: Direction) -> float:
        """
        Return the RT2012 indoor surface transmission coefficient in W/m2.h for the specified side direction.

        :param direction: direction of the side (see Direction)
        :type direction: Direction.'direction'
        :return: deperdition through the side in W/
        :rtype: float
        """
        hi = {Direction.VERTICAL.value: 7.69, Direction.HORIZONTAL.value: 5.88, Direction.HORIZONTAL_ASCENDING.value: 10}
        return hi[direction.value]

    @staticmethod
    def _surface_radiation_hr(emissivity, average_temperature_celcius: float):
        return 4 * sigma * emissivity * (average_temperature_celcius + 273.15) ** 3
    """
        Return the RT2012 indoor surface transmission coefficient in W/m2.K for the specified side direction.

        :param direction: direction of the side (see Direction)
        :type direction: Direction.'direction'
        :return: deperdition through the side in W/
        :rtype: float
        """

    def __init__(self):
        """
        initialize the BuildingEnergy object
        """
        config = configparser.ConfigParser()
        config.read('./setup.ini')
        self.library = dict()
        self.excel_workbook = openpyxl.load_workbook(config['folders']['data'] + 'propertiesDB.xlsx')
        print('Available properties:')
        for worksheet_name in self.excel_workbook.sheetnames:
            print('-', worksheet_name)
        self.sheet_mapping: Dict[str, function] = {'thermal': self._get_thermal, 'Uw_glazing': self._get_Uw_glazing, 'glass_transparency': self._get_glass_transparency, 'shading': self._get_shading, 'solar_absorptivity': self._get_solar_absorptivity, 'gap_resistance': self._get_gap_resistance, 'ground_reflectance': self._get_ground_reflectance}

    def store(self, short_name: str, sheet_name: str, row_number: int):
        """
        Load for usage a physical property related to a sheet name from the from the 'propertiesDB.xlsx' file, and a row number.
        :param short_name: short name used to refer to a material or a component
        :type short_name: str
        :param sheet_name: sheet name in the xlsx file where the property is
        :type sheet_name: str
        :param row_number: row in the sheet of the file containing the property loaded for local usage
        :type row_number: int
        """
        if short_name in self.library:
            raise ValueError(short_name + ' already in library')
        self.library[short_name] = self.sheet_mapping[sheet_name](row_number)

    def get(self, short_name: str) -> Dict[str, float]:
        """
        return the properties loaded locally with the 'store' method, corresponding to the specified sheet of the xlsx sheet, at the specified row

        :param short_name: short name used to refer to a material or a component
        :type short_name: str
        :return: dictionnary of values. If the short name is not present in the local database (locally loaded with 'store' method)
        :rtype: Dict[str, float]
        """
        return self.library[short_name]

    def __contains__(self, short_name: str) -> bool:
        """
        Used for checking whether a short name is in local database

        :param short_name: short name used to refer to a material or a component
        :type short_name: str
        :return: true if the short name is existing
        :rtype: bool
        """
        return short_name in self.library

    def _extract_from_worksheet(self, worksheet_name: str, description_column: str, property_column: str, row_number: str) -> float:
        """
        Get a property value from the xlsx file

        :param worksheet_name: sheet name from the xlsx file
        :type worksheet_name: str
        :param description_column: column where the description of the property is
        :type description_column: str
        :param property_column: column where the value of the property is
        :type property_column: str
        :param row_number: row where the property is
        :type row_number: str
        :return: the refered property value
        :rtype: float
        """
        worksheet = self.excel_workbook[(worksheet_name)]
        property_description = worksheet["%s%i" % (description_column, row_number)].value
        property_name = worksheet['%s1' % property_column].value
        property_value = worksheet["%s%i" % (property_column, row_number)].value
        print('> get property "%s" for "%s"' % (property_name, property_description))
        return property_value

    def _get_thermal(self, row_number: int) -> Dict[str, float]:
        """
        get a thermal property (sheet thermal)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: conductivity, Cp, density and emissivity (0.93 is used in case the value is not present) properties
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['conductivity'] = self._extract_from_worksheet('thermal', 'B', 'C', row_number=row_number)
        properties['Cp'] = self._extract_from_worksheet('thermal', 'B', 'D', row_number=row_number)
        properties['density'] = self._extract_from_worksheet('thermal', 'B', 'E', row_number=row_number)
        emissivity = self._extract_from_worksheet('thermal', 'B', 'F', row_number=row_number)
        if emissivity == '':
            emissivity = 0.93
        properties['emissivity'] = emissivity
        return properties

    def _get_Uw_glazing(self, row_number: int) -> Dict[str, float]:
        """
        get a heat transmission cooefficient for a type of window (sheet Uw_glazing)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: Uw, Uw_sheltered and Uw_severe properties
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['Uw'] = self._extract_from_worksheet('Uw_glazing', 'A', 'C', row_number=row_number)
        properties['Uw_sheltered'] = self._extract_from_worksheet('Uw_glazing', 'A', 'B', row_number=row_number)
        properties['Uw_severe'] = self._extract_from_worksheet('Uw_glazing', 'A', 'D', row_number=row_number)
        return properties

    def _get_glass_transparency(self, row_number: int) -> Dict[str, float]:
        """
        get distribution coefficients between reflection, absorption and transmission for different types of glasses (sheet glass_transparency), and the refractive_index

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: reflection, absorption, transmission and refractive_index
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['reflection'] = self._extract_from_worksheet('glass_transparency', 'A', 'B', row_number=row_number)
        properties['absorption'] = self._extract_from_worksheet('glass_transparency', 'A', 'C', row_number=row_number)
        properties['transmission'] = self._extract_from_worksheet('glass_transparency', 'A', 'D', row_number=row_number)
        properties['refractive_index'] = self._extract_from_worksheet('glass_transparency', 'A', 'E', row_number=row_number)
        return properties

    def _get_shading(self, row_number: int) -> Dict[str, float]:
        """
        get shading coefficient for different building components (sheet shading)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: shading coefficient
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['shading_coefficient'] = self._extract_from_worksheet('shading', 'A', 'B', row_number=row_number)
        return properties

    def _get_solar_absorptivity(self, row_number: int) -> Dict[str, float]:
        """
        get solar absorptivity coefficient for different surfaces (sheet solar_absorptivity)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: absorption coefficient
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['absorption'] = self._extract_from_worksheet('solar_absorptivity', 'A', 'B', row_number=row_number)
        return properties

    def _get_gap_resistance(self, row_number: int):
        """
        get air gap convection resistance for different thickness (sheet gap_resistance)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: thermal resistance Rth
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['Rth'] = self._extract_from_worksheet('gap_resistance', 'B', 'C', row_number=row_number)
        return properties

    def _get_ground_reflectance(self, row_number: int) -> Dict[str, float]:
        """
        get ground reflectance (albedo) for different surfaces (sheet ground_reflectance)

        :param row_number: row number in xlsx file
        :type row_number: int
        :return: albedo
        :rtype: Dict[str, float]
        """
        properties = {}
        properties['albedo'] = self._extract_from_worksheet('ground_reflectance', 'A', 'B', row_number=row_number)
        return properties

    def thermal_air_gap_resistance(self, material1: str, material2: str, gap_in_m: float, direction: Direction, average_temperature_celsius: float=20) -> float:
        """
        Compute the thermal resistance of a 1m2 air gap including convection and radiation phenomena

        :param material1: first material
        :type material1: str
        :param material2: second material
        :type material2: str
        :param gap_in_m: thickness of the air gap in m
        :type gap_in_m: float
        :param direction: direction of the air gap (VERTICAL, HORIZONTAL or HORIZONTAL_ASCENDING)
        :type direction: Direction
        :param average_temperature_celsius: average temperature used for linearization, defaults to 20°C for indoor (12°C for outdoor)
        :type average_temperature_celsius: float, optional
        :return: _description_
        :rtype: the equivalent thermal resistance
        """
        _thicknesses = (0, 5e-3, 7e-3, 10e-3, 15e-3, 25e-3, 30e-3)
        _thermal_air_gap_resistances = (0, 0.11, 0.13, 0.15, 0.17, 0.18, 0.18)
        emissivity = (self.get(material1)['emissivity'] + self.get(material2)['emissivity']) / 2
        if gap_in_m <=_thicknesses[-1]:
            hi = 1 / interp(gap_in_m, _thicknesses, _thermal_air_gap_resistances, left=0, right=_thermal_air_gap_resistances[-1])
        else:
            hi = 2 * Library._indoor_surface_convection_hi(direction)

        return 1 / (Library._surface_radiation_hr(emissivity, average_temperature_celsius) + hi)


    def indoor_surface_resistance(self, material: str, direction: Direction, average_temperature_celsius: float=20):
        """Indoor convective and radiative transmission coefficient for a vertical surface.

        :param material: name of the material at surface
        :type material: str
        :param direction: direction of the air gap (VERTICAL, HORIZONTAL or HORIZONTAL_ASCENDING)
        :type direction: Direction
        :param average_temperature_celsius: the temperature for which the coefficient is calculated
        :type average_temperature_celsius: float
        :return: the coefficient in W/m2.K
        :rtype: float
        """
        hi = Library._indoor_surface_convection_hi(direction)
        hr = Library._surface_radiation_hr(self.get(material)['emissivity'], average_temperature_celsius)
        return 1 / (hi + hr)


    def outdoor_surface_resistance(self, material: str, direction: Direction, average_temperature_celsius: float = 12, wind_speed_is_m_per_sec: float = 2.4):
        """Return outdoor convective and radiative transmission coefficient.

        :param material: one string among 'usual', 'glass', 'wood', 'plaster', 'concrete' or 'polystyrene'
        :type material: str
        :param direction: not used, here just for homogeneity
        :type direction: Direction
        :param average_temperature_celsius: the temperature for which the coefficient is calculated
        :type average_temperature_celsius: float
        :param wind_speed_is_m_per_sec: wind speed in m/s
        :type wind_speed_is_m_per_sec: wind spped on site
        :return: the coefficient in W/m2.K
        :rtype: float
        """

        return 1 / (11.4 + 5.7 * wind_speed_is_m_per_sec + Library._surface_radiation_hr(self.get(material)['emissivity'], average_temperature_celsius))

    def conduction_resistance(self, material: str, thickness: float):
        """Compute the conductive resistance of an layer depending of its thickness.

        :param thickness: thickness of the layer
        :type thickness: float
        :param material: one string among 'usual', 'glass', 'wood', 'plaster', 'concrete' or 'polystyrene'
        :type material: str
        :return: thermal resistance in K.m2/W
        :rtype: float
        """

        return thickness / self.get(material)['conductivity']


if __name__ == '__main__':
    properties = Library()
    properties._get_thermal(5)
    properties._get_Uw_glazing(11)
    properties._get_glass_transparency(18)
    properties._get_shading(11)
    properties._get_solar_absorptivity(22)
    properties._get_gap_resistance(23)
    properties._get_ground_reflectance(11)
