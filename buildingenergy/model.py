"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

A helper module dedicated to the design of time-varying state space model approximated by bilinear state space model.

Author: stephane.ploix@grenoble-inp.fr
"""

import math
from abc import ABC, abstractmethod
from typing import Dict, List, Tuple, TypeVar

import numpy
import scipy.signal
#from numpy import array, dot

from buildingenergy.parameters import ParameterSet

StateModel = TypeVar('StateModel')  #  dict[str, str | List[str] | numpy.array]
ZoneVariables = dict[str, str]

class Model(ABC):
    """
    This class gets the state space equation generated from the RC graph model and physics by modules buildings and physics and prepare it for use: generating and discrete time (1 hour) recurrent non-linear state model, identifying and organizing variables by type: inputs or outputs, but also by kind: temperature, heat gain, CO2 concentration, CO2 production for each zone, depending on the information provided by the designer as for example whether a zone has been declared simulated of not.
    """
    def __init__(self, order: int, parameter_set: ParameterSet, sample_time_in_seconds: int = 3600, max_buffer: int=1000):
        """
        Initialize the model and reduce its size depending on the requested order: a balanced truncature method is used. The state space model is regenerated at almost each sampled time depending on the parameter values but also of nonlinear inputs whose values are given in both cases by the parameter set. There is a cache mechanism: if a state model with nonlinear input (only these model parameters are liable to vary along a simulation) has already been computed: it is reused. If the regular parameters have been changed: the cache is cleared automatically.
        When the cache (cache) is full, no more state model can be stored but existing ones can be reused. Cache can be reset.

        :param order: order of the generated state model. None for no reduction
        :type order: int
        :param param: parameters of the model
        :type param: buildingenergy.runner.Param
        :param sample_time_in_seconds: the duration of a sampling time in seconds. it corresponds must correspond to 1h for this application (datacontainer Data correspond to 1h sampling), defaults to 3600
        :type sample_time_in_seconds: int, optional
        :param max_buffer; maximum size of the cache (buffer), defaults to 1000
        :type max_buffer: int
        """
        self.sample_time_in_seconds: int = sample_time_in_seconds
        self.order: int = order
        self.max_buffer: int = max_buffer
        self.current_invariant_parameters_fingerprint = parameter_set.invariant_fingerprint()
        self._computed_sampled_state_models = dict()  # Dict[Tuple[int|float], StateModel]

        _sampled_state_model = self.make_sampled_state_model(parameter_set)
        self.time_varying_parameter_names = _sampled_state_model['UNL']
        for p in self.time_varying_parameter_names:
            if p not in parameter_set.time_varying_names:
                raise (ValueError('The nonlinear input "%s" must be created with add_time_varying() method of the parameter set'))

        # extract from the RC model different architectural data like the room whose model variables belongs to, together with the type of variables.
        self.zones = self.site.zones()
        self._variable_zone = dict()
        self._zone_variables = dict()
        self.input_CO2production_zone = dict()
        self.input_CO2concentration_zone = dict()
        self.input_temperature_zone = dict()
        self.input_power_gain_zone = dict()
        self.output_temperature_zone = dict()
        self.output_CO2concentration_zone = dict()
        for zone_name in self.zones:
            characteristics = self.zones[zone_name]
            self._zone_variables[zone_name] = {'air_temperature': characteristics['air_temperature'], 'CO2_concentration': characteristics['CO2_production'],'CO2_production': characteristics['CO2_production'], 'power_gain': characteristics['power_gain']}
            self._variable_zone[characteristics['air_temperature']] = zone_name
            self._variable_zone[characteristics['CO2_concentration']] = zone_name
            self._variable_zone[characteristics['CO2_production']] = zone_name
            self._variable_zone[characteristics['power_gain']] = zone_name
            if characteristics['simulated']:
                self.output_temperature_zone[characteristics['air_temperature']] = zone_name
                self.output_CO2concentration_zone[characteristics['CO2_concentration']] = zone_name
                self.input_CO2production_zone[characteristics['CO2_production']] = zone_name
                self.input_power_gain_zone[characteristics['power_gain']] = zone_name
            else:
                self.input_temperature_zone[characteristics['air_temperature']] = zone_name
                self.input_CO2concentration_zone[characteristics['CO2_concentration']] = zone_name

        self.linear_input_names: List[str] = _sampled_state_model['U']
        self.output_names: List[str] = _sampled_state_model['Y']
        self.state_names: List[str] = _sampled_state_model['X']

    def variables(self, zone: str) -> Dict[str, str]:
        """
        Map zone name to model variables

        :param zone: name of the zone 
        :type zone: str
        :return: model variables
        :rtype: Dict[str, str]
        """
        return self._zone_variables[zone]

    def zone(self, variable_name: str) -> str:
        """
        Return the zone related to a model
        :param variable_name: model variable name
        :type variable_name: str
        :return: the zone name containing the variable
        :rtype: str
        """
        return self.variable_zone[variable_name]

    def include_simulated_zone(self, zone_name: str) -> bool:
        """
        Test if a zone_name is simulated

        :param zone_name: name of the zone
        :type zone_name: str
        :return: True if it has to be simulated, False otherwise
        :rtype: bool
        """
        return 'simulated' in self.zones[zone_name]

    def include_variable(self, variable_name: str) -> bool:
        """
        Test whether a variable is included in a zone

        :param variable_name: name of the variable
        :type variable_name: str
        :return: True if the variable is included, False otherwise
        :rtype: bool
        """
        return variable_name in self._variable_zone

    def make_sampled_state_model(self, parameter_set: ParameterSet) -> StateModel:
        """
        Generate a state space model considering the current nonlinear input values defined in the parameter set, using a cache (assuming other parameters of the parameter set are unchanged).

        :param param: a complete parameter set coming with the model, including nonlinear input values
        :type param: buildingenergy.runner.Param
        :return: a sampled state model (sampling time is equal to 1h)
        :rtype: StateModel
        """
        _regular_parameters_fingerprint = parameter_set.invariant_fingerprint()
        if self.current_invariant_parameters_fingerprint != _regular_parameters_fingerprint:
            self._computed_sampled_state_models.clear()
            self.current_invariant_parameters_fingerprint = _regular_parameters_fingerprint
            print('x', end='')
            
        nonlinear_input_fingerprint = parameter_set.time_varying_fingerprint()
        if nonlinear_input_fingerprint in self._computed_sampled_state_models:
            print('.', end='')
            return self._computed_sampled_state_models[nonlinear_input_fingerprint]
        print('*', end='')
        #nonlinear_input_name_values = {input: parameter_set(input) for input in parameter_set.time_varying_names}
        _differential_state_model = self.make_differential_state_model(parameter_set)

        _recurrent_state_model = _differential_state_model
        _recurrent_state_model['sample_time_in_seconds'] = self.sample_time_in_seconds
        _recurrent_state_model['A'], _recurrent_state_model['B'], _recurrent_state_model['C'], _recurrent_state_model['D'], _ = scipy.signal.cont2discrete((_differential_state_model['A'], _differential_state_model['B'], _differential_state_model['C'], _differential_state_model['D']), self.sample_time_in_seconds, method='zoh')
        
        if len(self._computed_sampled_state_models) < self.max_buffer:
            self._computed_sampled_state_models[nonlinear_input_fingerprint] = _recurrent_state_model

        return _recurrent_state_model

    @abstractmethod
    def make_differential_state_model(self, param: ParameterSet) -> StateModel:
        raise NotImplementedError

    def display_variables(self):
        print('- needed variables:')
        print('\t+ air temperatures: ', end='')
        print(', '.join(['%s (%s)' % (v, self.input_temperature_zone[v]) for v in self.input_temperature_zone]))
        print('\t+ power gain: ', end='')
        print(', '.join(['%s (%s)' % (v, self.input_power_gain_zone[v]) for v in self.input_power_gain_zone]))
        print('\t+ CO2 concentration: ', end='')
        print(', '.join(['%s (%s)' % (v, self.input_CO2concentration_zone[v]) for v in self.input_CO2concentration_zone]))
        print('\t+ CO2 production: ', end='')
        print(', '.join(['%s (%s)' % (v, self.input_CO2production_zone[v]) for v in self.input_CO2production_zone]))
        print('- simulated variables:')
        print('\t+ air temperatures: ', end='')
        print(', '.join(['%s (%s)' % (v, self.output_temperature_zone[v]) for v in self.output_temperature_zone]))
        print('\t+ CO2 concentration: ', end='')
        print(', '.join(['%s (%s)' % (v, self.output_CO2concentration_zone[v]) for v in self.output_CO2concentration_zone]))
        print('- input / output summary:')
        print('\t+ linear inputs: ', end='')
        print(', '.join(['%s' % (v) for v in self.linear_input_names]))
        print('\t+ nonlinear inputs: ', end='')
        print(', '.join(['%s' % (v) for v in self.time_varying_parameter_names]))
        print('\t+ outputs: ', end='')
        print(', '.join(['%s' % (v) for v in self.output_names]))


class Preference:
    """Provide de a model of the occupants'preferences. It deals with thermal comfort, air quality, number of home configuration changes, energy cost, icone,..."""

    def __init__(self, preferred_temperatures=(21, 23), extreme_temperatures=(18, 26), preferred_CO2_concentration=(500, 1500), temperature_weight_wrt_CO2: float = 0.5, power_weight_wrt_comfort: float = 0.5e-3):
        """Definition of comfort regarding  number of required actions by occupants, temperature and CO2 concentration, but also weights between cost and comfort, and between thermal and air quality comfort.

        :param preferred_temperatures: preferred temperature range, defaults to (21, 23)
        :type preferred_temperatures: tuple, optional
        :param extreme_temperatures: limits of acceptable temperatures, defaults to (18, 26)
        :type extreme_temperatures: tuple, optional
        :param preferred_CO2_concentration: preferred CO2 concentration range, defaults to (500, 1500)
        :type preferred_CO2_concentration: tuple, optional
        :param temperature_weight_wrt_CO2: relative importance of thermal comfort wrt air quality (1 means only temperature is considered), defaults to 0.5
        :type temperature_weight_wrt_CO2: float, optional
        :param power_weight_wrt_comfort: relative importance of energy cost wrt comfort (1 means only energy cost is considered), defaults to 0.5e-3
        :type power_weight_wrt_comfort: float, optional
        """
        self.preferred_temperatures = preferred_temperatures
        self.extreme_temperatures = extreme_temperatures
        self.preferred_CO2_concentration = preferred_CO2_concentration
        self.temperature_weight_wrt_CO2 = temperature_weight_wrt_CO2
        self.power_weight_wrt_comfort = power_weight_wrt_comfort

    def change_dissatisfaction(self, occupancy, action_set):
        """Compute the ratio of the number of hours where occupants have to change their home configuration divided by the number of hours with presence.

        :param occupancy: a vector of occupancies
        :type occupancy: list[float]
        :param action_set: different vectors of actions
        :type action_set: tuple[list[float]]
        :return: the number of hours where occupants have to change their home configuration divided by the number of hours with presence
        :rtype: float
        """
        number_of_changes = 0
        number_of_presences = 0
        previous_actions = [actions[0] for actions in action_set]
        for k in range(len(occupancy)):
            if occupancy[k] > 0:
                number_of_presences += 1
                for i in range(len(action_set)):
                    actions = action_set[i]
                    if actions[k] != previous_actions[i]:
                        number_of_changes += 1
                        previous_actions[i] = actions[k]
        return number_of_changes / number_of_presences if number_of_presences > 0 else 0

    def thermal_comfort_dissatisfaction(self, temperatures, occupancies):
        """Compute average dissatisfaction regarding thermal comfort: 0 means perfect and greater than 1 means not acceptable. Note that thermal comfort is only taken into account if occupancy > 0, i.e. in case of presence.

        :param temperatures: vector of temperatures
        :type temperatures: list[float]
        :param occupancies: vector of occupancies (number of people per time slot)
        :type occupancies: list[float]
        :return: average dissatisfaction regarding thermal comfort
        :rtype: float
        """
        if type(temperatures) is not list:
            temperatures = [temperatures]
            occupancies = [occupancies]
        dissatisfaction = 0
        for i in range(len(temperatures)):
            if occupancies[i] != 0:
                if temperatures[i] < self.preferred_temperatures[0]:
                    dissatisfaction += (self.preferred_temperatures[0] - temperatures[i]) / (self.preferred_temperatures[0] - self.extreme_temperatures[0])
                elif temperatures[i] > self.preferred_temperatures[1]:
                    dissatisfaction += (temperatures[i] - self.preferred_temperatures[1]) / (self.extreme_temperatures[1] - self.preferred_temperatures[1])
        return dissatisfaction / len(temperatures)

    def air_quality_dissatisfaction(self, CO2_concentrations, occupancies):
        """Compute average dissatisfaction regarding air quality comfort: 0 means perfect and greater than 1 means not acceptable. Note that air quality comfort is only taken into account if occupancy > 0, i.e. in case of presence.

        :param CO2_concentrations: vector of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: vector of occupancies (number of people per time slot)
        :type occupancies: list[float]
        :return: average dissatisfaction regarding air quality comfort
        :rtype: float
        """
        if type(CO2_concentrations) is not list:
            CO2_concentrations = [CO2_concentrations]
            occupancies = [occupancies]
        dissatisfaction = 0
        for i in range(len(CO2_concentrations)):
            if occupancies[i] != 0:
                dissatisfaction += max(0., (CO2_concentrations[i] - self.preferred_CO2_concentration[0]) /
                                       (self.preferred_CO2_concentration[1] - self.preferred_CO2_concentration[0]))
        return dissatisfaction / len(CO2_concentrations)

    def comfort_dissatisfaction(self, temperatures, CO2_concentrations, occupancies):
        """Compute the comfort weighted dissatisfaction that combines thermal and air quality dissatisfactions: it uses the thermal_dissatisfaction and air_quality_dissatisfaction methods.

        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :return: the global comfort dissatisfaction
        :rtype: float
        """
        return self.temperature_weight_wrt_CO2 * self.thermal_comfort_dissatisfaction(temperatures, occupancies) + (1 - self.temperature_weight_wrt_CO2) * self.air_quality_dissatisfaction(CO2_concentrations, occupancies)

    def cost(self, Pheat, kWh_price=.13):
        """Compute the heating cost.

        :param Pheat: list of heating power consumptions
        :type Pheat: list[float]
        :param kWh_price: tariff per kWh, defaults to .13
        :type kWh_price: float, optional
        :return: energy cost
        :rtype: float
        """
        if type(Pheat) is not list:
            Pheat = [Pheat]
        return sum(Pheat) / 1000 * kWh_price

    def icone(self, CO2_concentration, occupancy):
        """Compute the ICONE indicator dealing with confinement regarding air quality.

        :param CO2_concentration: list of CO2 concentrations
        :type CO2_concentration: list[float]
        :param occupancy: list of occupancies
        :type occupancy: list[float]
        :return: value between 0 and 5
        :rtype: float
        """
        n_presence = 0
        n1_medium_containment = 0
        n2_high_containment = 0
        for k in range(len(occupancy)):
            if occupancy[k] > 0:
                n_presence += 1
                if 1000 <= CO2_concentration[k] < 1700:
                    n1_medium_containment += 1
                elif CO2_concentration[k] >= 1700:
                    n2_high_containment += 1
        f1 = n1_medium_containment / n_presence if n_presence > 0 else 0
        f2 = n2_high_containment / n_presence if n_presence > 0 else 0
        return 8.3 * math.log10(1 + f1 + 3 * f2)

    def assess(self, Pheater, temperatures, CO2_concentrations, occupancies) -> float:
        """Compute the global objective to minimize including both comforts and energy cost for heating.

        :param Pheater: list of heating powers
        :type Pheater: list[float]
        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :return: objective value
        :rtype: float
        """
        return self.cost(Pheater) * self.power_weight_wrt_comfort + (1 - self.power_weight_wrt_comfort) * self.comfort_dissatisfaction(temperatures, CO2_concentrations, occupancies)

    def print_assessment(self, Pheat, temperatures, CO2_concentrations, occupancies, action_sets):
        """Print different indicators to appreciate the impact of a series of actions.

        :param Pheat: list of heating powers
        :type Pheat: list[float]
        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :param actions: list of actions
        :type actions: tuple[list[float]]
        """
        print('- global objective: %s' % self.assess(Pheat, temperatures, CO2_concentrations, occupancies))
        print('- average thermal dissatisfaction: %.2f%%' % (self.thermal_comfort_dissatisfaction(temperatures, occupancies) * 100))
        print('- average CO2 dissatisfaction: %.2f%%' % (self.air_quality_dissatisfaction(CO2_concentrations, occupancies) * 100))
        print('- ICONE: %.2f' % (self.icone(CO2_concentrations, occupancies)))
        print('- average comfort dissatisfaction: %.2f%%' % (self.comfort_dissatisfaction(temperatures, CO2_concentrations, occupancies) * 100))
        print('- change dissatisfaction (number of changes / number of time slots with presence): %.2f%%' % (self.change_dissatisfaction(occupancies, action_sets) * 100))
        print('- heating cost: %s' % self.cost(Pheat))

        temperatures_when_presence = list()
        CO2_concentrations_when_presence = list()
        for i in range(len(occupancies)):
            if occupancies[i] > 0:
                temperatures_when_presence.append(temperatures[i])
                CO2_concentrations_when_presence.append(CO2_concentrations[i])
        if len(temperatures_when_presence) > 0:
            temperatures_when_presence.sort()
            CO2_concentrations_when_presence.sort()
            office_temperatures_estimated_presence_lowest = temperatures_when_presence[:math.ceil(len(temperatures_when_presence) * 0.1)]
            office_temperatures_estimated_presence_highest = temperatures_when_presence[math.floor(len(temperatures_when_presence) * 0.9):]
            office_co2_concentrations_estimated_presence_lowest = CO2_concentrations_when_presence[:math.ceil(len(CO2_concentrations_when_presence) * 0.1)]
            office_co2_concentrations_estimated_presence_highest = CO2_concentrations_when_presence[math.floor(len(CO2_concentrations_when_presence) * 0.9):]
            print('- average temperature during presence:', sum(temperatures_when_presence) / len(temperatures_when_presence))
            print('- average 10% lowest temperature during presence:', sum(office_temperatures_estimated_presence_lowest) / len(office_temperatures_estimated_presence_lowest))
            print('- average 10% highest temperature during presence:', sum(office_temperatures_estimated_presence_highest) / len(office_temperatures_estimated_presence_highest))
            print('- average CO2 concentration during presence:', sum(CO2_concentrations_when_presence) / len(CO2_concentrations_when_presence))
            print('- average 10% lowest CO2 concentration during presence:', sum(office_co2_concentrations_estimated_presence_lowest) / len(office_co2_concentrations_estimated_presence_lowest))
            print('- average 10% highest CO2 concentration during presence:',
                  sum(office_co2_concentrations_estimated_presence_highest) / len(office_co2_concentrations_estimated_presence_highest))

    def __str__(self):
        """Return a description of the defined preferences.

        :return: a descriptive string of characters.
        :rtype: str
        """
        string = 'preference: temperature in %f<%f-%f>%f, concentrationCO2 %f>%f\n' % (
            self.extreme_temperatures[0], self.preferred_temperatures[0], self.preferred_temperatures[1], self.extreme_temperatures[1], self.preferred_CO2_concentration[0], self.preferred_CO2_concentration[1])
        string += '%.3f * cost + %.3f disT + %.3f disCO2' % (self.power_weight_wrt_comfort, (1-self.power_weight_wrt_comfort)
                                                             * self.temperature_weight_wrt_CO2, (1-self.power_weight_wrt_comfort) * (1-self.temperature_weight_wrt_CO2))
        return string
