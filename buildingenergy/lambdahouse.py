import configparser
import math
from pathlib import Path
import matplotlib.pylab as plt
import numpy
import prettytable
import buildingenergy.solar
from buildingenergy.openweather import OpenWeatherMapJsonReader
from buildingenergy.solar import SolarModel
from buildingenergy.thermics import Composition
from buildingenergy.timemg import datetime_to_stringdate

config = configparser.ConfigParser()
config.read('setup.ini')
plot_size = (int(config['sizes']['width']), int(config['sizes']['height']))

class Configuration:
    """
    Configuration of the lambda-house: this is a reference house used to appreciate the features of a building taking into account the weather file (json weather file from https://openweathermap.org metric units and Celcius degrees), an optional solar mask (https://globalsolaratlas.info), the year under interested, the sea_level_in_meter and the albedo (0.1 by default).

    Other parameter values describing the lambda house can also be specified here. The best way to configure is not inherit from this Configuration class, and to modified only the values that have to be changed.
    """

    def __init__(self, jupyter_notebook=False):
        """Initialize a default configuration of the lambda-house
        """
        self.jupyter_notebook = jupyter_notebook
        # site
        self.weather_file_name = 'refuge-des-bans.json'
        self.location = None
        self.weather_year = 2019
        self.sea_level_in_meter = 2074
        self.albedo = 0.1
        self.sky_horizon = None
        # lambda house geometry
        self.total_living_surface= 100
        self.height_per_floor = 3
        self.shape_factor = 1
        self.number_of_floors = 1
        self.wall_composition_in_out = (('polystyrene', 16e-2), ('concrete', 13e-2), ('plaster', 13e-3))
        self.roof_composition_in_out = (('brick', 2e-2), ('foam', 16e-2), ('plaster', 13e-3))
        self.glass_composition_in_out = (('glass', 5e-3), ('air', 12e-2), ('glass_foam', 5e-3))
        self.ground_composition_in_out = (('concrete', 13e-2), ('polystyrene', 16e-2), ('gravels', 50e-2))
        self.shape_factors = [.25, .5, .75, 1, 1.25, 1.5, 1.75, 2]
        # lambda house windows
        self.offset_exposure = 0
        self.offset_exposures = [alpha for alpha in range(-90, 90, 5)]
        self.beta = {'north': 0.1, 'west': 0.1, 'east': 0.1, 'south': 0.1}
        self.beta_variation = [0, .1, .2, .4, .8]
        self.solar_factor = 0.85
        self.south_solar_protection_angle = 60
        # lambda house HVAC
        self.high_heating_setpoint = 21
        self.high_heating_setpoints = [18, 19, 20, 21, 22, 23]  # 20 has to be in the list
        self.low_heating_setpoint = 17
        self.cooling_setpoint = 26
        self.cooling_setpoints = [23, 24, 25, 26, 27, 28, 29]  # 26 has to be in the list
        self.air_renewal = 0.8
        self.ventilation_heat_recovery_efficiency = 0.85
        # lambda house occupancy
        self.occupancy_schema = { # days of weeks (1=Monday,...), period (start. hour, end. hour) : avg occupancy
            (1, 2, 3, 4, 5): {(18,8): 3, (8, 18): 0},
            (6, 7): {(0, 24): 2}
            }
        self.average_occupancy_gain = 150
        self.average_permanent_gain = 50
        # lambda house production
        self.PV_efficiency = 0.15


    def __str__(self):
        """Describe the lambda-house in a text string

        :return: description of configuration
        :rtype: str
        """
        string = '## Features of the Lambda House \n'
        string += '### Site \n'
        string += '* location: %s\n' % str(self.location)
        string += '* Weather file name: %s (year: %i)\n'%(self.weather_file_name, self.weather_year)
        string += '* sea level in meter: %.f \n' % self.sea_level_in_meter
        string += '* albedo: %f (reflexion of the ground) \n' % self.albedo
        string += '* sky horizon: '
        if self.sky_horizon is not None:
            for solar_position in self.sky_horizon:
                string += '(AZ:%f, AL:%f),' % (solar_position[0], solar_position[1])
        string += '\n### Geometry \n'
        string += '* total living surface %.fm2 (%.fm2 / floor) \n' % (self.total_living_surface, self.total_living_surface / self.number_of_floors)
        string += '* number of floors %i \n' % self.number_of_floors
        string += '* height %.2fm \n' % self.height_per_floor
        string += '* shape factor %.f%% (the higher value the larger the south and north sides, the smaller the west and east sides) \n' % (self.shape_factor*100)
        string += '* shape factors for parametric study: '
        for shape_factor in self.shape_factors:
            string += '%.f%%, ' % (shape_factor*100)
        string += '\n wall composition in out \n'
        for layer in self.wall_composition_in_out:
            string += '\t+ %s with thickness: %.1fcm\n' % (layer[0], 100*layer[1])
        string += '* roof_composition_in_out \n'
        for layer in self.roof_composition_in_out:
            string += '\t+ %s with thickness: %.1fcm \n' % (layer[0], 100*layer[1])
        string += '* glass_composition_in_out\n'
        for layer in self.glass_composition_in_out:
            string += '\t+ %s with thickness: %.1fcm \n' % (layer[0], 100*layer[1])
        string += '* ground_composition_in_out\n'
        for layer in self.ground_composition_in_out:
            string += '\t+ %s with thickness: %.1fcm \n' % (layer[0], 100*layer[1])
        string += ' ### Windows\n'
        string += ' * offset_exposure %.1f° \n (offset angle of south wall with actual south (clockwise, i.e. positive value rotate the house to the west) \n' % self.offset_exposure
        string += ' * offset_exposures for parametric study: '
        for offset_exposure in self.offset_exposures:
            string += '%.f, ' % (offset_exposure)
        string += ' \n'
        string += ' * ratio of wall surface with glass \n'
        for wall_direction in self.beta:
            string += '\t + wall direction %s: %.f%% \n' % (wall_direction, self.beta[wall_direction]*100)
        string += ' * ratios of wall surface with glass for parametric study: '
        for beta in self.beta_variation:
            string+= '%i%%, ' % (beta * 100)
        string += ' \n'
        string += ' * solar factor for windows: %.f%% \n' % (self.solar_factor * 100)
        string += ' * south passive solar protection angle: %.f° (sun is hidden over this angle on the South window)\n' % self.south_solar_protection_angle
        string += ' ### HVAC \n'
        string += '  * Set-points \n'
        string += '\t+ high_heating_setpoint %.1f°Celsius (applied during heating period during presence) \n' % self.high_heating_setpoint
        string += '\t+ variation of high heating setpoints: ' + ', '.join([str(T)+'°Celcius' for T in self.high_heating_setpoints]) + ' \n'
        string += '\t+ low_heating_setpoint %.1f°Celsius (applied during heating period during absence) \n' % self.low_heating_setpoint
        string += '\t+ cooling_setpoint %.1f°Celsius (applied during cooling period during presence) \n' % self.cooling_setpoint
        string += ' * air renewal: %.2fvol/h (in case of presence and heating period only) \n' % self.air_renewal
        string += ' * ventilation heat recovery efficiency: %.f%% \n' % (self.ventilation_heat_recovery_efficiency * 100)
        string += '### Occupancy \n'
        string += ' * Occupancy schema for days of week \n'
        day_of_weeks = {1:'Monday', 2:'Tuesday', 3:'Wednesday', 4:'Thursday', 5:'Friday', 6:'Saturday', 7:'Sunday'}
        for day_series in self.occupancy_schema:
            occupancy_profile = self.occupancy_schema[day_series]
            for day_number in day_series:
                string += '\t+ '+day_of_weeks[day_number]+': '
                for period in occupancy_profile:
                    if period[0] > period[1]:
                        string += 'out of %ih to %ih: %.1f, ' % (period[1], period[0], occupancy_profile[period])
                    string += '%ih to %ih: %.1f, ' % (period[0], period[1], occupancy_profile[period])
                string += ' \n'
        string += ' * average occupancy gain: %.fW per occupant \n' % self.average_occupancy_gain
        string += ' * average permanent gain: %.fW \n' % self.average_permanent_gain
        string += ' \nProduction \n'
        string += ' * PV efficiency: %.f%% \n' % (self.PV_efficiency * 100)
        return string


def value_sort(datetimes, values):
    """sort the time series defined by the lists datetimes and values according to the values, with descending order.

    :param datetimes: times corresponding to values
    :type datetimes: list[datetime]
    :param values: values to be sorter
    :type values: list[float]
    :return: both input series sorted
    :rtype: list[datetime], list[float]
    """
    values_array = numpy.array(values)
    months_array = numpy.array([datetimes[i].month for i in range(len(datetimes))])
    indices = (-values_array).argsort()
    sorted_values_array = values_array[indices]
    sorted_months_array = months_array[indices]
    return sorted_months_array.tolist(), sorted_values_array.tolist()


def day_averager(datetimes, values, n_days):
    """replace each hours of a day by the n_days average

    :param datetimes: times
    :type datetimes: list[datetime]
    :param values: values corresponding to times
    :type values: list[float]
    :param n_days: number of days in the moving average
    :type n_days: int
    :return: the averaged values
    :rtype: ist[float]
    """
    values_array = numpy.array(values[0:-1])
    avg_values = list()
    days_of_year = numpy.array([datetimes[i].timetuple().tm_yday for i in range(len(datetimes)-1)])
    idx = numpy.where((0 < days_of_year) & (days_of_year <= n_days))[0]
    avg = numpy.average(values_array[idx])
    for i in idx:
        avg_values.append(avg)

    for day in range(n_days, days_of_year[-1]):
        idx = numpy.where((day < days_of_year) & (days_of_year <= day + n_days))[0]
        avg = numpy.average(values_array[idx])
        for _ in range(24):
            avg_values.append(avg)

    avg_values.append(avg)
    return avg_values


class Report:
    """
    Multimarkdown report maker.
    """

    def __init__(self, configuration, recall_configuration: bool=True):
        """Initialize the report maker

        :param configuration: configuration class
        :type configuration: Configuration
        """
        self.configuration = configuration
        self.figure_counter = 0
        self.add_text('# Report dealing with a lambda-house with weather file %s \n' % Path(configuration.weather_file_name))
        if recall_configuration:
            self.add_text(configuration.__str__())

    def add_text(self, text: str):
        """Add a text line in the report

        :param text: text to be added
        :type text: str
        """
        print(text + '\n')


    def add_figure(self):
        """Add the last figure to the report

        :param figure_name: name of the figure used for saving, defaults to None
        :type figure_name: str, optional
        """
        plt.show()
        

    def close(self):
        """Close the report and save it.
        """
        pass


class House:

    def __init__(self, configuration):
        """Initializer of the lambda-house

        :param configuration: configuration class
        :type configuration: Configuration
        """
        self.configuration = configuration
        self.offset_exposure = configuration.offset_exposure
        self.site_weather_data = OpenWeatherMapJsonReader(configuration.weather_file_name, '1/01/%i 0:00:00' % configuration.weather_year, '1/01/%i 0:00:00' % (configuration.weather_year + 1), sea_level_in_meter=configuration.sea_level_in_meter, albedo=configuration.albedo, location=configuration.location).site_weather_data
        self.site_weather_data = self.site_weather_data
        self.solar_model = SolarModel(site_weather_data=self.site_weather_data)
        self.Sh = configuration.total_living_surface / configuration.number_of_floors
        self.Hh = configuration.height_per_floor * configuration.number_of_floors
        self.rho = configuration.shape_factor
        self.heating_period: tuple[int] = None  # ending and starting indices related to datetimes of the heating period
        self.cooling_period: tuple[int] = None  # starting and ending indices related to datetimes of the heating period
        self.datetimes = self.site_weather_data.get('datetime')
        self.outdoor_temperatures = self.site_weather_data.get('temperature')
        self.wind_speeds = self.site_weather_data.get('wind_speed')
        self.average_outdoor_temperature = sum(self.outdoor_temperatures) / len(self.outdoor_temperatures)
        self.average_wind_speed = sum(self.wind_speeds) / len(self.wind_speeds)

        self.wall_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration.wall_composition_in_out:
            self.wall_composition.add_layer(material, thickness)

        self.glass_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration.glass_composition_in_out:
            self.glass_composition.add_layer(material, thickness)

        self.roof_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='horizontal', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration.roof_composition_in_out:
            self.roof_composition.add_layer(material, thickness)

        self.ground_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='horizontal', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration.ground_composition_in_out:
            self.ground_composition.add_layer(material, thickness)

        avg_outdoor_temperatures_array = numpy.array(day_averager(self.datetimes, self.outdoor_temperatures, n_days = 5))
        no_heating = numpy.where(avg_outdoor_temperatures_array > 18)[0]
        if no_heating.size != 0:
            self.heating_period = (no_heating[0], no_heating[-1])

        cooling = numpy.where(numpy.array(avg_outdoor_temperatures_array) > 26)[0]
        if cooling.size != 0:
            self.cooling_period = (cooling[0], cooling[-1])

        self.sky_horizon_zone = buildingenergy.solar.SkyHorizonZone(configuration.sky_horizon)

        self.update_dimensions()
        self.max_heading = 0

    def occupancy(self, datetime):
        occupancy_schema = self.configuration.occupancy_schema
        day_of_week = datetime.isoweekday()
        hour_in_day = datetime.hour
        for weekdays in occupancy_schema:
            if day_of_week in weekdays:
                for period in occupancy_schema[weekdays]:
                    if period[0] <= hour_in_day <= period[1]:
                        return occupancy_schema[weekdays][period]
                    elif  hour_in_day >= period[0] or hour_in_day <= period[1]:
                        return occupancy_schema[weekdays][period]
        return 0

    def update_dimensions(self, betas: dict={}, rho=None, offset_exposure=None):
        if rho is not None:
            self.rho = rho
        else:
            self.rho = self.configuration.shape_factor
        if offset_exposure is not None:
            self.offset_exposure = offset_exposure
        else:
            self.offset_exposure = self.configuration.offset_exposure
        for wall_direction in ('north', 'west', 'east', 'south'):
            if wall_direction not in betas:
                betas[wall_direction] = self.configuration.beta[wall_direction]
        Snorth_south = math.sqrt(self.rho * self.Sh) * self.Hh
        Swest_east = math.sqrt(self.Sh / self.rho) * self.Hh
        self.Sglass_south = betas['south'] * Snorth_south
        self.Sglass_north = betas['north'] * Snorth_south
        self.Sglass_west = betas['west'] * Swest_east
        self.Sglass_east = betas['east'] * Swest_east
        self.Swall = 2 * (math.sqrt(self.rho * self.Sh) + math.sqrt(self.Sh / self.rho)) * self.Hh + (1-betas['south']) * Snorth_south + (1-betas['west']) * Swest_east + (1-betas['north']) * Snorth_south + (1-betas['east']) * Swest_east
        self.Sground = self.Sh
        self.Sroof = self.Sh
        self.Volume = self.Sh * self.Hh
        self.Dventilation = (1 - self.configuration.ventilation_heat_recovery_efficiency) * 1.204 * 1005 * self.Volume * self.configuration.air_renewal / 3600

        self.Uwall = self.wall_composition.U * self.Swall
        self.Uglass = self.glass_composition.U * (self.Sglass_north + self.Sglass_east + self.Sglass_south + self.Sglass_west)
        self.Uroof = self.roof_composition.U * self.Sroof
        self.Uground = self.ground_composition.U * self.Sground

    def get_solar_gains(self):
        alpha = self.offset_exposure
        solar_building = buildingenergy.solar.System(self.site_weather_data, self.sky_horizon_zone)
        south_solar_mask = buildingenergy.solar.RectangularMask((-90 + alpha, 90 + alpha), (0, self.configuration.south_solar_protection_angle))
        solar_building.add_collector('south', surface=self.Sglass_south, exposure_in_deg=alpha, slope_in_deg=90, solar_factor=self.configuration.solar_factor, window_mask=south_solar_mask)
        solar_building.add_collector('west', surface=self.Sglass_west, exposure_in_deg=alpha+90, slope_in_deg=90, solar_factor=self.configuration.solar_factor)
        solar_building.add_collector('north', surface=self.Sglass_north, exposure_in_deg=alpha+180, slope_in_deg=90, solar_factor=self.configuration.solar_factor)
        solar_building.add_collector('east', surface=self.Sglass_east, exposure_in_deg=alpha-90, slope_in_deg=90, solar_factor=self.configuration.solar_factor)

        return solar_building.solar_gain

    def get_heating_needs(self, inertia, high_heating_setpoint):
        if self.heating_period is not None:
            setpoint_temperatures = list()
            heating_needs = list()
            useless_heats = list()
            total_solar_gains, _ = self.get_solar_gains()
            outdoor_temperatures = self.outdoor_temperatures
            if inertia:
                total_solar_gains = day_averager(self.datetimes, total_solar_gains, 1)
                outdoor_temperatures = day_averager(self.datetimes, outdoor_temperatures, 1)
            for i, datetime in enumerate(self.datetimes):
                if i <= self.heating_period[0] or i >= self.heating_period[1]:
                    occupancy_value = self.occupancy(datetime)
                    set_point_temperature = high_heating_setpoint if occupancy_value > 0 else self.configuration.low_heating_setpoint
                    Dventilation = self.Dventilation if occupancy_value > 0 else 0
                    raw_need = (self.Uwall + self.Uglass + self.Uroof + Dventilation) * (set_point_temperature - self.outdoor_temperatures[i]) + self.Uground * (set_point_temperature - self.average_outdoor_temperature) - occupancy_value * self.configuration.average_occupancy_gain -  self.configuration.average_permanent_gain - total_solar_gains[i]
                    heating_need = max(0,raw_need)
                    useless_heat = heating_need - raw_need
                else:
                    heating_need = 0
                    useless_heat = 0
                    set_point_temperature = None
                heating_needs.append(heating_need)
                useless_heats.append(useless_heat)
                setpoint_temperatures.append(set_point_temperature)
            return heating_needs, useless_heats, setpoint_temperatures
        else:
            return None, None, None

    def get_cooling_needs(self, inertia=False, cooling_setpoint=None):
        if self.cooling_period is not None:
            setpoint_temperatures = list()
            cooling_needs = list()
            total_solar_gains, solar_gains_per_window = self.get_solar_gains()
            outdoor_temperatures = self.outdoor_temperatures
            if inertia:
                total_solar_gains = day_averager(self.datetimes, total_solar_gains, 1)
                outdoor_temperatures = day_averager(self.datetimes, outdoor_temperatures, 1)
            for i, datetime in enumerate(self.datetimes):
                if i >= self.cooling_period[0] and i <= self.cooling_period[1]:
                    occupancy_value = self.occupancy(datetime)
                    if occupancy_value > 0:
                        set_point_temperature = cooling_setpoint if cooling_setpoint is not None else self.configuration.cooling_setpoint
                        raw_need = (self.Uwall + self.Uglass + self.Uroof) * (set_point_temperature - self.outdoor_temperatures[i]) + self.Uground * (set_point_temperature - self.average_outdoor_temperature) - occupancy_value * self.configuration.average_occupancy_gain -  self.configuration.average_permanent_gain - total_solar_gains[i]
                    else:
                        set_point_temperature = None
                        raw_need = 0
                    cooling_need = max(0,-raw_need)
                else:
                    cooling_need = 0
                    set_point_temperature = None
                cooling_needs.append(cooling_need)
                setpoint_temperatures.append(set_point_temperature)
            return cooling_needs, setpoint_temperatures
        else:
            return None, None

    def parametric_glass_variations(self):
        self.update_dimensions()
        energy_needs = {'reference': sum(self.get_heating_needs(inertia=True, high_heating_setpoint=self.configuration.high_heating_setpoint)[0])}
        for wall_direction in ('south', 'west', 'east', 'north'):
            energy_needs[wall_direction] = []
            for beta in self.configuration.beta_variation:
                self.update_dimensions({wall_direction: beta})
                energy_needs[wall_direction].append(sum(self.get_heating_needs(inertia=True, high_heating_setpoint=self.configuration.high_heating_setpoint)[0]))
        self.update_dimensions()
        return energy_needs

    def parametric_shape_factor_variations(self, inertia=True):
        self.update_dimensions()
        heat_energy_needs = []
        cool_energy_needs = []
        reference_heating_needs = None
        reference_cooling_needs = None
        for shape_factor in self.configuration.shape_factors:
            self.update_dimensions(rho=shape_factor)
            heating_needs = self.get_heating_needs(inertia=inertia, high_heating_setpoint=self.configuration.high_heating_setpoint)
            cooling_needs = self.get_cooling_needs(inertia=inertia)
            if heating_needs[0] is not None:
                heat_energy_needs.append(sum(heating_needs[0]))
            else:
                heat_energy_needs.append(None)
            if cooling_needs[0] is not None:
                cool_energy_needs.append(sum(cooling_needs[0]))
            else:
                cool_energy_needs.append(None)
            if shape_factor == 1:
                reference_heating_needs = heat_energy_needs[-1]
                reference_cooling_needs = cool_energy_needs[-1]
            if reference_heating_needs is not None:
                referenced_heating_needs = [100 * need / reference_heating_needs for need in heat_energy_needs]
            else:
                referenced_heating_needs = None
            if reference_cooling_needs is not None:
                referenced_cooling_needs = [100 * need / reference_cooling_needs for need in cool_energy_needs]
            else:
                referenced_cooling_needs = None
            self.update_dimensions()
        return referenced_heating_needs, referenced_cooling_needs

    def parametric_offset_exposure_variations(self, inertia=True):
        self.update_dimensions()
        heat_energy_needs = []
        cool_energy_needs = []
        reference_heating_needs = None
        reference_cooling_needs = None
        for offset_exposure in self.configuration.offset_exposures:
            self.update_dimensions(offset_exposure=offset_exposure)
            heating_needs = self.get_heating_needs(inertia=inertia, high_heating_setpoint=self.configuration.high_heating_setpoint)
            cooling_needs = self.get_cooling_needs(inertia=inertia)
            if heating_needs[0] is not None:
                heat_energy_needs.append(sum(heating_needs[0]))
            else:
                heat_energy_needs.append(None)
            if cooling_needs[0] is not None:
                cool_energy_needs.append(sum(cooling_needs[0]))
            else:
                cool_energy_needs.append(None)
            if offset_exposure == 0:
                reference_heating_needs = heat_energy_needs[-1]
                reference_cooling_needs = cool_energy_needs[-1]
            if reference_heating_needs is not None:
                referenced_heating_needs = [100 * need / reference_heating_needs for need in heat_energy_needs]
            else:
                referenced_heating_needs = None
            if reference_cooling_needs is not None:
                referenced_cooling_needs = [100 * need / reference_cooling_needs for need in cool_energy_needs]
            else:
                referenced_cooling_needs = None
            self.update_dimensions()
        return referenced_heating_needs, referenced_cooling_needs

    def get_best_PV_unit_year_production(self):
        neighborhood = [(-1,0), (-1,1), (-1,-1), (0,-1), (0,1), (1,-1), (1,0), (1,1)]
        taboo = list()

        best_position = position = (0, 30)
        solar_PV = buildingenergy.solar.System(self.site_weather_data, self.sky_horizon_zone)
        solar_PV.add_collector('PV', surface=1, exposure_in_deg=position[0], slope_in_deg=position[1], solar_factor=self.configuration.PV_efficiency)
        best_production = sum(solar_PV.solar_gain[0])
        taboo.append(position)

        improvement = True
        while improvement:
            improvement = False
            for neighbor in neighborhood:
                candidate = (position[0] + neighbor[0], position[1] + neighbor[1])
                if -90<=candidate[0]<=90 and 0<=candidate[1]<=90 and candidate not in taboo:
                    taboo.append(candidate)
                    solar_PV = buildingenergy.solar.System(self.site_weather_data, self.sky_horizon_zone)
                    solar_PV.add_collector('PV', surface=1, exposure_in_deg=candidate[0], slope_in_deg=candidate[1], solar_factor=self.configuration.PV_efficiency)
                    production = sum(solar_PV.solar_gain[0])
                    if production > best_production:
                        improvement = True
                        best_position = position = candidate
                        best_production = production
        return best_production, best_position[0], best_position[1]

    def results(self, sections: set={'climate', 'evolution', 'sun', 'house', 'balance', 'geometry'}, recall_configuration: bool=True) -> str:
        

        report = Report(self.configuration, recall_configuration)
        if 'climate' in sections:
            report.add_text('### Local climate Analysis')
            report.add_text('In the following figures, the recorded weather variable hourly values, curve filled with blue, left scale, have been sorted in descending order.')
            report.add_text('The light blue dots indicate in which month the value has been recorded (right scale with month number in the year (1=January,..., 12=December)).')
            indices = [100*i/(len(self.outdoor_temperatures)-1) for i in range(len(self.outdoor_temperatures))]
            average_outdoor_temperature = sum(self.outdoor_temperatures) / len(self.outdoor_temperatures)
            sorted_months, sorted_outdoor_temperatures = value_sort(self.datetimes, self.outdoor_temperatures)
            _, ax1 = plt.subplots(figsize=plot_size)
            ax1.fill_between(indices, sorted_outdoor_temperatures, alpha=1)
            ax1.plot([0,100],[18, 18],'r')
            ax1.plot([0,100],[26, 26],'r')
            ax1.plot([0,100],[average_outdoor_temperature, average_outdoor_temperature],'k:')
            ax1.set_xlim([0, 100])
            ax1.grid(True)
            ax1.set_xlabel('% of the year')
            ax1.set_ylabel('temperature in Celsius')
            for label in ax1.get_xticklabels():
                label.set_visible(True)
            ax2 = ax1.twinx()
            ax2.plot(indices, sorted_months,'.c')
            ax2.set_ylabel('month number')
            plt.tight_layout()
            report.add_figure()

            avg_outdoor_temperatures_array = numpy.array(day_averager(self.datetimes, self.outdoor_temperatures, n_days = 5))
            _, axis = plt.subplots(figsize=plot_size)
            axis.plot(self.datetimes, self.outdoor_temperatures, alpha=1)
            axis.plot(self.datetimes, avg_outdoor_temperatures_array, alpha=1)
            axis.plot([self.datetimes[0],self.datetimes[-1]], [18, 18], 'r')
            axis.plot([self.datetimes[0],self.datetimes[-1]], [26, 26], 'r')
            axis.set_title('Outdoor temperature and 5-day average with heating/cooling periods')
            minT, maxT = min(self.outdoor_temperatures), max(self.outdoor_temperatures)
            report.add_figure()

            wind_speed = self.site_weather_data.get('wind_speed')
            average_wind_speed = sum(wind_speed) / len(wind_speed)
            sorted_months, sorted_wind_speed = value_sort(self.datetimes, wind_speed)
            _, axs = plt.subplots(2, 2, figsize=plot_size)
            ax1, ax2, ax3, ax4 = axs[0, 0], axs[0, 1], axs[1, 0], axs[1, 1]
            ax1.fill_between(indices, sorted_wind_speed, alpha=1)
            ax1.plot([0,100],[average_wind_speed, average_wind_speed],'k:')
            ax1.set_xlim([0, 100])
            ax1.grid(True)
            ax1.set_xlabel('% of the year')
            ax1.set_ylabel('wind speed in m/s')
            for label in ax1.get_xticklabels():
                label.set_visible(True)
            ax1bis = ax1.twinx()
            ax1bis.plot(indices, sorted_months,'.c')
            ax1bis.set_ylabel('month number')
            plt.tight_layout()

            cloudiness = self.site_weather_data.get('cloudiness')
            average_cloudiness = sum(cloudiness) / len(cloudiness)
            sorted_months, sorted_cloudiness = value_sort(self.datetimes, cloudiness)
            ax2.fill_between(indices, sorted_cloudiness, alpha=1)
            ax2.plot([0,len(self.datetimes)],[average_cloudiness, average_cloudiness],'k:')
            ax2.set_xlim([0, 100])
            ax2.grid(True)
            ax2.set_xlabel('% of the year')
            ax2.set_ylabel('cloudiness in %')
            for label in ax2.get_xticklabels():
                label.set_visible(True)
            ax2bis = ax2.twinx()
            ax2bis.plot(indices, sorted_months,'.c')
            ax2bis.set_ylabel('month number')
            plt.tight_layout()

            humidity = self.site_weather_data.get('humidity')
            average_humidity = sum(humidity) / len(humidity)
            sorted_months, sorted_humidity = value_sort(self.datetimes, humidity)
            ax3.fill_between(indices, sorted_humidity, alpha=1)
            ax3.plot([0,len(self.datetimes)],[average_humidity, average_humidity],'k:')
            ax3.set_xlim([0, 100])
            ax3.grid(True)
            ax3.set_xlabel('% of the year')
            ax3.set_ylabel('humidity in %')
            for label in ax3.get_xticklabels():
                label.set_visible(True)
            ax3bis = ax3.twinx()
            ax3bis.plot(indices, sorted_months,'.c')
            ax3bis.set_ylabel('month number')
            plt.tight_layout()

            irradiation = self.solar_model.solar_irradiations(0, 0)['total']
            average_irradiation = sum(irradiation) / len(irradiation)
            sorted_months, sorted_irradiation = value_sort(self.datetimes, irradiation)
            ax4.fill_between(indices, sorted_irradiation, alpha=1)
            ax4.plot([0,len(self.datetimes)],[average_irradiation, average_irradiation],'k:')
            ax4.set_xlim([0, 100])
            ax4.grid(True)
            ax4.set_xlabel('% of the year')
            ax4.set_ylabel('horizontal irradiation in W/m2')
            for label in ax4.get_xticklabels():
                label.set_visible(True)
            ax4bis = ax4.twinx()
            ax4bis.plot(indices, sorted_months,'.c')
            ax4bis.set_ylabel('month number')
            plt.tight_layout()
            report.add_figure()
            
            ## End of Climate analysis
            
        if 'evolution' in sections:
            import plotly.graph_objects as go
            
            all_site_weather_data = OpenWeatherMapJsonReader(self.configuration.weather_file_name, None, None, sea_level_in_meter=self.configuration.sea_level_in_meter, albedo=self.configuration.albedo, location=self.configuration.location).site_weather_data
            
            datetimes = all_site_weather_data.get('datetime')
            temperatures = all_site_weather_data.get('temperature')
            cloudinesses = all_site_weather_data.get('cloudiness')
            
            class YearMonthData:
                
                def __init__(self) -> None:
                    self.month_data = dict()
                    self.months = list()
                
                def append(self, datetime, value):
                    month_name = datetime.strftime('%b')
                    if month_name not in self.months:
                        self.month_data[month_name] = list()
                        self.months.append(month_name)
                    self.month_data[month_name].append(value)
                
                def data(self):
                    return self.months, [sum(self.month_data[month]) / len(self.month_data[month]) for month in self.months]
                        
            year_monthlytemperatures = dict()
            year_monthlycloudinesses = dict()
            for i, datetime in enumerate(datetimes):
                if datetime.year not in year_monthlytemperatures:
                    year_monthlytemperatures[datetime.year] = YearMonthData()
                    year_monthlycloudinesses[datetime.year] = YearMonthData()
                year_monthlytemperatures[datetime.year].append(datetime, temperatures[i])
                year_monthlycloudinesses[datetime.year].append(datetime, cloudinesses[i])

            # Get the colors 
            colors = [f'rgb(%i,%i,%i)' %(255-i*255/(len(year_monthlytemperatures) - 1), abs(128-i*255/(len(year_monthlytemperatures)- 1)) , i*255/(len(year_monthlytemperatures)- 1)) for i in range(len(year_monthlytemperatures))]

            fig = go.Figure() # create a figure
            # Plot each year with a corresponding color
            for i, year in enumerate(year_monthlytemperatures):
                months, values = year_monthlytemperatures[year].data()
                fig.add_trace(go.Scatterpolar(r=values, theta=months, name=str(year), line_color=colors[i]))
            # Adjust the size of the figure
            fig.update_layout(autosize=False, width=1000, height=800,)
            fig.show()
            
            fig = go.Figure()
            for i, year in enumerate(year_monthlycloudinesses):
                months, values = year_monthlycloudinesses[year].data()
                fig.add_trace(go.Scatterpolar(r=values, theta=months, name=str(year), line_color=colors[i]))
            # Adjust the size of the figure
            fig.update_layout(autosize=False, width=1000, height=800,)
            fig.show()
            

        if 'sun' in sections:
            report.add_text('## Solar irradiation Analysis')

            axis = self.solar_model.plot_heliodor(self.configuration.weather_year)
            self.sky_horizon_zone.plot('Sky Horizon', axis=axis)
            report.add_figure()
            
            global_solar_gains, solar_gains_per_window = self.get_solar_gains()
            total_solar_gains = sum(global_solar_gains)
            windows_names = ['south', 'west', 'east', 'north']
            windows_gains_percentage = [100 * sum(solar_gains_per_window[window]) / total_solar_gains for window in windows_names]
            _, axis = plt.subplots(tight_layout=True, figsize=plot_size)
            axis.pie(windows_gains_percentage,  labels=windows_names, autopct='%1.1f%%', startangle=90)
            axis.legend(('south', 'west', 'east', 'north'))
            axis.set_title('Total solar gain for the 4 sides: %.fkWh' % (total_solar_gains/1000))
            axis.axis('equal')
            report.add_figure()
            
            best_PVproduction, best_PVexposure, best_PVslope = self.get_best_PV_unit_year_production()
            report.add_text('Best PV exposure: %i° and slope %i°' % (best_PVexposure, best_PVslope))
            report.add_text('Best production with a %.fm2 PV panel (efficiency %.f%%): %.f kWh' % (self.configuration.total_living_surface, 100*self.configuration.PV_efficiency, best_PVproduction*self.configuration.total_living_surface/1000))
                
            
            ## End of Solar analysis
            
        if 'house' in sections:
            report.add_text('## House Analysis')
            
            avg_outdoor_temperatures_array = numpy.array(day_averager(self.datetimes, self.outdoor_temperatures, n_days = 5))
            minT, maxT = min(self.outdoor_temperatures), max(self.outdoor_temperatures)
            _, axis = plt.subplots(figsize=plot_size)
            axis.plot(self.datetimes, self.outdoor_temperatures, alpha=1)
            axis.set_ylabel('outdoor temperature and heating/cooling periods')
            no_heating = numpy.where(avg_outdoor_temperatures_array > 18)[0]
            if no_heating.size != 0:
                i_end_heating = no_heating[0]
                i_start_heating = no_heating[-1]
                report.add_text('Detected heating period: ' + datetime_to_stringdate(self.datetimes[i_start_heating], date_format='%d %B') + ' till ' + datetime_to_stringdate(self.datetimes[i_end_heating], date_format='%d %B') + '\n')
                axis.plot([self.datetimes[i_start_heating],self.datetimes[i_start_heating]],[minT, maxT], 'r:')
                axis.plot([self.datetimes[i_end_heating],self.datetimes[i_end_heating]],[minT, maxT], 'r:')

            cooling = numpy.where(avg_outdoor_temperatures_array > 26)[0]
            if cooling.size != 0:
                i_start_cooling = cooling[0]
                i_end_cooling = cooling[-1]
                report.add_text('Detected cooling period: ' + datetime_to_stringdate(self.datetimes[i_start_cooling], date_format='%d %B') + ' till ' + datetime_to_stringdate(self.datetimes[i_end_cooling] , date_format='%d %B') + '\n')
                axis.plot([self.datetimes[i_start_cooling],self.datetimes[i_start_cooling]],[minT, maxT], 'r:')
                axis.plot([self.datetimes[i_end_cooling],self.datetimes[i_end_cooling]],[minT, maxT], 'r:')
            report.add_figure()

            heating_needs_no_inertia, useless_heats_no_inertia, setpoint_temperatures_no_inertia = self.get_heating_needs(inertia=False, high_heating_setpoint=self.configuration.high_heating_setpoint)
            if heating_needs_no_inertia is not None and len(heating_needs_no_inertia) > 0:
                report.add_text('Energy needs from heater without inertia (useless heat is free power gain not useful for heating because the temperature is higher than the requested set-point)')
                fig, axs = plt.subplots(2, 1, tight_layout=True, figsize=plot_size)
                axs[0].plot(self.datetimes, heating_needs_no_inertia)
                axs[0].legend(('Heating need',))
                axs[1].plot(self.datetimes, useless_heats_no_inertia)
                axs[1].legend(('Useless heat',))
                ax2 = axs[1].twinx()
                ax2.plot(self.datetimes, setpoint_temperatures_no_inertia,'.r')
                ax2.legend(('setpoint temperature',))
                report.add_figure()

            heating_needs_inertia, useless_heats_inertia, setpoint_temperatures_inertia = self.get_heating_needs(inertia=True, high_heating_setpoint=self.configuration.high_heating_setpoint)
            if heating_needs_inertia is not None and len(heating_needs_inertia) > 0:
                report.add_text('Energy needs from heater with inertia (useless heat is free power gain not useful for heating because the temperature is higher than the requested set-point)')
                fig, axs = plt.subplots(2, 1, tight_layout=True, figsize=plot_size)
                axs[0].plot(self.datetimes, heating_needs_inertia)
                axs[0].legend(('Heating need',))
                axs[1].plot(self.datetimes, useless_heats_inertia)
                axs[1].legend(('Useless heat',))
                ax2 = axs[1].twinx()
                ax2.plot(self.datetimes, setpoint_temperatures_inertia,'.r')
                ax2.legend(('setpoint temperature',))
                report.add_figure()

            cooling_needs_no_inertia, setpoint_temperatures_no_inertia = self.get_cooling_needs()
            if cooling_needs_no_inertia is None:
                report.add_text('No cooling needs without inertia.')
            else:
                report.add_text('Energy needs from climatisation without inertia')
                _, axs = plt.subplots(tight_layout=True, figsize=plot_size)
                axs.plot(self.datetimes, cooling_needs_no_inertia)
                axs.legend(('Cooling need',))
                axs.set_ylabel('Cooling power in W')
                ax2 = axs.twinx()
                ax2.plot(self.datetimes, setpoint_temperatures_no_inertia,'.r')
                ax2.legend(('setpoint temperature',))
                report.add_figure()

            cooling_needs_inertia, setpoint_temperatures_inertia = self.get_cooling_needs(inertia=True)
            if cooling_needs_inertia is None:
                report.add_text('No cooling needs with inertia.')
            else:
                report.add_text('Energy needs from climatisation with inertia')
                fig, axs = plt.subplots(tight_layout=True, figsize=plot_size)
                axs.plot(self.datetimes, cooling_needs_inertia)
                axs.legend(('Cooling need',))
                axs.set_ylabel('Cooling power in W')
                ax2 = axs.twinx() 
                ax2.plot(self.datetimes, setpoint_temperatures_inertia,'.r')
                ax2.legend(('setpoint temperature',))
                report.add_figure()
                
            heater_needed_energy = [sum(heating_needs_no_inertia)/1000/self.configuration.total_living_surface if heating_needs_no_inertia is not None else 0, sum(heating_needs_inertia)/1000/self.configuration.total_living_surface if heating_needs_inertia is not None else 0] 
            waste_energy = [sum(useless_heats_no_inertia)/1000/self.configuration.total_living_surface if useless_heats_no_inertia is not None else 0, sum(useless_heats_inertia)/1000/self.configuration.total_living_surface if useless_heats_inertia is not None else 0]
            cooling_needed_energy = [sum(cooling_needs_no_inertia)/1000/self.configuration.total_living_surface if cooling_needs_no_inertia is not None else 0, sum(cooling_needs_inertia)/1000/self.configuration.total_living_surface if cooling_needs_inertia is not None else 0] 
            labels = ['no inertia', 'inertia']
            fig, ax = plt.subplots(tight_layout=True, figsize=plot_size)
            x = numpy.arange(len(labels))
            width = 0.35
            rects1 = ax.bar(x-width, waste_energy, width, label='waste')
            rects2 = ax.bar(x, heater_needed_energy, width, label='heating')
            rects3 = ax.bar(x+width, cooling_needed_energy, width, label='cooling')
            ax.set_ylabel('kWh/m2/year')
            ax.set_title('Energy for heating and cooling + waste')
            ax.set_xticks(x, labels)
            ax.legend()
            ax.bar_label(rects1, padding=3)
            ax.bar_label(rects2, padding=3)
            ax.bar_label(rects3, padding=3)
            report.add_figure()
        
            glass_percentage = [100 * beta for beta in self.configuration.beta_variation]
            wall_directions = []
            energy_needs = self.parametric_glass_variations()
            fig, ax = plt.subplots(tight_layout=True, figsize=plot_size)
            for wall_direction in energy_needs:
                if wall_direction != 'reference':
                    wall_directions.append(wall_direction)
                    needs_percentage = []
                    for need in energy_needs[wall_direction]:
                        needs_percentage.append(need/energy_needs['reference']*100)
                    ax.plot(glass_percentage, needs_percentage)
            ax.legend(wall_directions)
            ax.set_xlabel('percentage of glass per wall')
            ax.set_ylabel('variation of heating energy needed with reference to 10%')
            ax.set_title('Influence of the glass surface in case of inertia')
            ax.grid()
            report.add_figure()

        if 'balance' in sections:
            report.add_text('## Zero energy over the year')
            report.add_text('The aim is to appreciate the yearly energy needed by the HVAC system. To do it, the carbon neutrality is searched thanks to a surface of PV panels.')
            best_PVproduction, best_PVexposure, best_PVslope = self.get_best_PV_unit_year_production()
            report.add_text('Best PV exposure: %i° and slope %i°' % (best_PVexposure, best_PVslope))

            heating_needs_no_inertia, useless_heats_no_inertia, setpoint_temperatures_no_inertia = self.get_heating_needs(inertia=False, high_heating_setpoint=self.configuration.high_heating_setpoint)
            heating_needs_inertia, useless_heats_inertia, setpoint_temperatures_inertia = self.get_heating_needs(inertia=True, high_heating_setpoint=self.configuration.high_heating_setpoint)
            cooling_needs_no_inertia, setpoint_temperatures_no_inertia = self.get_cooling_needs()
            cooling_needs_inertia, setpoint_temperatures_inertia = self.get_cooling_needs(inertia=True)

            total_heating_needs_no_inertia = sum(heating_needs_no_inertia) if heating_needs_no_inertia is not None else 0
            total_heating_needs_inertia = sum(heating_needs_inertia) if heating_needs_inertia is not None else 0
            total_cooling_needs_no_inertia = sum(cooling_needs_no_inertia) if cooling_needs_no_inertia is not None else 0
            total_cooling_needs_inertia = sum(cooling_needs_inertia) if cooling_needs_inertia is not None else 0
            

            report.add_text('- Required surface of PV for balancing the annual energy consumption of the HVAC system:')
            table = prettytable.PrettyTable()
            table.field_names = ('PV (efficiency: %.f%%)' % (100*self.configuration.PV_efficiency), 'no inertia', 'inertia')
            table.add_row(('heater', '%.fm2' % (total_heating_needs_no_inertia / best_PVproduction), ' %.fm2' % (total_heating_needs_inertia / best_PVproduction)))
            table.add_row(('air conditioning', '%.fm2' % (total_cooling_needs_no_inertia / best_PVproduction), ' %.fm2' % (total_cooling_needs_inertia / best_PVproduction)))
            table.add_row(('both', '%.fm2' % ((total_heating_needs_no_inertia + total_cooling_needs_no_inertia)/best_PVproduction), ' %.fm2' % ((total_heating_needs_inertia + total_cooling_needs_inertia)/best_PVproduction)))
            print(table)
            
        if 'geometry' in sections:
            report.add_text(' \n## Geometry')
            report.add_text('- shape factor (keep the building surface constant, 1 yields a square, higher than 1 yields a rectangle with South/North sides bigger than East/West sides and lower than one, the opposite')
            table = prettytable.PrettyTable()
            table.field_names = ('shape factor', 'south/north side length', 'west/east side length')
            for shape_factor in self.configuration.shape_factors:
                table.add_row(('%.2f' % shape_factor, '%.2f' % math.sqrt(shape_factor * self.Sh), '%.2f' % math.sqrt(self.Sh / shape_factor))) 
            print(table)
            
            report.add_text(' \n')
            heat_energy_needs, cool_energy_needs = self.parametric_shape_factor_variations()
            fig, axs = plt.subplots(tight_layout=True, figsize=plot_size)
            the_legend=[]
            if heat_energy_needs is not None:
                axs.plot(self.configuration.shape_factors, heat_energy_needs)
                the_legend.append('heater need')
            if cool_energy_needs is not None:
                axs.plot(self.configuration.shape_factors, cool_energy_needs)
                the_legend.append('air conditioning need')
            axs.legend(tuple(the_legend))
            axs.set_ylabel('percentage')
            axs.set_xlabel('shape factors')
            report.add_figure()
            

            report.add_text('- best angle of the so-called South wall with the South (0 stands for South wall is facing the South, 90° the West and -90° the East.)')
            heat_energy_needs, cool_energy_needs = self.parametric_offset_exposure_variations()
            fig, axs = plt.subplots(tight_layout=True, figsize=plot_size)
            the_legend=[]
            if heat_energy_needs is not None:
                axs.plot(self.configuration.offset_exposures, heat_energy_needs)
                the_legend.append('heater need')
            if cool_energy_needs is not None:
                axs.plot(self.configuration.offset_exposures, cool_energy_needs)
                the_legend.append('climatizer need')
            axs.legend(tuple(the_legend))
            axs.set_ylabel('percentage')
            axs.set_xlabel('offset exposures')
            report.add_figure()

            report.add_text('Influence of temperature set-points during presence (with inertia)')
            the_legend=[]
            _, axis = plt.subplots(tight_layout=True, figsize=plot_size)
            if heating_needs_inertia is not None:
                the_legend.append('heating')
                heating_needs_inertias = list()
                for high_heating_setpoint in self.configuration.high_heating_setpoints:
                    heating_needs_inertia, useless_heats_inertia, setpoint_temperatures_inertia = self.get_heating_needs(inertia=True, high_heating_setpoint=high_heating_setpoint)
                    heating_needs_inertias.append(sum(heating_needs_inertia))
                index20 = self.configuration.high_heating_setpoints.index(20)
                
                axis.plot(self.configuration.high_heating_setpoints, [100 * heating_needs_inertia / heating_needs_inertias[index20] for heating_needs_inertia in heating_needs_inertias])
                
            if cooling_needs_inertia is not None:
                the_legend.append('cooling')
                cooling_needs_inertias = list()
                for cooling_setpoint in self.configuration.cooling_setpoints:
                    cooling_needs_inertia, setpoint_temperatures_inertia = self.get_cooling_needs(inertia=True, cooling_setpoint=cooling_setpoint)
                    cooling_needs_inertias.append(sum(cooling_needs_inertia))
                index26 = self.configuration.cooling_setpoints.index(26)
                axis.plot(self.configuration.cooling_setpoints, [100 * cooling_needs_inertia / cooling_needs_inertias[index26] for cooling_needs_inertia in cooling_needs_inertias])
                axis.grid()
                report.add_figure()
                
            axis.legend(('heating needs',))
            axis.set_xlabel('temperature setpoint')
            axis.set_ylabel('energy in percentage of needs for 20°C (heat) & 26°C (cooling)')
            axis.grid()
            report.add_figure()
        ##return report
