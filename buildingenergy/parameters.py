"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module deals with parameters that can intervene in data (ParameterizedData) or in the models.
"""

import os
import pickle
from abc import ABC, abstractmethod
from operator import matmul
from typing import Dict, List, Tuple
import configparser

import numpy
import numpy.linalg

config = configparser.ConfigParser()
config.read('setup.ini')

class ParameterSet:
    """
    A parameter is referring to a value that may change from one hour simulation step to another, but more commonly, from a simulation to another. A specific type of parameters is the nonlinear input variable, which is connected to the model (in practice, some air flows) and is a cause of bilinear nonlinearity. They are not considered as adjustable because they are not concerned by parameter adjustment process.
    """
    
    def __init__(self, resolution: int=10):
        """
        Initialize a parameter set with given resolution, used to store in a cache already computed state models in order to reduce computations but also to estimate the parameters' values. If resolution = n, then the parameter intervals will be decomposed in n and all the state models belonging to the same hypercube are considered as equal. 

        :param resolution: Resolution corresponds to the number of value levels a nonlinear input is approximated to be replaced by a cached close state model. The resolution has to be at least equal to 2. The higher, the more precise but also the slower, and conversely;
        :type resolution: int
        """
        self._values: Dict[str, float] = dict()
        self._bounds: Dict[str, Tuple[float, float]] = dict()
        self._nominal: Dict[str, float] = dict()
        self.invariant_names: List[str] = list()
        self.time_varying_names: List[str] = list()
        self.resolution = resolution

    def __call__(self, name: str) -> float:
        """
        Use to make easy to use functor, which returns the current value of a parameter

        :param name: name of the parameter
        :type name: str
        :return: the current value
        :rtype: float
        """
        return self._values[name]
    
    def __add(self, name: str, value: float, min_value: float = None, max_value: float = None) -> float:
        """
        Add a parameter to the set of parameters. The initial value is stored as the nominal value for future uses. This private method should be used directly.

        :param name: name of the parameter
        :type name: str
        :param value: current value of the parameter
        :type value: float
        :param min_value: use for parameter whose value might change from a simulation to another, but also from one hour simulation step to another. It specifies the minimum possible value, if None, the parameter is considered as invariant, defaults to None
        :type min_value: float, optional
        :param max_value: specify the maximum possible value, if None, the parameter is considered as invariant, defaults to None
        :type max_value: float, optional
        :return: the parameter value is returned
        :rtype: float
        """
        if name not in self._nominal:
            self._nominal[name] = value
        self._values[name] = value
        if min_value is not None and max_value is not None:
            self._bounds[name] = (min_value, max_value)

    def add_invariant(self, name: str, value: float, min_value: float = None, max_value: float = None) -> float:
        """
        Add a parameter to the set of invariant parameters that keep the same value during a simulation. They are decomposed into adjustable parameters when an interval bounds is specified, and non-adjustable ones. Adjustable parameters' values can be modified by a parameter adjustment process providing their values belong to the specified interval bounds. The initial value is stored as the nominal value for future uses.

        :param name: name of the parameter
        :type name: str
        :param value: current value of the parameter
        :type value: float
        :param min_value: use for parameter whose value might change from a simulation to another, but also from one hour simulation step to another. It specifies the minimum possible value, if None, the parameter is considered as invariant, defaults to None
        :type min_value: float, optional
        :param max_value: specify the maximum possible value, if None, the parameter is considered as invariant, defaults to None
        :type max_value: float, optional
        :return: the parameter value is returned
        :rtype: float
        """
        self.__add(name, value, min_value=min_value, max_value=max_value)
        if name not in self.invariant_names:
            self.invariant_names.append(name)
        return value
        
    def add_time_varying(self, name: str, value: float, min_value: float, max_value: float):
        """
        Same behavior than add() method but for time-varying parameters based on nonlinear input data.

        :param name: name of the parameter
        :type name: str
        :param value: see add() method
        :type value: float
        :param min_value: see add() method
        :type min_value: float
        :param max_value: see add() method
        :type max_value: float
        :return: the parameter value is returned
        :rtype: float
        """
        self.__add(name, value, min_value=min_value, max_value=max_value)
        if name not in self.time_varying_names:
            self.time_varying_names.append(name)
        return value

    def bounds(self, name: str) -> Tuple[float, float]:
        """
        return the interval of possible parameter values

        :param name: name of the requested bound parameter 
        :type name: str
        :return: interval bounding the possible parameter values
        :rtype: Tuple[float, float]
        """
        return self._bounds[name]

    def nominal(self, name: str) -> float:
        """
        return the initial parameter value

        :param name: name of the requested initial parameter value
        :type name: str
        :return: the initial parameter value
        :rtype: float
        """
        return self._nominal[name]
    
    def time_varying_names(self) -> List[str]:
        """
        return the list of parameters added as time-varying parameters

        :return: the list of parameters added as time-varying parameters
        :rtype: List[str]
        """
        return self.time_varying_names
    
    def time_varying_names_values(self) -> Dict[str, float]:
        return {p: self(p) for p in self.time_varying_names()}
    
    def time_varying_fingerprint(self) -> Tuple[int]:
        """
        It returns a list of integers representing an hypercube of the space of time-varying parameters. The size of the hypercube corresponds to the normalized maximum amplitude of the time-varying parameters' interval of values multiplied by the resolution. If a set of time-varying parameters lead to the same fingerprint than a previously used state model (with same regular parameters' fingerprint), it will be reused.

        :return: fingerprint for time-varying parameters
        :rtype: Tuple[int]
        """
        fingerprint = []
        for nonlinear_input_name in self.time_varying_names:
            nonlinear_input_value = self._values[nonlinear_input_name]
            nonlinear_input_bounds = self._bounds[nonlinear_input_name]
            nonlinear_input_level = round((nonlinear_input_value - nonlinear_input_bounds[0]) / (nonlinear_input_bounds[1] - nonlinear_input_bounds[0]) * self.resolution)
            fingerprint.append(nonlinear_input_level)
        return tuple(fingerprint)
    
    def invariant_fingerprint(self) -> 'Tuple[int|float]':
        """
        It returns a list of integers (is bounds have been specified) or floats (just a parameter value is available) representing a point (no bounds have been specified) or an hypercube of the space of regular parameters. The size of the hypercube corresponds to the normalized maximum amplitude of the parameter interval of values multiplied by the resolution. If a set of regular parameters lead to the same fingerprint than a previously used state model (with same regular parameters' fingerprint), it will be reused.

        :return: fingerprint for regular parameters
        :rtype: Tuple[int|float]
        """
        fingerprint = []
        for regular_parameter_name in self.invariant_names:
            regular_input_value = self._values[regular_parameter_name]
            if regular_parameter_name in self._bounds and self._bounds[regular_parameter_name][1] > self._bounds[regular_parameter_name][0]:
                regular_input_bounds = self._bounds[regular_parameter_name]
                regular_input_level = round((regular_input_value - regular_input_bounds[0]) / (regular_input_bounds[1] - regular_input_bounds[0]) * self.resolution)
            else:
                regular_input_level = regular_input_value
            fingerprint.append(regular_input_level)
        return tuple(fingerprint)
    
    @property
    def number_of_ajustables(self) -> int:
        """
        Return the number of adjustable parameters

        :return: number of adjustable parameters
        :rtype: int
        """
        return len(self.adjustables_names)

    @property
    def adjustables_names(self) -> List[str]:
        """
        Return the list of invariant parameters that have defined bounds

        :return: the list of invariant parameters that have defined bounds
        :rtype: List[str]
        """
        _adjustables: List[str] = list()
        for parameter_name in self.invariant_names:
            if parameter_name in self._bounds and self._bounds[parameter_name][0] < self._bounds[parameter_name][1]:
                _adjustables.append(parameter_name)
        return _adjustables
    
    @property
    def adjustables_values(self) -> List[float]:
        """
        Return the current values of adjustable parameters according to order given by adjustable_names

        :return: the current values of adjustable parameters
        :rtype: List[float]
        """
        return [self._values[parameter_name] for parameter_name in self.adjustables_names]
    
    @property
    def adjustables_fingerprint(self) -> List[int]:
        """
        Return a list of integers belonging to {0, ..., resolution-1} representing a value level for each parameter: 0 for min_value and resolution-1 for max_value

        :return: a value level for each parameter
        :rtype: List[int]
        """
        levels = []
        for parameter_name in self.adjustables_names:
            regular_input_value = self._values[parameter_name]
            regular_input_bounds = self._bounds[parameter_name]
            regular_input_level = round((regular_input_value - regular_input_bounds[0]) / (regular_input_bounds[1] - regular_input_bounds[0]) * self.resolution)
            levels.append(regular_input_level)
        return levels
    
    @adjustables_fingerprint.setter
    def adjustables_fingerprint(self, levels: List[int]) -> List[float]:
        """
        Set all the adjustable parameter value levels at once

        :param levels: discrete level between 0 and resolution - 1 for each adjustable parameter
        :type levels: List[int]
        :return: the float values of adjustable parameters
        :rtype: List[float]
        """
        parameter_name_values = dict()
        for i, parameter_name in enumerate(self.adjustables_names):
            parameter_name_values[parameter_name] = self.bounds(parameter_name)[0] + (self.bounds(parameter_name)[1] - self.bounds(parameter_name)[0]) / self.resolution * levels[i]
        self.update(parameter_name_values)
        return parameter_name_values

    def save(self, pickle_file_name):
        """Save current parameter values into a pickle file.

        :param pickle_file_name: name of the pickle file
        :type pickle_file_name: str
        """
        with open(config['folders']['results'] + pickle_file_name, "wb") as file:
            pickle.dump([self._values[p] for p in self.invariant_names], file)

    def load(self, parameters_filename: str="best_parameters.p"):
        """Load parameter values from a pickle file: these values will replace the nominal ones.

        :param parameters_filename: name of the file, defaults to 'best_parameters.p'
        :type parameters_filename: str, optional
        """
        if os.path.exists( config['folders']['data'] + parameters_filename):
            with open( config['folders']['data'] + parameters_filename, 'rb') as file:
                parameter_values = pickle.load(file)
            for name in parameter_values:
                self.add_invariant(name, parameter_values[name])

    def update(self, parameter_value_dict: Dict[str, float]):
        """
        Update the values of several parameters at once. It's just an update i.e. each parameter in the list should have been added before.

        :param parameter_value_dict: _description_
        :type parameter_value_dict: Dict[str, float]
        :raises ValueError: _description_
        """
        for parameter_name in parameter_value_dict:
            parameter_value = parameter_value_dict[parameter_name]
            if parameter_name not in self._values:
                raise ValueError('parameter "%s" has not been created before update' % parameter_name)
            if parameter_value_dict[parameter_name] < self._bounds[parameter_name][0]:
                parameter_value_dict[parameter_name] = self._bounds[parameter_name][0]
            if parameter_value_dict[parameter_name] > self._bounds[parameter_name][1]:
                parameter_value_dict[parameter_name] = self._bounds[parameter_name][1]
            else:
                self._values[parameter_name] = parameter_value

    def __str__(self) -> str:
        """
        Return a string representation of a parameter set

        :return: a string representation of a parameter set
        :rtype: str
        """
        string: str= ''
        for p in self.invariant_names:
            if p in self.adjustables_names:
                string += '\t- adjustable parameter %s=%f in (%f, %f)\n' % (p, self(p), self.bounds(p)[0], self.bounds(p)[1])
            else:
                string += '\t- invariant parameter %s=%f\n' % (p, self(p))
        for p in self.time_varying_names:
            string += '\t- time varying parameter %s=%f in (%f, %f)\n' % (p, self(p), self.bounds(p)[0], self.bounds(p)[1])
        return string