from sklearn.ensemble import RandomForestClassifier


X = [ [0, 0], [1, 1], [0, 1] ]  # training data (2 features 3 samples)
Y = [0, 1, 0]  # labelling data
classifier = RandomForestClassifier(n_estimators=10, random_state=0, criterion='entropy',max_depth=None)#n_estimators is the number of trees in the forest
classifier.fit(X, Y) # fitting the tree according to training and labelling data
print(classifier.predict([[2, 2]])) # predict the label (validation)