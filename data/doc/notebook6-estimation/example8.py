import sqlite3
import numpy
from time import strftime, localtime, strptime, mktime
from matplotlib.pylab import *
from sklearn.ensemble import RandomForestClassifier


def to_epochtime(datetime):  # epoch time in seconds
    epochdate=mktime(strptime(datetime, '%d/%m/%Y %H:%M:%S'))
    return int(epochdate)


def to_datetime(epochtime):  # epoch time in seconds
    return strftime('%d/%m/%Y %H:%M:%S', localtime(int(round(epochtime, 0))))


def to_timequantum(epochtime, seconds_in_timequantum: int):# epoch time in seconds
    return (epochtime // seconds_in_timequantum) * seconds_in_timequantum


class DatabaseExtraction():

    def __init__(self, database_name: str, starting_datetime: '%d/%m/%Y %H:%M:%S', ending_datetime: '%d/%m/%Y %H:%M:%S', seconds_in_sample: int=1800):
        self.connection = sqlite3.connect(database_name)
        self.starting_datetime = starting_datetime
        self.ending_datetime = ending_datetime
        self.seconds_in_timequantum = seconds_in_sample

    def get_seconds_in_sample(self):
        return self.seconds_in_timequantum

    def get_raw_measurement(self, variable_name: str, remove_outliers: bool=True, outlier_sensitivity: float=5):
        cursor = self.connection.cursor()
        cursor.execute('SELECT time, val FROM %s WHERE time>=%i AND time<%i ORDER BY time ASC' % (variable_name, to_epochtime(self.starting_datetime) * 1000, to_epochtime(self.ending_datetime) * 1000))
        epochtimes, values = [], []
        for record in cursor.fetchall():
            epochtimes.append(int(record[0]) // 1000)
            values.append(record[1])
        if remove_outliers:
            delta_values = [values[k + 1] - values[k] for k in range(len(values)-1)]
            mean_delta = mean(delta_values)
            sigma_delta = numpy.std(delta_values)
            for k in range(1,len(values)-1):
                pdiff = values[k] - values[k-1]
                fdiff = values[k+1] - values[k]
                if (abs(pdiff) > mean_delta + outlier_sensitivity * sigma_delta) and (abs(fdiff) > mean_delta + outlier_sensitivity * sigma_delta) and pdiff * fdiff > 0:
                    print('Outlier detected in %s at time %s: %f (replaced by average value)' % (variable_name, to_datetime(epochtimes[k]), values[k]))
                    values[k] = (values[k-1] + values[k+1])/2
        return epochtimes, values

    def get_counter_measurement(self, variable_name: str):
        data_epochtimes, data_values = self.get_raw_measurement(variable_name, remove_outliers=False)
        counters_dict = dict()
        for i in range(len(data_epochtimes)):
            if data_values[i] == 1:
                datasample = self.to_datasample_epochtime(data_epochtimes[i])
                if datasample in counters_dict:
                    counters_dict[datasample] += 1
                else:
                    counters_dict[datasample] = 1
        sampled_data_epochtimes = [datasample for datasample in range(self.to_datasample_epochtime(to_epochtime(self.starting_datetime)), self.to_datasample_epochtime(to_epochtime(self.ending_datetime)), self.seconds_in_timequantum)]
        sampled_data_values = []
        for i in range(len(sampled_data_epochtimes)):
            if sampled_data_epochtimes[i] in counters_dict:
                sampled_data_values.append(counters_dict[sampled_data_epochtimes[i]])
            else:
                sampled_data_values.append(0)
        return sampled_data_epochtimes, sampled_data_values


    def get_sampled_measurement(self, variable_name: str, remove_outliers: bool=True, outlier_sensitivity: float=5):
        data_epochtimes, data_values = self.get_raw_measurement(variable_name, remove_outliers, outlier_sensitivity)
        if len(data_epochtimes) == 0:
            return None
        augmented_data_epochtimes = [self.to_datasample_epochtime(to_epochtime(self.starting_datetime))]
        augmented_data_values = [data_values[0]]
        for i in range(len(data_epochtimes)):
            data_epochtime = data_epochtimes[i]
            for epochtime in range(self.to_datasample_epochtime(augmented_data_epochtimes[-1]) + self.seconds_in_timequantum, self.to_datasample_epochtime(data_epochtime), self.seconds_in_timequantum):
                augmented_data_epochtimes.append(epochtime)
                augmented_data_values.append(augmented_data_values[-1])
            if self.to_datasample_epochtime(data_epochtime) > self.to_datasample_epochtime(augmented_data_epochtimes[-1]):
                augmented_data_epochtimes.append(self.to_datasample_epochtime(data_epochtime))
                augmented_data_values.append(augmented_data_values[-1])
            augmented_data_epochtimes.append(data_epochtime)
            augmented_data_values.append(data_values[i])
        for epochtime in range(self.to_datasample_epochtime(augmented_data_epochtimes[-1]), to_epochtime(self.ending_datetime), self.seconds_in_timequantum):
            augmented_data_epochtimes.append(epochtime + self.seconds_in_timequantum)
            augmented_data_values.append(augmented_data_values[-1])
        sampled_data_epochtimes, sampled_data_values = [], []
        integrator = 0
        for i in range(1,len(augmented_data_epochtimes)):
            previous_epochtime = augmented_data_epochtimes[i - 1]
            previous_datasample_epochtime = self.to_datasample_epochtime(augmented_data_epochtimes[i-1])
            epochtime = augmented_data_epochtimes[i]
            datasample_epochtime = self.to_datasample_epochtime(augmented_data_epochtimes[i])
            previous_value = augmented_data_values[i - 1]
            integrator += (epochtime - previous_epochtime) * previous_value
            if datasample_epochtime > previous_datasample_epochtime:
                sampled_data_epochtimes.append(previous_datasample_epochtime)
                sampled_data_values.append(integrator / self.seconds_in_timequantum)
                integrator = 0
        return sampled_data_epochtimes, sampled_data_values

    def to_datasample_epochtime(self, epochtime):
        return (epochtime // self.seconds_in_timequantum) * self.seconds_in_timequantum

    def close(self):
        self.connection.commit()
        self.connection.close()


def discretize(value, bounds=((0,0), (0,1), (1,1))):
    for i in range(len(bounds)):
        if value >= bounds[i][0] and value<= bounds[i][1]:
            return i
    return -1


def average_error(actual_occupants):
    level_midpoint=[]
    i = 0
    for i in range(0,len(actual_occupants),1):
        x = actual_occupants[i]
        if (x == 0 ):
            level_midpoint.append(0)
        else:
            if (0<actual_occupants[i]<2):
                level_midpoint.append(1)
            else:
                if (actual_occupants[i]>=2):
                    level_midpoint.append(3.15)
    return level_midpoint

def decision_tree(label, feature1, feature2, feature3, test_label, test_feature1, test_feature2, test_feature3):
    X, Y = [], []
    for i in range(0,len(label)):
        training_features = [feature1[i],feature2[i],feature3[i]]
        X.append(training_features)
        Y.append(label[i])

    classifier = RandomForestClassifier(n_estimators=10, random_state=0, criterion='entropy',max_depth=None)#n_estimators is the number of trees in the forest
    classifier.fit(X, Y)
    features_importance = classifier.feature_importances_
    X_t = []
    for i in range(0, len(test_label)):
        test_features = [test_feature1[i],test_feature2[i],test_feature3[i]]
        X_t.append(test_features)
    Y_t=classifier.predict(X_t)
    accuracy = classifier.score(X_t, test_label)
    #save the tree

    return accuracy, features_importance, Y_t, classifier


seconds_in_sample = 1800
database_extraction = DatabaseExtraction('office4.db', '4/05/2015 00:30:00', '15/05/2015 00:30:00', seconds_in_sample)
times, occupancies = database_extraction.get_sampled_measurement('labels')
times, co2in = database_extraction.get_sampled_measurement('co2')
for i in range(len(co2in)):  # removal of outliers
   if co2in[i] >= 2500:
       co2in[i] = 390
times, Tin = database_extraction.get_sampled_measurement('Temperature')
times, door = database_extraction.get_sampled_measurement('Door_contact')
times, RMS = database_extraction.get_sampled_measurement('RMS')

database_extraction = DatabaseExtraction('testing.db', '17/05/2015 00:30:00', '21/05/2015 00:30:00', seconds_in_sample)
times_test, occupancies_test = database_extraction.get_sampled_measurement('labels')

time_test, co2in_test = database_extraction.get_sampled_measurement('co2')
times_test, Tin_test = database_extraction.get_sampled_measurement('Temperature')
times_test, door_test = database_extraction.get_sampled_measurement('Door_contact')
times_test, RMS_test = database_extraction.get_sampled_measurement('RMS')

label = [discretize(occupancies[i], ( (0, 2), (2, 6) )) for i in range(len(occupancies))]
label_test=[discretize(occupancies_test[i], ( (0, 2), (2, 6) )) for i in range(len(occupancies_test))]
print("applying the decision tree classification..............")
accuracy, features_importance, estimated_occupancy, clasifier = decision_tree (label,RMS, co2in, door, label_test, RMS_test, co2in_test, door_test)
labelerror=average_error(estimated_occupancy)

print("Accuracy = ",accuracy)
print("Features Importance(microphone,physical model,door position) = ",features_importance)
error = []
for i in range(len(label_test)):
    s = labelerror[i] - occupancies_test[i]
    error.append(abs(s))
average_error = (numpy.sum(error)) / len(labelerror)
print("average_error = ", average_error)
plot(label_test, 'bo-', label='actual_level')
plot(estimated_occupancy, 'ro-', label='predicited _level')
plt.ylabel('Occupancy Level')
plt.xlabel('Time Quantum')
axis('tight')
grid()
plt.legend(('measured','estimated'), loc =0)
plt.show()