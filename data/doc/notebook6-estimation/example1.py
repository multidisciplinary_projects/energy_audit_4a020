import math

X1 = [0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1]
X2 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1]
X3 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

def entropy(variable_data): #occupancy is the target
    count0, count1 = 0, 0  # counter for the class0 and class1 in the occupancy column
    for i in range(len(variable_data)):
        if variable_data[i] == 0: count0 += 1
        else:  count1 += 1
    p0, p1 = count0 / len(variable_data), count1 / len(variable_data)
    v0 = math.log(p0, 2) if p0 != 0 else 0  # to avoid log(0)
    v1 = math.log(p1, 2) if p1 != 0 else 0  # to avoid log(0)
    return - p0 * v0 - p1 * v1

print("entropy(x1)=",entropy(X1))  # 1.0
print("entropy(x2)=",entropy(X2))  # 0.41381685030363374
print("entropy(x3)=",entropy(X3))  # 0.0