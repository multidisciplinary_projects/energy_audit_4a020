import sqlite3
import numpy
from time import strftime, localtime, strptime, mktime
from matplotlib.pylab import *

# calculate entropy and information gain


def to_epochtime(datetime):  # epoch time in seconds
    epochdate=mktime(strptime(datetime, '%d/%m/%Y %H:%M:%S'))
    return int(epochdate)


def to_datetime(epochtime):  # epoch time in seconds
    return strftime('%d/%m/%Y %H:%M:%S', localtime(int(round(epochtime, 0))))


def to_timequantum(epochtime, seconds_in_timequantum: int):# epoch time in seconds
    return (epochtime // seconds_in_timequantum) * seconds_in_timequantum


class DatabaseExtraction():

    def __init__(self, database_name: str, starting_datetime: '%d/%m/%Y %H:%M:%S', ending_datetime: '%d/%m/%Y %H:%M:%S', seconds_in_sample: int=1800):
        self.connection = sqlite3.connect(database_name)
        self.starting_datetime = starting_datetime
        self.ending_datetime = ending_datetime
        self.seconds_in_timequantum = seconds_in_sample

    def get_seconds_in_sample(self):
        return self.seconds_in_timequantum

    def get_raw_measurement(self, variable_name: str, remove_outliers: bool=True, outlier_sensitivity: float=5):
        cursor = self.connection.cursor()
        cursor.execute('SELECT time, val FROM %s WHERE time>=%i AND time<%i ORDER BY time ASC' % (variable_name, to_epochtime(self.starting_datetime) * 1000, to_epochtime(self.ending_datetime) * 1000))
        epochtimes, values = [], []
        for record in cursor.fetchall():
            epochtimes.append(int(record[0]) // 1000)
            values.append(record[1])
        if remove_outliers:
            delta_values = [values[k + 1] - values[k] for k in range(len(values)-1)]
            mean_delta = mean(delta_values)
            sigma_delta = numpy.std(delta_values)
            for k in range(1,len(values)-1):
                pdiff = values[k] - values[k-1]
                fdiff = values[k+1] - values[k]
                if (abs(pdiff) > mean_delta + outlier_sensitivity * sigma_delta) and (abs(fdiff) > mean_delta + outlier_sensitivity * sigma_delta) and pdiff * fdiff > 0:
                    print('Outlier detected in %s at time %s: %f (replaced by average value)' % (variable_name, to_datetime(epochtimes[k]), values[k]))
                    values[k] = (values[k-1] + values[k+1])/2
        return epochtimes, values

    def get_counter_measurement(self, variable_name: str):
        data_epochtimes, data_values = self.get_raw_measurement(variable_name, remove_outliers=False)
        counters_dict = dict()
        for i in range(len(data_epochtimes)):
            if data_values[i] == 1:
                datasample = self.to_datasample_epochtime(data_epochtimes[i])
                if datasample in counters_dict:
                    counters_dict[datasample] += 1
                else:
                    counters_dict[datasample] = 1
        sampled_data_epochtimes = [datasample for datasample in range(self.to_datasample_epochtime(to_epochtime(self.starting_datetime)), self.to_datasample_epochtime(to_epochtime(self.ending_datetime)), self.seconds_in_timequantum)]
        sampled_data_values = []
        for i in range(len(sampled_data_epochtimes)):
            if sampled_data_epochtimes[i] in counters_dict:
                sampled_data_values.append(counters_dict[sampled_data_epochtimes[i]])
            else:
                sampled_data_values.append(0)
        return sampled_data_epochtimes, sampled_data_values


    def get_sampled_measurement(self, variable_name: str, remove_outliers: bool=True, outlier_sensitivity: float=5):
        data_epochtimes, data_values = self.get_raw_measurement(variable_name, remove_outliers, outlier_sensitivity)
        if len(data_epochtimes) == 0:
            return None
        augmented_data_epochtimes = [self.to_datasample_epochtime(to_epochtime(self.starting_datetime))]
        augmented_data_values = [data_values[0]]
        for i in range(len(data_epochtimes)):
            data_epochtime = data_epochtimes[i]
            for epochtime in range(self.to_datasample_epochtime(augmented_data_epochtimes[-1]) + self.seconds_in_timequantum, self.to_datasample_epochtime(data_epochtime), self.seconds_in_timequantum):
                augmented_data_epochtimes.append(epochtime)
                augmented_data_values.append(augmented_data_values[-1])
            if self.to_datasample_epochtime(data_epochtime) > self.to_datasample_epochtime(augmented_data_epochtimes[-1]):
                augmented_data_epochtimes.append(self.to_datasample_epochtime(data_epochtime))
                augmented_data_values.append(augmented_data_values[-1])
            augmented_data_epochtimes.append(data_epochtime)
            augmented_data_values.append(data_values[i])
        for epochtime in range(self.to_datasample_epochtime(augmented_data_epochtimes[-1]), to_epochtime(self.ending_datetime), self.seconds_in_timequantum):
            augmented_data_epochtimes.append(epochtime + self.seconds_in_timequantum)
            augmented_data_values.append(augmented_data_values[-1])
        sampled_data_epochtimes, sampled_data_values = [], []
        integrator = 0
        for i in range(1,len(augmented_data_epochtimes)):
            previous_epochtime = augmented_data_epochtimes[i - 1]
            previous_datasample_epochtime = self.to_datasample_epochtime(augmented_data_epochtimes[i-1])
            epochtime = augmented_data_epochtimes[i]
            datasample_epochtime = self.to_datasample_epochtime(augmented_data_epochtimes[i])
            previous_value = augmented_data_values[i - 1]
            integrator += (epochtime - previous_epochtime) * previous_value
            if datasample_epochtime > previous_datasample_epochtime:
                sampled_data_epochtimes.append(previous_datasample_epochtime)
                sampled_data_values.append(integrator / self.seconds_in_timequantum)
                integrator = 0
        return sampled_data_epochtimes, sampled_data_values

    def to_datasample_epochtime(self, epochtime):
        return (epochtime // self.seconds_in_timequantum) * self.seconds_in_timequantum

    def close(self):
        self.connection.commit()
        self.connection.close()

def entropy(variable_data): #occupancy is the target
    count0, count1 = 0, 0  # counter for the class0 and class1 in the occupancy column
    for i in range(len(variable_data)):
        if variable_data[i] == 0: count0 += 1
        else:  count1 += 1
    p0, p1 = count0 / len(variable_data), count1 / len(variable_data)
    v0 = math.log(p0) / math.log(2) if p0 != 0 else 0  # to avoid log(0)
    v1 = math.log(p1) / math.log(2) if p1 != 0 else 0  # to avoid log(0)
    return - p0 * v0 - p1 * v1

def information_gain(feature_data, occupancy_data):
  occupancy_data0, occupancy_data1, feature_data0, feature_data1 = [], [], [], []
  for i in range(len(occupancy_data)):
     if feature_data[i] == 0:
        feature_data0.append(feature_data[i])
        occupancy_data0.append(occupancy_data[i])
     else:
        feature_data1.append(feature_data[i])
        occupancy_data1.append(occupancy_data[i])
  entropy1, entropy2 = entropy(occupancy_data0), entropy(occupancy_data1)
  return entropy(occupancy_data) - (len(feature_data0) / len(occupancy_data)) * entropy1 - (len(feature_data1) / len(occupancy_data)) * entropy2

def discretize(value, bounds=((0,0), (0,1), (1,1))): # discretization of window and door data
    for i in range(len(bounds)):
        if value >= bounds[i][0] and value<= bounds[i][1]:
            return i
    return -1

seconds_in_sample = 1800
database_extraction = DatabaseExtraction('testing.db', '17/05/2015 00:30:00', '21/05/2015 00:30:00', seconds_in_sample)
###################################
times, occupancies = database_extraction.get_sampled_measurement('labels')
times, door_openings = database_extraction.get_sampled_measurement('Door_contact')
times, window_openings = database_extraction.get_sampled_measurement('window')
times, microphone = database_extraction.get_sampled_measurement('RMS')

discretized_occupancies = [discretize(occupancy, ((0, 0.5), (0.5, 6))) for occupancy in occupancies]
discretized_door_openings = [discretize(door_opening, ((0,0), (0,1), (1,1))) for door_opening in door_openings]
discretized_window_openings = [discretize(window_opening, ((0,0), (0,1), (1,1))) for window_opening in window_openings]
discretized_microphone = [discretize(microphone, ((0,0.001), (0.001,0.1), (0.1,1))) for microphone in microphone]

print("1-information gain for door position=", information_gain(discretized_door_openings, discretized_occupancies))
print("2-information gain for window position=", information_gain(discretized_window_openings, discretized_occupancies))
print("3-information gain for microphone=", information_gain(discretized_microphone, discretized_occupancies))


