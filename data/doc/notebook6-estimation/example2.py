import math

door, window, occupancy_data = [], [], []
file = open("data.txt", 'r')
Title= file.readline()
for line in file:
  line_token = line.split(";")
  door.append(float(line_token[0])), window.append(float(line_token[1])), occupancy_data.append(float(line_token[2]))  # store the values of each column in example2.txt in 3 separate lists
file.close()
print(occupancy_data)

def entropy(variable_data): #occupancy is the target
    count0, count1 = 0, 0  # counter for the class0 and class1 in the occupancy column
    for i in range(len(variable_data)):
        if variable_data[i] == 0: count0 += 1
        else:  count1 += 1
    p0, p1 = count0 / len(variable_data), count1 / len(variable_data)
    v0 = math.log(p0, 2) if p0 != 0 else 0  # to avoid log(0)
    v1 = math.log(p1, 2) if p1 != 0 else 0  # to avoid log(0)
    return - p0 * v0 - p1 * v1
print("entropy=",entropy(occupancy_data))

def information_gain(feature_data, occupancy_data):
  occupancy_data0, occupancy_data1, feature_data0, feature_data1 = [], [], [], []
  for i in range(len(occupancy_data)):
     if feature_data[i] == 0:
        feature_data0.append(feature_data[i])
        occupancy_data0.append(occupancy_data[i])
     else:
        feature_data1.append(feature_data[i])
        occupancy_data1.append(occupancy_data[i])
  entropy0, entropy1 = entropy(occupancy_data0), entropy(occupancy_data1)
  return entropy(occupancy_data) - (len(feature_data0) / len(occupancy_data)) * entropy0 - (len(feature_data1) / len(occupancy_data)) * entropy1

info1=information_gain(door,occupancy_data)
print("1-information gain for door position=",info1)
info2=information_gain(window,occupancy_data)
print("2-information gain for window position=",info2)
