import numpy

actual_points,estimated1,estimated2=[],[],[]
file = open("data1.txt", 'r')
Title = file.readline()# read the first line of variable's names
for line in file:
    line_tokens = line.split(";")
    actual_points.append(float(line_tokens[0])), estimated1.append(float(line_tokens[1])), estimated2.append(float(line_tokens[2]))
file.close()
error,count =[],0
for i in range(len(actual_points)):
    distance = actual_points[i]-estimated1[i]
    if distance == 0: count += 1 # count the correctly estimated points to calculate the accuracy
    error.append(abs(distance))
accuracy = count * 100 / len(actual_points)  # accuracy %
average_error = (numpy.sum(error)) / len(actual_points)
print("average error = ", average_error)
print("accuracy=", accuracy, "%")