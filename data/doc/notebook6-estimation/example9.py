cost, error= [], []
file = open("data2.txt", 'r')
Title= file.readline()
for line in file:
  line_token = line.split(";")
  cost.append(float(line_token[0])), error.append(float(line_token[1]))
file.close()

def pareto_frontier(Xs,Ys, maxX = True, maxY = True):
#Default behaviour is to find the maximum for both X and Y, but the option is
#available to specify maxX = False or maxY = False to find the minimum for either
#or both of the parameters.

# Sort the list in either ascending or descending order of X
    myList = sorted([[Xs[i], Ys[i]] for i in range(len(Xs))], reverse=maxX)

# Start the Pareto frontier with the first value in the sorted list
    p_front = [myList[0]]

# Loop through the sorted list
    for pair in myList[1:]:
        if maxY:
            if pair[1] >= p_front[-1][1]: # Look for higher values of Y…
                p_front.append(pair) # … and add them to the Pareto frontier
        else:
            if pair[1] <= p_front[-1][1]: # Look for lower values of Y…
                p_front.append(pair) # … and add them to the Pareto frontier
# Turn resulting pairs back into a list of Xs and Ys
    p_frontX = [pair[0] for pair in p_front]
    p_frontY = [pair[1] for pair in p_front]
    return p_frontX, p_frontY

cost_pareto,error_pareto=(pareto_frontier(cost, error, maxX = False, maxY = False))
from matplotlib.pylab import *
plot(cost,error,'ro-')
plot(cost_pareto,error_pareto)
plt.ylabel('average error ')
plt.xlabel('cost')
plt.legend((' all estimation points ','pareto optimal estimation points'))
grid()
show()
