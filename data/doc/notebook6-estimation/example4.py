from sklearn import tree

X = [ [0, 0], [1, 1], [0, 1] ]  # training data (2 features 3 samples)
Y = [0, 1, 0]  # labelling data
classifier = tree.DecisionTreeClassifier(random_state=0, criterion='entropy', max_depth=None)
classifier.fit(X, Y)  # fit the tree according to training and labelling data
print(classifier.predict([[2, 2]]))  # predict the new target sample (validation)
with open("tree.dot", 'w') as file:
        f = tree.export_graphviz(classifier, out_file=file) # save the tree as dot file and open it using Graphviz or a text editor (at lear WordPad on Microsoft Windows)


