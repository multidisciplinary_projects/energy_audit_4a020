# blockhouse

## features

total surface: $S_h$

height: $H_h$

external size ratio/south $L \times \rho L$ → $L = \sqrt{\frac{S_h}{\rho}}$ → $\sqrt{\frac{S_h}{\rho}} \times \sqrt{\rho S_h}$

offset exposure wrt south : $\alpha$ 

outdoor wall thermal transmission coefficient: $U_w$

roof thermal transmission coefficient:  $U_r$

ground floor thermal transmission coefficient: $U_f$

glass insulation: $U_g$

day heating temperature set_points: $T_{h}=[.., ..., ...,]$

day cooling temperature set_points: $T_{c}=[.., ..., ...,]$

equivalent concrete inertial mass: thickness $d_i$  surface $S_i$ default = floor

ventilation in vol/h: $v$ for $n$ occupants to stay below 1000ppp 

average occupancy: $O_c$

average power / occupant: 100W+70W

far & close solar masks and glass direction : exposure, slope

ratio of glass per side $\beta$: 0%, 20%, 40%..., 100%

weather file

conditions for stopping heater 

________________________

## implications

glass surfaces: 

-  south: $\beta_{south}\sqrt{\rho S_h} H_h$ directed to $\alpha$ (west is positive angle)
- west: $\beta_{west}\sqrt{\frac{S_h}{\rho}} H_h$ directed to $\alpha + \pi/2$

- north: $\beta_{north}\sqrt{\rho S_h} H_h$ directed to $\alpha + \pi$
- east: east: $\beta_{east}\sqrt{\frac{S_h}{\rho}} H_h$ directed to $\alpha - \pi/2$

outdoor loss surface: $S_h H_h + (1-\beta_{south})\sqrt{\rho S_h}H_h + (1-\beta_{west})\sqrt{\frac{S_h}{\rho}}H_h + (1-\beta_{north})\sqrt{\rho S_h}H_h + (1-\beta_{east})\sqrt{\frac{S_h}{\rho}} H_h$

gound loss surface: $S_h$

## model

without heating / cooling : gains, Tout → distrib(Tin) without heating/cooling + ratio of comfortable temperature hours (between 21 and 26°C) 

with heating / cooling : gains, Tout, Tset → heating/cooling energy need

## indicators

$E_{href}$=reference heating_energy_need( $\beta_{default}$,  $\beta_{default}$,  $\beta_{default}$, $,  $ $\beta_{default}$,$\rho$=1 ) → in m2 of 12% PV panels south oriented with α, best slope → in euros → in kWh

$E_{cref}$=reference cooling_energy_need( $\beta_{default}$,  $\beta_{default}$,  $\beta_{default}$, $,  $ $\beta_{default}$,$\rho$=1 ) → in m2 of 12% PV panels south oriented with α, best slope → in euros → in kWh

- Parametric analysis:

(heating_energy_need($\beta_{south}$, $\beta_{west}$, $\beta_{north}$, $\beta_{east}$, $\rho$ )-$E_{href}$)/$E_{href}$; $\beta_{direction} \in \{0, 0.2, \dots, 1\}$ (with $\beta_{default}$) and $\rho \in \{.5, 1, 1.5\}$ 

(cooling_energy_need($\beta_{south}$, $\beta_{west}$, $\beta_{north}$, $\beta_{east}$, $\rho$ )-$E_{cref}$)/$E_{cref}; $$\beta_{direction} \in \{0, 0.2, \dots, 1\}$ (with $\beta_{default}$) and $\rho \in \{.5, 1, 1.5\}$ →in m2 of 12% PV panels south oriented with α, best slope

 