"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""

from typing import Dict, List

import sites.data_h358
import model_h358
import time
#from buildingenergy.building import InterfaceType, Site
from buildingenergy.data import Data
from buildingenergy.model import Model
from buildingenergy.parameters import ParameterSet
from buildingenergy.runner import Runner


class H358runner(Runner):

    def __init__(self) -> None:
        super().__init__((('TZcorridor', 'Tcorridor'), ('TZdownstairs', 'Tdownstairs'), ('TZoutdoor', 'weather_temperature'), ('PZoffice', [
            'total_electric_power', 'Psun_window', 'Pmetabolism', 'Pheater']), ('CCO2_corridor', 'corridor_CO2_concentration'), ('CCO2_outdoor', 'outdoorCCO2'), ('PCO2_office', 'CO2production'), ('Qoutdoor', 'Qoutdoor'), ('Qcorridor', 'Qcorridor'), ('TZoffice', 'Toffice_reference'), ('CCO2_office', 'office_CO2_concentration'), ('CCO2_corridor', 'corridor_CO2_concentration')))

    def make_parameter_set(self) -> ParameterSet:
        parameter_set = ParameterSet(resolution=100)
        parameter_set.add_time_varying('Qoutdoor', 100/3600, 2/3600, 540/3600)
        parameter_set.add_invariant('Qoutdoor0', 100/3600, 2/3600, 100/3600)
        parameter_set.add_invariant('Qoutdoor_window', 210/3600, 1/3600, 500/3600)
        parameter_set.add_invariant('Qoutdoor_door', 1/3600, 1/3600, 500/3600)
        
        parameter_set.add_time_varying('Qcorridor', 200/3600, 2/3600, 540/3600)
        parameter_set.add_invariant('Qcorridor0', 200/3600, 2/3600, 100/3600)
        parameter_set.add_invariant('Qcorridor_window', 1/3600, 1/3600, 500/3600)
        parameter_set.add_invariant('Qcorridor_door', 296/3600, 1/3600, 500/3600)
        
        parameter_set.add_invariant('slab_effective_thickness', 12e-2, 3e-2, 20e-2)
        parameter_set.add_invariant('room_volume', 89, 45, 100)
        parameter_set.add_invariant('psi_bridge', 0.5 * 0.99, 0.0 * 0.99, 0.5 * 5)  #1.95
        parameter_set.add_invariant('foam_thickness', 34e-3, 1034e-3, 50e-3)
        parameter_set.add_invariant('body_metabolism', 60, 50, 200)
        parameter_set.add_invariant('heater_power_per_delta_surface_temperature', 90, 50, 200)
        parameter_set.add_invariant('CO2_occupant_breath_production', 2, 1, 20)
        parameter_set.add_invariant('solar_factor', .4, 0, 1)
        return parameter_set

    def make_invariant_data(self) -> Data:
        return data_h358.H358Data('16/02/2015', '16/02/2016') # '16/02/2015', '23/02/2015'

    def make_parameterized_data(self, parameter_set, data):
        data_h358.Qoutdoor(data, parameter_set)
        data_h358.Qcorridor(data, parameter_set)
        data_h358.CO2production(data, parameter_set)
        data_h358.Pheater(data, parameter_set)
        data_h358.Pheat(data, parameter_set)
        data_h358.Pmetabolim(data, parameter_set)

    def make_model(self, inparam_set: ParameterSet) -> Model:
        return model_h358.H358Model(order=None, parameter_set=inparam_set)

    def get_controlled_linear_inputs(self, k: int, linear_input_names: List[str]) -> Dict[str, float]:
        pass

    def get_controlled_nonlinear_inputs(self, k: int, nonlinear_input_names: List[str]) -> Dict[str, float]:
        pass


def main():
    runner = H358runner()
    start_time = time.time()
    runner.simulate()
    
    #runner.memorize('_sim', inputs=False)
    #runner.fit(learning_ratio=2/3., suffix='*', maxiter=30, popsize=20, tol=1e-3, workers=0)
    #runner.simulate()
    runner.memorize('_sim', inputs=False)
    # runner.sensitivity(number_of_trajectories=10, number_of_levels=4)
    duration = (time.time() - start_time) 
    print('Operation time: %f seconds' % duration)
    runner.plot()


if __name__ == '__main__':
    main()

    # yappi -f pstat -o profiling.out runner_h358.py
    # snakeviz profiling.out
