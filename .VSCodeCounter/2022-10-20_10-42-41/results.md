# Summary

Date : 2022-10-20 10:42:41

Directory /Users/stephane/Documents/enseignements/BATEM/supports/src

Total : 87 files,  19598 codes, 4501 comments, 1365 blanks, all 25464 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 29 | 11,933 | 683 | 20 | 12,636 |
| Python | 46 | 6,737 | 3,806 | 1,289 | 11,832 |
| XML | 7 | 817 | 0 | 12 | 829 |
| pip requirements | 1 | 54 | 0 | 1 | 55 |
| Markdown | 1 | 38 | 0 | 37 | 75 |
| JavaScript | 2 | 12 | 12 | 6 | 30 |
| Ini | 1 | 7 | 0 | 0 | 7 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 87 | 19,598 | 4,501 | 1,365 | 25,464 |
| .ipynb_checkpoints | 1 | 585 | 0 | 1 | 586 |
| buildingenergy | 16 | 4,382 | 3,126 | 733 | 8,241 |
| data | 27 | 1,441 | 11 | 156 | 1,608 |
| data/doc | 17 | 1,431 | 11 | 154 | 1,596 |
| data/doc/notebook6-estimation | 16 | 1,393 | 11 | 117 | 1,521 |
| data/doc/notebook6-estimation/.idea | 7 | 817 | 0 | 12 | 829 |
| data/doc/notebook6-estimation/.idea/scopes | 1 | 5 | 0 | 0 | 5 |
| old | 7 | 714 | 332 | 126 | 1,172 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)