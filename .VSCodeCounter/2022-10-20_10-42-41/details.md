# Details

Date : 2022-10-20 10:42:41

Directory /Users/stephane/Documents/enseignements/BATEM/supports/src

Total : 87 files,  19598 codes, 4501 comments, 1365 blanks, all 25464 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.ipynb_checkpoints/notebook3_dynamic-checkpoint.ipynb](/.ipynb_checkpoints/notebook3_dynamic-checkpoint.ipynb) | JSON | 585 | 0 | 1 | 586 |
| [__init__.py](/__init__.py) | Python | 1 | 0 | 0 | 1 |
| [airflow_model.ipynb](/airflow_model.ipynb) | JSON | 132 | 0 | 1 | 133 |
| [apartment2zones.py](/apartment2zones.py) | Python | 74 | 8 | 17 | 99 |
| [building_h358.py](/building_h358.py) | Python | 73 | 13 | 21 | 107 |
| [buildingdesigner.ipynb](/buildingdesigner.ipynb) | JSON | 1,626 | 0 | 1 | 1,627 |
| [buildingenergy/__init__.py](/buildingenergy/__init__.py) | Python | 1 | 4 | 1 | 6 |
| [buildingenergy/building.py](/buildingenergy/building.py) | Python | 583 | 412 | 96 | 1,091 |
| [buildingenergy/data.py](/buildingenergy/data.py) | Python | 304 | 229 | 54 | 587 |
| [buildingenergy/dynprog.py](/buildingenergy/dynprog.py) | Python | 180 | 104 | 30 | 314 |
| [buildingenergy/lambdahouse.py](/buildingenergy/lambdahouse.py) | Python | 658 | 64 | 74 | 796 |
| [buildingenergy/linreg.py](/buildingenergy/linreg.py) | Python | 299 | 157 | 29 | 485 |
| [buildingenergy/model.py](/buildingenergy/model.py) | Python | 196 | 167 | 33 | 396 |
| [buildingenergy/model_old.py](/buildingenergy/model_old.py) | Python | 278 | 323 | 52 | 653 |
| [buildingenergy/openweather.py](/buildingenergy/openweather.py) | Python | 162 | 143 | 27 | 332 |
| [buildingenergy/parameters.py](/buildingenergy/parameters.py) | Python | 122 | 154 | 24 | 300 |
| [buildingenergy/physics.py](/buildingenergy/physics.py) | Python | 109 | 181 | 31 | 321 |
| [buildingenergy/runner.py](/buildingenergy/runner.py) | Python | 277 | 148 | 65 | 490 |
| [buildingenergy/solar.py](/buildingenergy/solar.py) | Python | 506 | 407 | 94 | 1,007 |
| [buildingenergy/thermal.py](/buildingenergy/thermal.py) | Python | 511 | 361 | 69 | 941 |
| [buildingenergy/thermics.py](/buildingenergy/thermics.py) | Python | 164 | 197 | 31 | 392 |
| [buildingenergy/timemg.py](/buildingenergy/timemg.py) | Python | 32 | 75 | 23 | 130 |
| [coimbra.py](/coimbra.py) | Python | 42 | 11 | 6 | 59 |
| [data/briancon.json](/data/briancon.json) | JSON | 1 | 0 | 0 | 1 |
| [data/bucharest-unirii.json](/data/bucharest-unirii.json) | JSON | 1 | 0 | 1 | 2 |
| [data/coimbra.json](/data/coimbra.json) | JSON | 1 | 0 | 0 | 1 |
| [data/doc/lambdahouse.md](/data/doc/lambdahouse.md) | Markdown | 38 | 0 | 37 | 75 |
| [data/doc/notebook6-estimation/.idea/encodings.xml](/data/doc/notebook6-estimation/.idea/encodings.xml) | XML | 4 | 0 | 2 | 6 |
| [data/doc/notebook6-estimation/.idea/examples.iml](/data/doc/notebook6-estimation/.idea/examples.iml) | XML | 8 | 0 | 2 | 10 |
| [data/doc/notebook6-estimation/.idea/misc.xml](/data/doc/notebook6-estimation/.idea/misc.xml) | XML | 9 | 0 | 2 | 11 |
| [data/doc/notebook6-estimation/.idea/modules.xml](/data/doc/notebook6-estimation/.idea/modules.xml) | XML | 8 | 0 | 2 | 10 |
| [data/doc/notebook6-estimation/.idea/scopes/scope_settings.xml](/data/doc/notebook6-estimation/.idea/scopes/scope_settings.xml) | XML | 5 | 0 | 0 | 5 |
| [data/doc/notebook6-estimation/.idea/vcs.xml](/data/doc/notebook6-estimation/.idea/vcs.xml) | XML | 6 | 0 | 2 | 8 |
| [data/doc/notebook6-estimation/.idea/workspace.xml](/data/doc/notebook6-estimation/.idea/workspace.xml) | XML | 777 | 0 | 2 | 779 |
| [data/doc/notebook6-estimation/example1.py](/data/doc/notebook6-estimation/example1.py) | Python | 16 | 0 | 3 | 19 |
| [data/doc/notebook6-estimation/example2.py](/data/doc/notebook6-estimation/example2.py) | Python | 34 | 0 | 5 | 39 |
| [data/doc/notebook6-estimation/example3.py](/data/doc/notebook6-estimation/example3.py) | Python | 131 | 2 | 26 | 159 |
| [data/doc/notebook6-estimation/example4.py](/data/doc/notebook6-estimation/example4.py) | Python | 8 | 0 | 4 | 12 |
| [data/doc/notebook6-estimation/example5.py](/data/doc/notebook6-estimation/example5.py) | Python | 169 | 1 | 29 | 199 |
| [data/doc/notebook6-estimation/example6.py](/data/doc/notebook6-estimation/example6.py) | Python | 17 | 0 | 1 | 18 |
| [data/doc/notebook6-estimation/example7.py](/data/doc/notebook6-estimation/example7.py) | Python | 6 | 0 | 2 | 8 |
| [data/doc/notebook6-estimation/example8.py](/data/doc/notebook6-estimation/example8.py) | Python | 166 | 1 | 29 | 196 |
| [data/doc/notebook6-estimation/example9.py](/data/doc/notebook6-estimation/example9.py) | Python | 29 | 7 | 6 | 42 |
| [data/engins.json](/data/engins.json) | JSON | 1 | 0 | 0 | 1 |
| [data/grenoble1979-2022.json](/data/grenoble1979-2022.json) | JSON | 1 | 0 | 0 | 1 |
| [data/grenoble_weather2015-2019.json](/data/grenoble_weather2015-2019.json) | JSON | 1 | 0 | 1 | 2 |
| [data/refuge-des-bans.json](/data/refuge-des-bans.json) | JSON | 1 | 0 | 0 | 1 |
| [data/tirana.json](/data/tirana.json) | JSON | 1 | 0 | 0 | 1 |
| [data/villard-de-Lans_autrans_2000-2022.json](/data/villard-de-Lans_autrans_2000-2022.json) | JSON | 1 | 0 | 0 | 1 |
| [data/viriville.json](/data/viriville.json) | JSON | 1 | 0 | 0 | 1 |
| [data_h358.py](/data_h358.py) | Python | 76 | 9 | 37 | 122 |
| [lambda_StMartinVinoux.ipynb](/lambda_StMartinVinoux.ipynb) | JSON | 373 | 0 | 1 | 374 |
| [lambda_VillardDeLansAutrans.ipynb](/lambda_VillardDeLansAutrans.ipynb) | JSON | 346 | 0 | 1 | 347 |
| [lambda_bans.ipynb](/lambda_bans.ipynb) | JSON | 377 | 0 | 1 | 378 |
| [lambda_briancon.ipynb](/lambda_briancon.ipynb) | JSON | 345 | 0 | 1 | 346 |
| [lambda_bucharest.ipynb](/lambda_bucharest.ipynb) | JSON | 368 | 0 | 1 | 369 |
| [lambda_coimbra.ipynb](/lambda_coimbra.ipynb) | JSON | 345 | 0 | 1 | 346 |
| [lambda_engins.ipynb](/lambda_engins.ipynb) | JSON | 347 | 0 | 1 | 348 |
| [lambda_tirana.ipynb](/lambda_tirana.ipynb) | JSON | 368 | 0 | 1 | 369 |
| [modelRC.ipynb](/modelRC.ipynb) | JSON | 626 | 0 | 1 | 627 |
| [model_h358.py](/model_h358.py) | Python | 95 | 33 | 23 | 151 |
| [notebook1_context.ipynb](/notebook1_context.ipynb) | JSON | 1,513 | 0 | 1 | 1,514 |
| [notebook2_static.ipynb](/notebook2_static.ipynb) | JSON | 77 | 683 | 0 | 760 |
| [notebook3_dynamic.ipynb](/notebook3_dynamic.ipynb) | JSON | 585 | 0 | 1 | 586 |
| [notebook4_learning.ipynb](/notebook4_learning.ipynb) | JSON | 1,209 | 0 | 1 | 1,210 |
| [notebook5_management.ipynb](/notebook5_management.ipynb) | JSON | 512 | 0 | 1 | 513 |
| [notebook_teet.ipynb](/notebook_teet.ipynb) | JSON | 1,556 | 0 | 1 | 1,557 |
| [old/h358dynprog_old.py](/old/h358dynprog_old.py) | Python | 28 | 22 | 6 | 56 |
| [old/h358linreg_old.py](/old/h358linreg_old.py) | Python | 51 | 8 | 21 | 80 |
| [old/h358measurements_old.py](/old/h358measurements_old.py) | Python | 37 | 19 | 9 | 65 |
| [old/h358model_old.py](/old/h358model_old.py) | Python | 276 | 86 | 35 | 397 |
| [old/h358parameters_old.py](/old/h358parameters_old.py) | Python | 147 | 87 | 18 | 252 |
| [old/h358simulator_old.py](/old/h358simulator_old.py) | Python | 129 | 106 | 22 | 257 |
| [old/h358sliding_old.py](/old/h358sliding_old.py) | Python | 46 | 4 | 15 | 65 |
| [requirements.txt](/requirements.txt) | pip requirements | 54 | 0 | 1 | 55 |
| [runner_h358.py](/runner_h358.py) | Python | 57 | 11 | 20 | 88 |
| [setup.ini](/setup.ini) | Ini | 7 | 0 | 0 | 7 |
| [solar_building.ipynb](/solar_building.ipynb) | JSON | 633 | 0 | 1 | 634 |
| [solar_h358.py](/solar_h358.py) | Python | 38 | 2 | 5 | 45 |
| [solr_pvlib.py](/solr_pvlib.py) | Python | 437 | 212 | 167 | 816 |
| [thermal_building_h358.py](/thermal_building_h358.py) | Python | 75 | 7 | 19 | 101 |
| [tirana.py](/tirana.py) | Python | 40 | 14 | 6 | 60 |
| [vandelay-js.js](/vandelay-js.js) | JavaScript | 6 | 6 | 3 | 15 |
| [vandelay-py.js](/vandelay-py.js) | JavaScript | 6 | 6 | 3 | 15 |
| [weather_bans.py](/weather_bans.py) | Python | 24 | 4 | 1 | 29 |
| [weather_engins.py](/weather_engins.py) | Python | 21 | 0 | 2 | 23 |
| [weather_h358.py](/weather_h358.py) | Python | 12 | 13 | 1 | 26 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)