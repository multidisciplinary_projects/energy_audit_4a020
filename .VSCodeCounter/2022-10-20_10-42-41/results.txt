Date : 2022-10-20 10:42:41
Directory : /Users/stephane/Documents/enseignements/BATEM/supports/src
Total : 87 files,  19598 codes, 4501 comments, 1365 blanks, all 25464 lines

Languages
+------------------+------------+------------+------------+------------+------------+
| language         | files      | code       | comment    | blank      | total      |
+------------------+------------+------------+------------+------------+------------+
| JSON             |         29 |     11,933 |        683 |         20 |     12,636 |
| Python           |         46 |      6,737 |      3,806 |      1,289 |     11,832 |
| XML              |          7 |        817 |          0 |         12 |        829 |
| pip requirements |          1 |         54 |          0 |          1 |         55 |
| Markdown         |          1 |         38 |          0 |         37 |         75 |
| JavaScript       |          2 |         12 |         12 |          6 |         30 |
| Ini              |          1 |          7 |          0 |          0 |          7 |
+------------------+------------+------------+------------+------------+------------+

Directories
+--------------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                                                     | files      | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                                                        |         87 |     19,598 |      4,501 |      1,365 |     25,464 |
| .ipynb_checkpoints                                                                                                       |          1 |        585 |          0 |          1 |        586 |
| buildingenergy                                                                                                           |         16 |      4,382 |      3,126 |        733 |      8,241 |
| data                                                                                                                     |         27 |      1,441 |         11 |        156 |      1,608 |
| data/doc                                                                                                                 |         17 |      1,431 |         11 |        154 |      1,596 |
| data/doc/notebook6-estimation                                                                                            |         16 |      1,393 |         11 |        117 |      1,521 |
| data/doc/notebook6-estimation/.idea                                                                                      |          7 |        817 |          0 |         12 |        829 |
| data/doc/notebook6-estimation/.idea/scopes                                                                               |          1 |          5 |          0 |          0 |          5 |
| old                                                                                                                      |          7 |        714 |        332 |        126 |      1,172 |
+--------------------------------------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+--------------------------------------------------------------------------------------------------------------------------+------------------+------------+------------+------------+------------+
| filename                                                                                                                 | language         | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------------------------------------+------------------+------------+------------+------------+------------+
| /Users/stephane/Documents/enseignements/BATEM/supports/src/.ipynb_checkpoints/notebook3_dynamic-checkpoint.ipynb         | JSON             |        585 |          0 |          1 |        586 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/__init__.py                                                   | Python           |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/airflow_model.ipynb                                           | JSON             |        132 |          0 |          1 |        133 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/apartment2zones.py                                            | Python           |         74 |          8 |         17 |         99 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/building_h358.py                                              | Python           |         73 |         13 |         21 |        107 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingdesigner.ipynb                                        | JSON             |      1,626 |          0 |          1 |      1,627 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/__init__.py                                    | Python           |          1 |          4 |          1 |          6 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/building.py                                    | Python           |        583 |        412 |         96 |      1,091 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/data.py                                        | Python           |        304 |        229 |         54 |        587 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/dynprog.py                                     | Python           |        180 |        104 |         30 |        314 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/lambdahouse.py                                 | Python           |        658 |         64 |         74 |        796 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/linreg.py                                      | Python           |        299 |        157 |         29 |        485 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/model.py                                       | Python           |        196 |        167 |         33 |        396 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/model_old.py                                   | Python           |        278 |        323 |         52 |        653 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/openweather.py                                 | Python           |        162 |        143 |         27 |        332 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/parameters.py                                  | Python           |        122 |        154 |         24 |        300 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/physics.py                                     | Python           |        109 |        181 |         31 |        321 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/runner.py                                      | Python           |        277 |        148 |         65 |        490 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/solar.py                                       | Python           |        506 |        407 |         94 |      1,007 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/thermal.py                                     | Python           |        511 |        361 |         69 |        941 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/thermics.py                                    | Python           |        164 |        197 |         31 |        392 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/buildingenergy/timemg.py                                      | Python           |         32 |         75 |         23 |        130 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/coimbra.py                                                    | Python           |         42 |         11 |          6 |         59 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/briancon.json                                            | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/bucharest-unirii.json                                    | JSON             |          1 |          0 |          1 |          2 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/coimbra.json                                             | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/lambdahouse.md                                       | Markdown         |         38 |          0 |         37 |         75 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/encodings.xml             | XML              |          4 |          0 |          2 |          6 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/examples.iml              | XML              |          8 |          0 |          2 |         10 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/misc.xml                  | XML              |          9 |          0 |          2 |         11 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/modules.xml               | XML              |          8 |          0 |          2 |         10 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/scopes/scope_settings.xml | XML              |          5 |          0 |          0 |          5 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/vcs.xml                   | XML              |          6 |          0 |          2 |          8 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/.idea/workspace.xml             | XML              |        777 |          0 |          2 |        779 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example1.py                     | Python           |         16 |          0 |          3 |         19 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example2.py                     | Python           |         34 |          0 |          5 |         39 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example3.py                     | Python           |        131 |          2 |         26 |        159 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example4.py                     | Python           |          8 |          0 |          4 |         12 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example5.py                     | Python           |        169 |          1 |         29 |        199 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example6.py                     | Python           |         17 |          0 |          1 |         18 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example7.py                     | Python           |          6 |          0 |          2 |          8 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example8.py                     | Python           |        166 |          1 |         29 |        196 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/doc/notebook6-estimation/example9.py                     | Python           |         29 |          7 |          6 |         42 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/engins.json                                              | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/grenoble1979-2022.json                                   | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/grenoble_weather2015-2019.json                           | JSON             |          1 |          0 |          1 |          2 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/refuge-des-bans.json                                     | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/tirana.json                                              | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/villard-de-Lans_autrans_2000-2022.json                   | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data/viriville.json                                           | JSON             |          1 |          0 |          0 |          1 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/data_h358.py                                                  | Python           |         76 |          9 |         37 |        122 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_StMartinVinoux.ipynb                                   | JSON             |        373 |          0 |          1 |        374 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_VillardDeLansAutrans.ipynb                             | JSON             |        346 |          0 |          1 |        347 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_bans.ipynb                                             | JSON             |        377 |          0 |          1 |        378 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_briancon.ipynb                                         | JSON             |        345 |          0 |          1 |        346 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_bucharest.ipynb                                        | JSON             |        368 |          0 |          1 |        369 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_coimbra.ipynb                                          | JSON             |        345 |          0 |          1 |        346 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_engins.ipynb                                           | JSON             |        347 |          0 |          1 |        348 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/lambda_tirana.ipynb                                           | JSON             |        368 |          0 |          1 |        369 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/modelRC.ipynb                                                 | JSON             |        626 |          0 |          1 |        627 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/model_h358.py                                                 | Python           |         95 |         33 |         23 |        151 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook1_context.ipynb                                       | JSON             |      1,513 |          0 |          1 |      1,514 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook2_static.ipynb                                        | JSON             |         77 |        683 |          0 |        760 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook3_dynamic.ipynb                                       | JSON             |        585 |          0 |          1 |        586 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook4_learning.ipynb                                      | JSON             |      1,209 |          0 |          1 |      1,210 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook5_management.ipynb                                    | JSON             |        512 |          0 |          1 |        513 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/notebook_teet.ipynb                                           | JSON             |      1,556 |          0 |          1 |      1,557 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358dynprog_old.py                                        | Python           |         28 |         22 |          6 |         56 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358linreg_old.py                                         | Python           |         51 |          8 |         21 |         80 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358measurements_old.py                                   | Python           |         37 |         19 |          9 |         65 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358model_old.py                                          | Python           |        276 |         86 |         35 |        397 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358parameters_old.py                                     | Python           |        147 |         87 |         18 |        252 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358simulator_old.py                                      | Python           |        129 |        106 |         22 |        257 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/old/h358sliding_old.py                                        | Python           |         46 |          4 |         15 |         65 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/requirements.txt                                              | pip requirements |         54 |          0 |          1 |         55 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/runner_h358.py                                                | Python           |         57 |         11 |         20 |         88 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/setup.ini                                                     | Ini              |          7 |          0 |          0 |          7 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/solar_building.ipynb                                          | JSON             |        633 |          0 |          1 |        634 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/solar_h358.py                                                 | Python           |         38 |          2 |          5 |         45 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/solr_pvlib.py                                                 | Python           |        437 |        212 |        167 |        816 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/thermal_building_h358.py                                      | Python           |         75 |          7 |         19 |        101 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/tirana.py                                                     | Python           |         40 |         14 |          6 |         60 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/vandelay-js.js                                                | JavaScript       |          6 |          6 |          3 |         15 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/vandelay-py.js                                                | JavaScript       |          6 |          6 |          3 |         15 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/weather_bans.py                                               | Python           |         24 |          4 |          1 |         29 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/weather_engins.py                                             | Python           |         21 |          0 |          2 |         23 |
| /Users/stephane/Documents/enseignements/BATEM/supports/src/weather_h358.py                                               | Python           |         12 |         13 |          1 |         26 |
| Total                                                                                                                    |                  |     19,598 |      4,501 |      1,365 |     25,464 |
+--------------------------------------------------------------------------------------------------------------------------+------------------+------------+------------+------------+------------+