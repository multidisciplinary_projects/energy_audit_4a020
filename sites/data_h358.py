"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

"""

from typing import Dict, List, Tuple, TypeVar

from buildingenergy.openweather import SiteWeatherData
from buildingenergy.solar import System, RectangularMask
from buildingenergy.data import Data, ParameterizedData, SetpointGenerator
from buildingenergy.parameters import ParameterSet


class H358Data(Data):

    def __init__(self, starting_stringdate: str=None, ending_stringdate: str=None):  # type: ignore
        super().__init__(csv_measurement_filename='h358data_2015-2016.csv', json_openweather_filename='grenoble_weather2015-2019.json', starting_stringdate=starting_stringdate, ending_stringdate=ending_stringdate, sea_level_in_meter=290, albedo=.1,
                         pollution=0.1, location='Grenoble', deleted_variables=('Tyanis', 'zetaW7', 'zetaW9', 'wind_speed', 'wind_direction_in_deg', 'feels_like', 'occupancy', 'humidity', 'pressure', 'temp_min', 'temp_max', 'description', 'power_heater'))
        
        # create constant data (no measurement available)
        self.add_constant('Tdownstairs', 19)
        self.add_constant('outdoorCCO2', 395)

        # creation of the solar mask
        solar_mask = RectangularMask((-86, 60), (20, 68))
        solar_system = System(self.weather_data)
        solar_system.add_collector('main', surface=2, exposure_in_deg=-13, slope_in_deg=90, solar_factor=0.85, window_mask=solar_mask)
        solar_gains_with_mask, _ = solar_system.solar_gain
        self.add_external_variable('Psun_window', solar_gains_with_mask)

        # build invariant variables
        detected_motions = [int(d > 1) for d in self('detected_motions')]
        occupancy = [max(detected_motions[k], int(self('power_stephane', k) > 17) + int(self('power_khadija', k) > 17) +
                         int(self('power_stagiaire', k) > 17) + int(self('power_audrey', k) > 17)) for k in range(self.number_of_hours)]
        presence = [int(occupancy[k] > 0) for k in range(self.number_of_hours)]
        self.add_external_variable('occupancy', occupancy)
        self.add_external_variable('presence', presence)

        # create a series of setpoint values (useful or not, depending on a control temperature is declared or not)
        setpoint_generator = SetpointGenerator(reference_setpoint=21, datetimes=self('datetime'))
        setpoint_generator.seasonal('15/03', '15/10')
        setpoint_generator.daily([0, 1, 2, 3, 4], 17, [0, 6, 12, 14, 18])
        setpoint_generator.daily([0, 1, 2, 3, 4], 13, [0, 5, 22])
        setpoint_generator.daily([5, 6], 13, [0, 24])
        setpoint_generator.long_absence(9, 5, self('presence'))
        setpoint_generator.opening(5, .7, self('window_opening'))
        self.add_external_variable('Tsetpoint', setpoint_generator.values)


class Pmetabolim(ParameterizedData):
 
    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'Pmetabolism', ['body_metabolism'], ['occupancy'])      

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return data['occupancy'] * param['body_metabolism']


class Pheater(ParameterizedData):

    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'Pheater', ['heater_power_per_delta_surface_temperature'], ['dT_heat'])    

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return param['heater_power_per_delta_surface_temperature'] * data['dT_heat']
    

class Pheat(ParameterizedData):

    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'Pheat', ['heater_power_per_delta_surface_temperature', 'body_metabolism'], ['dT_heat', 'total_electric_power', 'solar_factor', 'Psun_window', 'occupancy']) 

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return param['heater_power_per_delta_surface_temperature'] * data['dT_heat'] + data['total_electric_power'] + data['occupancy'] * param['body_metabolism'] + param('solar_factor') * data['Psun_window']


class CO2production(ParameterizedData):

    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'CO2production', ['CO2_occupant_breath_production'], ['occupancy'])

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return param['CO2_occupant_breath_production'] * data['occupancy']


class Qoutdoor(ParameterizedData):

    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'Qoutdoor', ['Qoutdoor0', 'Qoutdoor_window', 'Qoutdoor_door'], ['window_opening', 'door_opening'])

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return param['Qoutdoor0'] + param['Qoutdoor_window'] * data['window_opening'] + param['Qoutdoor_door'] * data['door_opening']


class Qcorridor(ParameterizedData):

    def __init__(self, data: Data, param: ParameterSet):
        super().__init__(data, param, 'Qcorridor', ['Qcorridor0', 'Qcorridor_window', 'Qcorridor_door'], ['window_opening', 'door_opening'])

    def formula(self, param: Dict[str, float], data: Dict[str, float]) -> float:
        return param['Qcorridor0'] + param['Qcorridor_window'] * data['window_opening'] + param['Qcorridor_door'] * data['door_opening']
    

if __name__ == '__main__':
    data = H358Data(None, None)  # starting_stringdate = '20/02/2015', ending_stringdate = '1/03/2015')
    param = ParameterSet()
    param.add_invariant('body_metabolism', 50, 120)
    param.add_invariant('heater_power_per_delta_surface_temperature', 70, 50, 200)
    param.add_invariant('CO2_occupant_breath_production', 7, 5, 12)
    param.add_time_varying('Qoutdoor', 12/3600, 2/3600, 200/3600)
    param.add_invariant('Qoutdoor_window', 50/3600, 2/3600, 2000/3600)
    param.add_invariant('Qoutdoor_door', 50/3600, 2/3600, 2000/3600)
    Qoutdoor(data, param)
    CO2production(data, param)
    Pheat(data, param)
    Pheater(data, param)
    Pmetabolim(data, param)

    data.save('h358data.csv')
    data.plot()
