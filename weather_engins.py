import matplotlib.pylab
import buildingenergy.openweather
import buildingenergy.solar

site_weather_data = buildingenergy.openweather.OpenWeatherMapJsonReader('data/engins.json', from_stringdate = None, to_stringdate = None, sea_level_in_meter=900, albedo=.1).site_weather_data
print('weather variables:', site_weather_data.variable_names)  #  from_stringdate = '01/01/2019 0:00:00', to_stringdate = '01/01/2020 0:00:00'
solar_model = buildingenergy.solar.SolarModel(site_weather_data=site_weather_data)
solar_model.plot_heliodor(2015, 'heliodon')
solar_model.plot_solar_cardinal_irradiations()
matplotlib.pylab.figure()
phis1 = solar_model.solar_irradiations(slope_in_deg=90+35, exposure_in_deg=0)
print('energy PV:', sum(phis1['total'])*150*.13/1000,'kWh')
phis2 = solar_model.solar_irradiations(slope_in_deg=90+35, exposure_in_deg=-20)
print('energy PV:', sum(phis2['total'])*150*.13/1000, 'kWh')
matplotlib.pylab.plot(solar_model._site_weather_data.get('datetime'), phis2['total'])
matplotlib.pylab.plot(solar_model._site_weather_data.get('datetime'), phis1['total'])
matplotlib.pylab.legend(('0','-20'))
matplotlib.pyplot.figure()
matplotlib.pylab.plot(solar_model._site_weather_data.get('datetime'), solar_model._site_weather_data.get('temperature'))
solar_model.plot_angles()
solar_model.try_export()
matplotlib.pylab.show()
