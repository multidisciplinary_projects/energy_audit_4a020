from builtins import float, str, int

import math
from matplotlib.pylab import *
from math import pi, cos, sin, acos, asin, atan, sqrt, exp, log
from pytz import timezone, utc
from timemg import *
from typing import TypeVar
import pandas as pd, numpy as np
from datetime import timedelta ,time as tt
import pvlib
import consumption
import plotly.express as px

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler,PolynomialFeatures
from sklearn.pipeline import make_pipeline

from sklearn.ensemble import RandomForestRegressor
from urllib3 import PoolManager
from json import loads
import pickle
from pygam import LinearGAM, s, f
import lightgbm as lgbm
import  logging
from send_data_TB import Connector

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor

from pandas.plotting import register_matplotlib_converters
datatime = TypeVar('datetime')

# SOUTH = pi
# EAST = - pi / 2
# WEST = pi / 2
# NORTH = 0
#
# HORIZONTAL = 0  # solar captor directed horizontally to the sky zenith
# VERTICAL = pi / 2  # solar captor directed vertically


# logging.basicConfig(filename='PV_logger.log', encoding='utf-8', level=logging.)



def sign(x: float):
    return 1 if x > 0 else -1 if x < 0 else 0

class SolarGain():

    def __init__(self, time_zone: str='Europe/Paris', latitude_in_deg: float=45.183, longitude_in_deg: float=5.717, sea_level_in_meters: float=330, albedo: float=0.1):
        """
        compute solar gains at a given location (Grenoble by default)
        :param time_zone: time zone
        :param latitude_in_deg: latitude in degrees
        :param longitude_in_deg: longitude in degrees
        :param sea_level_in_meters: sea level in meters
        :param albedo: albedo
        """
        self.time_zone = time_zone
        self.latitude_in_deg = latitude_in_deg
        self.longitude_in_deg = longitude_in_deg
        self.sea_level_in_meters = sea_level_in_meters
        self.albedo = albedo

    def get_solar_time(self, datetime: datetime):
        """
        convert administrative time to solar time
        :param datetime: datetime to be converted
        :return: day_in_year, solartime_in_secondes
        """
        thetimezone = timezone(self.time_zone)
        # local_datetime = thetimezone.localize(datetime, is_dst=True)
        try:
            local_datetime = thetimezone.localize(datetime, is_dst=True)
        except ValueError:
            local_datetime = datetime
            # pass
        utc_datetime = local_datetime.astimezone(utc)
        utc_timetuple = utc_datetime.timetuple()
        day_in_year = utc_timetuple.tm_yday
        hour_in_day = utc_timetuple.tm_hour
        minute_in_hour = utc_timetuple.tm_min
        seconde_in_minute = utc_timetuple.tm_sec
        standard_time_in_seconds = hour_in_day * 3600 + minute_in_hour * 60 + seconde_in_minute + self.longitude_in_deg * 4 * 60
        coefsin1 = 0.001868
        coefcos1 = 0.032077
        angle1 = atan(coefsin1 / coefcos1)
        deltaday1 = angle1 / (2 * pi) * 365
        coef1 = sqrt(coefcos1 ** 2 + coefsin1 ** 2)
        coefsin2 = 0.014615
        coefcos2 = 0.04089
        angle2 = atan(coefsin2 / coefcos2)
        coef2 = sqrt(coefcos2 ** 2 + coefsin2 ** 2)
        deltaday2 = angle2 / (4 * pi) * 365
        solartime_in_secondes = standard_time_in_seconds + 229.2 * 60 * (0.000075 + coef1 * sin(2 * pi * (deltaday1 - day_in_year) / 365) - coef2 * sin(4 * pi * (deltaday2 + day_in_year) / 365))
        return day_in_year, solartime_in_secondes

    def get_solar_angles(self, day_in_year: int, solartime_in_secondes: int):
        """
        calculate angles
        :param day_in_year: day of year between 0 and 265
        :param solartime_in_secondes: solar time in seconds
        :return: altitude_in_rad, azimuth_in_rad, hour_angle_in_rad, latitude_in_rad, declination_in_rad
        """
        latitude_in_rad = self.latitude_in_deg / 180 * pi
        declination_in_rad = 23.45 * pi / 180 * sin(2 * pi * (285 + day_in_year) / 365)
        hour_angle_in_rad = pi / 12 * (solartime_in_secondes / 3600 - 12)
        altitude_in_rad = asin(sin(declination_in_rad) * sin(latitude_in_rad) + cos(declination_in_rad) * cos(latitude_in_rad) * cos(hour_angle_in_rad))
        if 0 < altitude_in_rad < pi/2 :
            azimuth_in_rad = sign(hour_angle_in_rad) * abs(acos((sin(altitude_in_rad) * sin(latitude_in_rad) - sin(declination_in_rad)) / (cos(altitude_in_rad) * cos(latitude_in_rad))))
        else:
            altitude_in_rad = 0
            azimuth_in_rad = 0
        return altitude_in_rad, azimuth_in_rad, hour_angle_in_rad, latitude_in_rad, declination_in_rad

    def get_solar_beam_transfer(self, phis_with_nebulosity: float, altitude_in_rad: float, temperature: float, humidity: float, pollution: float):
        atmospheric_pressure = 101325 * (1 - 2.26e-5 * self.sea_level_in_meters) ** 5.26
        transmitivity = 0.6 ** ((sqrt(1229 + (614 * sin(altitude_in_rad)) ** 2) - 614 * sin(altitude_in_rad)) * ((288 - 0.0065 * self.sea_level_in_meters) / 288) ** 5.256)
        air_mass = atmospheric_pressure / (101325 * sin(altitude_in_rad) + 15198.75 * (3.885 + altitude_in_rad) ** (-1.253))
        Erayleigh = 1 / (0.9 * air_mass + 9.4)
        Pv = 2.165 * (1.098 + temperature / 100) ** 8.02 * humidity
        lLinke = 2.4 + 14.6 * pollution + 0.4 * (1 + 2 * pollution) * log(Pv)
        phi_direct_atmosphere = transmitivity * phis_with_nebulosity * exp(-air_mass * Erayleigh * lLinke)
        return phi_direct_atmosphere, transmitivity

    def get_solar_gain(self, exposure_in_rad: float, slope_in_rad: float, datetime: datetime, temperature: float=15.4, humidity: float=.2, nebulosity_in_percentage: float=0, pollution: float=0.1):
        """
        compute the solar power on a 1m2 flat surface
        :param exposure_in_rad: angle of the surface with the north. O means north oriented, -pi/2 means West, pi/2 East and pi South oriented
        :param slope_in_rad: angle in radiants of the flat surface. 0 means horizontal directed to the sky zenith and pi/2 means vertical
        :param datetime: hour in the day
        :param temperature: outdoor temperature
        :param humidity: outdoor humidity
        :param nebulosity_in_percentage: cloudiness ie percentage of the sky covered by clouds
        :param pollution: pollution rate
        :return: phi_total, phi_direct_collected, phi_diffuse, phi_reflected
        """
        day_in_year, solartime_in_secondes = self.get_solar_time(datetime)
        altitude_in_rad, azimuth_in_rad, solarangle_in_rad, latitude_in_rad, declination_in_rad = self.get_solar_angles(day_in_year, solartime_in_secondes)
        phis = 1367 * (1 + 0.033 * cos(2 * pi * day_in_year / 365))
        phis_with_nebulosity = (1 - 0.75 * nebulosity_in_percentage ** 3.4) * phis
        phi_direct_atmosphere, transmitivity = self.get_solar_beam_transfer(phis_with_nebulosity, altitude_in_rad, temperature, humidity, pollution)
        incidence_in_rad = acos(cos(altitude_in_rad) * sin(slope_in_rad) * cos(azimuth_in_rad + exposure_in_rad) - sin(altitude_in_rad) * cos(slope_in_rad))
        if altitude_in_rad == 0 or (slope_in_rad !=0 and exposure_in_rad == 0):
           phi_direct_collected = 0
        else:
            phi_direct_collected = incidence_in_rad * phi_direct_atmosphere

        phi_diffuse = phis_with_nebulosity * (0.271 - 0.294 * transmitivity) * sin(altitude_in_rad)
        phi_reflected = self.albedo * phis_with_nebulosity * (0.271 + 0.706 * transmitivity) * sin(altitude_in_rad) * cos(slope_in_rad / 2) ** 2
        return phi_direct_collected + phi_diffuse + phi_reflected, phi_direct_collected, phi_diffuse, phi_reflected


def get_weather_forecast(time="1h", start_time=datetime.datetime.today(), fields=["temperature"]):
    #todo move to toolbox
    """time  = "1h , 5m, 1d"""
    lat = 45.19144
    lon = 5.714258
    http = PoolManager()

    df_main = []
    KEY_TOMORROW_IO = "bULGDf4QTXA7pO6QC0CpLRzAbFzPzZYs"  # key for emailID : muhammad-salman.shahid@g2elab.grenoble-inp.fr
    for field in fields:

        forecast_url = "https://api.tomorrow.io/v4/timelines?fields=" + field + "&units=metric&timesteps=" + time + "&apikey=" + KEY_TOMORROW_IO + "&location=" + str(
            lat) + "," + str(lon)

        try:
            print ("Sending URL request :  " , forecast_url)
            response = http.request('GET', forecast_url)
            print ("###################################")
            print (response.data)
            data = loads(response.data)

        except ConnectionError as ce:
            print(ce.strerror)

        if 'message' in data:
            print(f"ERROR MESSAGE ==> {data['message']}")
            exit(1)

        if 'data' in data:

            df = pd.json_normalize(data['data']['timelines'][0]['intervals'])

            df.columns = ["time", field]

            df.time = pd.to_datetime(df.time)

            if time == '1d':
                df.time = df.time.map(lambda t: t.replace(hour=0, minute=0, second=0, microsecond=0))

            df.set_index('time', inplace=True)

            df = df.apply(pd.to_numeric, downcast='float')

            if len(df_main) == 0:
                df_main = df
            else:
                df_main = pd.concat([df_main, df], axis = 1)

    return df_main, lat, lon


def weather_data (prediction = True):
    if prediction:
        df_variables, lattitude_,longitude_ = get_weather_forecast(fields=["temperature", "rainAccumulation","humidity",
                                                                           "windSpeed","cloudCover"])
        df_variables = df_variables.rename(columns = {"cloudCover" : "cloudiness", "windSpeed" : "wind_speed" ,
                                                      "rainAccumulation" : "rain" })
        df_variables =  df_variables.reset_index()
        df_variables.rename(columns={"time" : "datetime"}, inplace=True)
        # print ( "YEST" , df_variables)
        #
        # df_variables.to_csv("data/temp.csv",decimal='.',sep=',')
        # df_variables = pd.read_csv("data/temp.csv",decimal='.',sep=',')
        # df_variables["datetime"] = pd.to_datetime(df_variables["datetime"],dayfirst= False,utc=True)
        start = str(df_variables["datetime"].iloc[0]).split("+")[0]
        df_variables["datetime"] = pd.date_range(start = start,freq="H",
                                           periods = len(df_variables), tz= "UTC").tz_convert('Europe/Paris')

        print(df_variables)
        # df_variables.index = df_variables["datetime"]
        # df_variables.index = df_variables.index.tz_localize("UTC").tz_convert('Europe/Paris',)
        # df_variables["datetime"] = df_variables.index
        # print(df_variables)
        lattitude_ , longitude_ =45.183 , 5.717

        df_variables = generate_irradiation_data(df=df_variables.copy(), latitude=lattitude_, longitude=longitude_)
        df_variables = gen_sloar_pow(df = df_variables , lattitude=lattitude_ , longitude = longitude_)

    else:

        df_variables = pd.read_json("data/grenoble_weather2015-2019.json")
        df_variables = df_variables.fillna(0)

        lattitude_ = df_variables["lat"][0]
        # print (df_variables["lon"][0])
        longitude_ = df_variables["lon"][0]
        from_stringdate = "2015-12-31 23:00:00"
        end_stringdate = "2020-02-17 23:00:00"

        start = str(df_variables["datetime"].iloc[0]).split("+")[0]
        df_variables["datetime"] = pd.date_range(start=start, freq="H",
                                                 periods=len(df_variables), tz="UTC").tz_convert('Europe/Paris')

        # df_variables["datetime"] = pd.to_datetime(df_variables[['dt',"timezone"]].sum(axis = 1), unit='s')#.dt.tz_localize("Europe/paris")
        # print (df_variables.columns)
        # print(df_variables["timezone"].tolist())

        df_variables['dt'] *= 10000
        # df_variables["datetime"] = df_variables["datetime"].tz_convert("Europe/paris")
        df_variables = df_variables[df_variables.datetime > from_stringdate]
        df_variables = df_variables[df_variables.datetime <= end_stringdate]
        # print (df_variables["rain"].tolist())
        df_variables["temperature"] = [val["temp"] if type(val)== dict else val for val in df_variables["main"]]
        df_variables["temp_min"] = [val["temp_min"] if type(val) == dict else val for val in df_variables["main"]]
        df_variables["temp_max"] = [val["temp_max"] if type(val) == dict else val for val in df_variables["main"]]

        df_variables["humidity"] = [val["humidity"] if type(val) == dict else val for val in df_variables["main"]]
        df_variables["pressure"] = [val["pressure"] if type(val) == dict else val for val in df_variables["main"]]

        df_variables["wind_direction"] = [val["deg"] if type(val) == dict else val for val in df_variables["wind"]]
        df_variables["wind_speed"] = [val["speed"] if type(val) == dict else val for val in df_variables["wind"]]

        df_variables["cloudiness"] = [val["all"] if type(val) == dict else val for val in df_variables["clouds"]]
        df_variables["wind_speed"] = [val["speed"] if type(val) == dict else val for val in df_variables["wind"]]


        df_variables = df_variables.rename(columns = {"dt": "epochtimems","dt_iso": "stringdate"})

        df_variables = df_variables.drop(columns=["lat","lon",'city_name',"main" , "rain","timezone","wind","clouds","weather"])
    return df_variables, lattitude_,longitude_

def generate_irradiation_data(df,longitude , latitude):
    solar_captor_slope_horizontal = 0  # horizontale directed to the sky zenith

    solar_gain = SolarGain(time_zone='Europe/Paris', latitude_in_deg=latitude, longitude_in_deg=longitude,
                                  sea_level_in_meters=330, albedo=0.1)
    SOUTH = pi

    directions = {SOUTH: ('Grenoble_slope%idirSOUTH.png')}
    names, results, results_all = [], [], []
    variables = {}
    for solar_captor_slope in [solar_captor_slope_horizontal]:
        for solar_captor_direction in directions:
            print(directions[solar_captor_direction] % round(solar_captor_slope * 180 / math.pi))
            variables['datetime'] = []
            variables['solar_power_total'] = []
            variables['solar_power_direct'] = []
            variables['solar_power_diffuse'] = []
            variables['solar_power_reflected'] = []  # altitude_in_rad, azimuth_in_rad, hour_angle_in_rad, latitude_in_rad, declination_in_rad
            variables['altitude_in_rad'] = []
            variables['azimuth_in_rad'] = []
            variables['hour_angle_in_rad'] = []
            variables['latitude_in_rad'] = []
            variables['declination_in_rad'] = []

            for k in range(len(df['datetime'])):
                # current_datetime = stringdate_to_datetime(df['stringdate'].iloc[k])
                current_datetime = df['datetime'].iloc[k]
                variables['datetime'].append(current_datetime)
                day_in_year, solartime_in_secondes = solar_gain.get_solar_time(current_datetime)
                angles = solar_gain.get_solar_angles(day_in_year, solartime_in_secondes)
                variables['altitude_in_rad'].append(angles[0])
                variables['azimuth_in_rad'].append(angles[1])
                variables['hour_angle_in_rad'].append(angles[2])
                variables['latitude_in_rad'].append(angles[3])
                variables['declination_in_rad'].append(angles[4])
                phi_total, phi_direct_collected, phi_diffuse, phi_reflected = solar_gain.get_solar_gain(
                    solar_captor_direction, solar_captor_slope,
                    df['datetime'].iloc[k],temperature=df['temperature'].iloc[k], humidity=df['humidity'].iloc[k],
                    nebulosity_in_percentage=df['cloudiness'].iloc[k] / 100, pollution=0.1)
                variables['solar_power_total'].append(phi_total)
                variables['solar_power_direct'].append(phi_direct_collected)
                variables['solar_power_diffuse'].append(phi_diffuse)
                variables['solar_power_reflected'].append(phi_reflected)

    #         if directions[solar_captor_direction] % round(
    #                 solar_captor_slope * 180 / math.pi) == "saint-bonnet_slope90dirSOUTH.png":
    #             print("success", directions[solar_captor_direction] % round(solar_captor_slope * 180 / math.pi))
    #             results.append(variables)
    #         results_all.append([variables['solar_power_total'], variables['solar_power_diffuse']])
    #         names.append(directions[solar_captor_direction] % round(solar_captor_slope * 180 / math.pi))
    #         register_matplotlib_converters()
    #         fig, ax = subplots()
    #         ax.plot(variables['datetime'], variables['solar_power_total'])
    #         ax.plot(variables['datetime'], variables['solar_power_direct'])
    #         ax.plot(variables['datetime'], variables['solar_power_diffuse'])
    #         ax.plot(variables['datetime'], variables['solar_power_reflected'])
    #         ax.legend(('total', 'direct', 'diffuse', 'reflected'))
    #         ax.axis('tight')
    #         ax.grid()
    #         ax.set_title(directions[solar_captor_direction] % round(solar_captor_slope * 180 / math.pi))
    #         #             results.append(variables)
    # show()

    df["solar_power_total"] = variables["solar_power_total"]
    df["solar_power_direct"] = variables["solar_power_direct"]
    df["solar_power_diffuse"] = variables["solar_power_diffuse"]
    df["solar_power_reflected"] = variables["solar_power_reflected"]
    # df["solar_power_total"] = variables["solar_power_total"]
    # df["solar_power_total"] = variables["solar_power_total"]
    return df

def gen_sloar_pow(df, lattitude, longitude):



    sandia_modules = pvlib.pvsystem.retrieve_sam('SandiaMod')


    sapm_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')
    sapm_inverters = pvlib.pvsystem.retrieve_sam('cecinverter')

    module = sandia_modules['Schott_Solar_ASE_300_DGF_50__320___2007__E__']



    temperature_model_parameters = pvlib.temperature.TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_glass']

    system = {'module': module, 'surface_azimuth': 230}
    # start = datetime_to_stringdate(a_datetime = df.index[0], date_format ='%d/%m/%Y %H:%M:%S')
    # stop = datetime_to_stringdate(a_datetime = df.index[-1], date_format = '%d/%m/%Y %H:%M:%S')

    # dates = pd.date_range(start=start, end=stop, freq='1h')


    df_ = pd.DataFrame()
    df_["dates"] = df["datetime"].tolist()
    df_ = df_.set_index('dates')


    temp_air = df["temperature"]
    wind_speed = df["wind_speed"]
    print ("Fail :  ", df.columns)

    # print (dates[0], dates[-1])
    # print(len(total_irr) , len(df))
    df_["total_irrad"] = df['solar_power_total']
    df_["difuse_irrad"] = df['solar_power_diffuse']
    df_["amb_temp"] = df["temperature"]
    df_["wind_speed"] =  df["wind_speed"]
    # df_["snow"] =  df["snow"]
    df_["cloudiness"] = df["cloudiness"]
    df_["temperature"] = df["temperature"]
    #df_["cloudiness"] = df["cloudiness"]


    dates_ = df_.index
    # print(dates_[0], dates_[-1])

    # city
    # print (lattitude_)
    surface_tilt = 0
    altitude = 330  # m

    solpos = pvlib.solarposition.get_solarposition(dates_, lattitude, longitude)

    dni_extra = pvlib.irradiance.get_extra_radiation(dates_)

    # print ("len", len(dni_extra))
    airmass = pvlib.atmosphere.get_relative_airmass(solpos['apparent_zenith'])

    pressure = pvlib.atmosphere.alt2pres(altitude)

    am_abs = pvlib.atmosphere.get_absolute_airmass(airmass, pressure)
    df_["am_abs"] = am_abs
    tl = pvlib.clearsky.lookup_linke_turbidity(dates_, lattitude, longitude)

    cs = pvlib.clearsky.ineichen(solpos['apparent_zenith'], df_["am_abs"], tl,  # altitude=altitude)
                                 dni_extra=dni_extra, altitude=altitude)

    aoi = pvlib.irradiance.aoi(surface_tilt, system['surface_azimuth'],
                               solpos['apparent_zenith'], solpos['azimuth'])

    total_irrad = pvlib.irradiance.get_total_irradiance(surface_tilt, system['surface_azimuth'],
                                                        solpos['apparent_zenith'],
                                                        solpos['azimuth'], cs['dni'], cs['ghi'], cs['dhi'],
                                                        dni_extra=dni_extra, model='haydavies')

    df_["total_irrad"] = total_irrad["poa_global"]
    df_["difuse_irrad"] = total_irrad["poa_diffuse"]
    df_["direct_irrad"] = total_irrad["poa_direct"]

    ###########SNOW
    # surface_tilt = 30
    # snow_cov = pvlib.snow.coverage_nrel(snowfall=df_["snow"], poa_irradiance=df_["total_irrad"],
    #                                     temp_air=df_["amb_temp"],
    #                                     surface_tilt=surface_tilt, initial_coverage=0, threshold_snowfall=0.5,
    #                                     can_slide_coefficient=-80.0, slide_amount_coefficient=0.197)
    #
    # snow_loss = pvlib.snow.dc_loss_nrel(snow_cov, 1)
    # df_["snow_loss"] = snow_loss

    ###########  END ########

    aoi = pvlib.irradiance.aoi(surface_tilt, system['surface_azimuth'], solpos['apparent_zenith'], solpos['azimuth'])
    df_["aoi"] = aoi

    tcell = [pvlib.temperature.sapm_cell(df_["total_irrad"].iloc[i], temp_air.iloc[i], wind_speed.iloc[i], **temperature_model_parameters) for i
             in range(len(df_["total_irrad"]))]

    df_["cell_temp"] = tcell
    effective_irradiance = pvlib.pvsystem.sapm_effective_irradiance(df_["direct_irrad"], df_["difuse_irrad"],
                                                                    df_["am_abs"], df_["aoi"], module)
    df_["effective_irradiance"] = effective_irradiance
    # effective_irradiance = effective_irradiance[:-1]
    # print ("len", len(effective_irradiance), len(df["cell_temp"]))

    correction = []
    for irr, cloud in zip(effective_irradiance, df["cloudiness"]):
        #     print ("irr : {} and cloud : {}".format(irr,cloud))
        cloud /= 100
        correction.append((1 - 0.51 * cloud ** 6.42) * irr)

    df_["corrected_irradiation"] = correction
    prod_sim = pvlib.pvsystem.sapm(df_["corrected_irradiation"], df_["cell_temp"], module)
    print (prod_sim)
    #df = df[~df.index.duplicated(keep='first')]
    df_["PV_prod_pvlib"] = (prod_sim["p_mp"]/1000) * 22*3.33335

    return df_

def get_actual_prod(start= '01/01/2017 00:00:00'):
    client = consumption.connectToDB("appli_greener")
    start = stringdate_to_datetime(start, "%d/%m/%Y %H:%M:%S")

    print(start)
    start = int(time.mktime((start).timetuple()) * 1000000000)
    actual = consumption.sendRequest(client= client, table='ve_sql', cols='pv_energy_cumul', start = start)

    actual = consumption.check_energy(actual)

    actual = actual.resample("60T").asfreq()
    # print("TEST 3", len(actual))
    actual = actual.interpolate(method="linear")

    actual.index = pd.to_datetime(actual.index).tz_localize(None)#.tz_convert("Europe/paris").tz_localize(None)

    actual = consumption.energy_to_power(actual)

    #actual.drop_duplicates(inplace=True)
    actual = actual[~actual.index.duplicated(keep='first')]

    print (actual)
    return actual



def reshape_data(df):
    x_data, y_data = [], []
    temp = df[["pv_energy_cumul"]]
    y_data = temp.to_numpy()
    y_data = y_data.reshape(y_data.shape[0] * y_data.shape[1])

    temp = df.drop(columns="pv_energy_cumul")
    x_data = temp.to_numpy()
    # x_data = x_data.reshape(x_data.shape[0] * x_data.shape[1])
    # start = df.index[0]
    #
    # l=0
    # for day in df.index.date:
    #     # print (df[df.index.date == day])
    #     temp = df[df.index.date == day]
    #     if len(temp)<24 or len(temp)>24:
    #         print (day)
    #     else:
    #         y = temp[["pv_energy_cumul"]]
    #         y = y.to_numpy()
    #         y = y.reshape(y.shape[0] * y.shape[1])
    #
    #         x = temp.drop(columns="pv_energy_cumul")
    #         x = x.to_numpy()
    #         x= x.reshape(x.shape[0]*x.shape[1])
    #
    #         x_data.append(x)
    #         y_data.append(y)
    #     l += 1
    #     if l == 50:
    #         break
    # print(x_data)
    # x_data, y_data = np.array(x_data), np.array(y_data)
    print(x_data.shape, y_data.shape)

    return x_data, y_data
    #     end = start +timedelta(hours=23)
    #     temp_df = df.loc[start:end]
    # for day in df.drop(columns= "pv_energy_cumul").groupby(by = df.index.date):
    #     print (type(day))
    #     #y = day[["pv_energy_cumul"]].to_numpy()
    #     x = day.to_numpy()
    #
    #     print (x.shape , y.shape)
def get_csv_data(path = "data/production_data.csv"):
    df = pd.read_csv(path, sep=',', decimal='.', index_col ="datetime",
                     parse_dates = ["datetime"])

    return df
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(25, input_dim=5, kernel_initializer='normal', activation='relu',))
	model.add(Dense(1, kernel_initializer='normal'))
    # model.add(Dense(1 ))

	model.compile(loss='mean_squared_error', optimizer='adam')
	return model
# evaluate model
def train_model(x_data, y_data):
    data_train , data_test,target_train,target_test = train_test_split(x_data,y_data,random_state=42,train_size = 0.75)

    # model = make_pipeline(StandardScaler(), MLPRegressor(max_iter = 5000 , solver = "lbfgs",activation="relu"))
    # model.fit(X=data_train , y = target_train)
    lam = np.logspace(-3, 5, 5)
    lams = [lam] * 5

    model  = LinearGAM()
    model.fit(x_data, y_data)
    # model.gridsearch(data_train, target_train, lam=lams)

    model_2 =  make_pipeline(StandardScaler(), RandomForestRegressor(n_estimators=200 , criterion = 'mae'))
    model_2.fit(X=x_data , y = y_data)

    #
    # model_3 = lgbm.LGBMRegressor(
    #     objective='regression',
    #     max_depth=5,
    #     num_leaves=5 ** 2 - 1,
    #     learning_rate=0.007,
    #     n_estimators=30000,
    #     min_child_samples=80,
    #     subsample=0.8,
    #     colsample_bytree=1,
    #     reg_alpha=0,
    #     reg_lambda=0,
    #     random_state=np.random.randint(10e6)
    # )

    lgb_train = lgbm.Dataset(x_data, y_data)
    lgb_eval = lgbm.Dataset(data_test, target_test, reference=lgb_train)

    params = {
            "boosting_type" : "rf",
            "objective" : 'regression',
            "metric" :  "l1",
            "max_depth" : 10,
            "num_leaves" : 2 ** 8 - 1,
            "learning_rate" : 0.007,
            "n_estimators" : 30000,
            "min_child_samples" : 80,
            "bagging_fraction": 0.8,
             "bagging_freq": 5,
            "verbose" : -1,
            "subsample" : 0.8,
            "colsample_bytree" : 1,
            "reg_alpha" : 0,
            "reg_lambda" : 0,
    #         "num_boost_round" : 10000,
            "random_state" : np.random.randint(10e6)}

    model_3 = lgbm.train(params , lgb_train, valid_sets=lgb_eval)

    # model_3.fit(data_train,target_train,eval_metric='l2',early_stopping_rounds=200, verbose=False)
    # model_3 = make_pipeline( PolynomialFeatures(degree=4), RandomForestRegressor(n_estimators=200, criterion='mae'))
    # model_3.fit(X=data_train, y=y_data)

    model_4 = KerasRegressor(build_fn=baseline_model, epochs=100, batch_size=5, verbose=0)
    model_4.fit(x = x_data, y = y_data)


    # model_3 = make_pipeline(StandardScaler() , SVC(kernel="poly"))


    prediction  = model.predict(data_test)
    pickle.dump(model, open("models/prediction_GAMS", 'wb'))

    prediction_2 = model_2.predict(data_test)
    pickle.dump(model_2, open("models/prediction_PV_R_Forest", 'wb'))

    prediction_3 = model_3.predict(data_test)
    pickle.dump(model_3, open("models/prediction_PV_LGBM", 'wb'))
    print (prediction_3)
    prediction_4 = model_4.predict(data_test)
    pickle.dump(model_4, open("models/prediction_PV_ANN_1", 'wb'))

    print ("hello " , prediction_4.flatten())

    df = pd.DataFrame({"Actual":target_test , "Predicted_GAMS":prediction,"Predicted_RF":prediction_2,"Predicted_LGBM":prediction_3
                       ,"Predicted_ANN":prediction_4.flatten()})
    fig = px.line(df)
    fig.show()
    return df

    fig = px.line(df)
    fig.show()
    # scaler= StandardScaler()
    # scaler.fit(data_train)
    #
    # data_train = scaler.transform(data_train)
    # target_train = scaler.transform(target_train)
    #
    # data_test = scaler.transform(data_test)
    #
def clean_data (data) :

    for i in range (len(data)):
        # print (data.iloc[i])
        if data.iloc[i]["pv_energy_cumul"] > 22 :
            try :
                data.iloc[i] =  (data.iloc[i-1] + data.iloc[i+1]) / 2
            except:
                if i==0:
                    data.iloc[i] = data.iloc[i + 1]
                elif i == len(data)-1:
                    data.iloc[i] = data.iloc[i-1]
                else:
                    raise Warning ("Check data")

    for day in data.resample("1D").asfreq().index:
        rep = day - timedelta(days = 1)
        day = day.strftime('%Y-%m-%d')

        if data.loc[day]["pv_energy_cumul"].sum() == 0 and len(data.loc[day]["pv_energy_cumul"]) >=23:
            rep = rep.strftime('%Y-%m-%d')
            try :
                data.loc[day]["pv_energy_cumul"] = data.loc[rep]["pv_energy_cumul"].tolist()
            except:
                print (day)
                print(len(data.loc[day]["pv_energy_cumul"]))
                print(len(data.loc[rep]["pv_energy_cumul"]))

                raise Exception


    return data




def predictor(x_data, dates):

    model = pickle.load(open(f"models/prediction_GAMS", 'rb'))
    model_2 = pickle.load(open(f"models/prediction_PV_R_Forest", 'rb'))
    # model_3 = pickle.load(open("models/prediction_PV_ANN_1", 'rb'))
    model_4 = pickle.load(open(f"models/prediction_PV_LGBM", 'rb'))

    gam = model.predict(x_data)
    r_for = model_2.predict(x_data)
    # ann = model_3.predict(x_data)
    lgbm = model_4.predict(x_data)

    return pd.DataFrame({"LGBM" : lgbm , "GAM" : gam , "Random_Forest" : r_for ,  }, index=dates)





def generate_csv():
    df, lattitude_, longitude_ = weather_data()

    print ("TEST : " , lattitude_ , longitude_)
    # print (df_variables)
    df = generate_irradiation_data(df=df.copy(), latitude=lattitude_, longitude=longitude_)
    df = gen_sloar_pow(df=df.copy(), longitude=longitude_, lattitude=lattitude_)

    # df.drop_duplicates(inplace =True)
    df = df[~df.index.duplicated(keep='first')]
    df.index = [ind + timedelta(hours=1) for ind in df.index]
    start = df.index[0]
    end = df.index[-1]
    dates_ = pd.date_range(start=start, end=end, freq="1H")
    # df.index = dates_

    # df = df.resample("60T").mean()
    actual = get_actual_prod(start='01/01/2016 01:00:00')
    actual = actual[actual.index <= df.index[-1]]
    # actual.index = dates_
    # print (actual.index)
    # print (df.index[-1])
    # end = df.index[-1]
    # actual
    df = pd.concat([df, actual], axis=1)
    df = df.astype(float)
    # df["actual"]  = actual["pv_energy_cumul"].tolist()
    df.fillna(0)
    print(df)
    df.to_csv("data/production_data.csv", sep=',', decimal='.',index_label="datetime")

def send_data(df):
    measurement = {}
    connector = Connector()
    for row in df.index:
        measurement["ts"] = row
        measurement["values"] = df.loc[row].to_dict()
        connector.T_B_updater(measurements=measurement, token = "v7eyNslYxkszxudUla6z",)
if __name__ == '__main__':
    train = False
    # df_variables, lattitude_,longitude_ = weather_data()
    # df_variables =

    # print (df_variables)
    # generate_csv()
    if train:
        df= get_csv_data()
        print(df.columns)
        df = df [['wind_speed','PV_prod_pvlib', 'pv_energy_cumul',"temperature",
                  "cloudiness"]]

        df = df.fillna(0)
        df["pv_energy_cumul"] = clean_data(df[["pv_energy_cumul"]])

        df['month'] = df.index.month
        print("here " , df.columns)
        x_data, y_data = reshape_data(df)
        print ("Shape" , x_data.shape)

        train_model(x_data=x_data, y_data=y_data)


    else:
        df , lat,lon = weather_data(prediction=True)
        # print (df)

        # df = generate_irradiation_data(df=df.copy(), latitude=lattitude_, longitude=longitude_)
        # df = gen_sloar_pow(df=df.copy(), longitude=longitude_, lattitude=lattitude_)


        df["pv_energy_cumul"] = [0]* len(df)

        df = df.fillna(0)
        df['month'] = df.index.month
        pv_lib = df["PV_prod_pvlib"]
        df = df[['wind_speed', 'PV_prod_pvlib', 'pv_energy_cumul', 'temperature',
       'cloudiness', 'month']]
        print (df.columns)
        x_data, y_data = reshape_data(df)

        predict = predictor (x_data , dates = df.index.tolist())
        predict["PV_lib"] = pv_lib
        start = datetime.datetime.combine(datetime.datetime.today().date(), tt())
        end = start + timedelta(hours = 24)

        predict = predict[predict.index < str(end)]

        send_data(predict)


        # temp = get_actual_prod(start=datetime_to_stringdate(start))["pv_energy_cumul"]
        # temp.index = pd.date_range(start = start , freq = "1h", periods = len(temp))
        # print("#################")
        # print (temp)
        # predict["measured"]  = temp


    #     print (predict)
    #     fig = px.line(predict)
    #     fig.show()
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    # fig = px.line(df)
    # fig.show()

    #actual = get_actual_prod(start= '01/01/2016 00:00:00')
