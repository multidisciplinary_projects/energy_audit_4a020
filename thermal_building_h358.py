from buildingenergy.thermal import ThermalNetwork, CAUSALITY
from buildingenergy.building import Library, Site, InterfaceType, LayeredInterface
import matplotlib.pyplot as plt

library = Library()
library.store('concrete', 'thermal', 269)
library.store('gypsum', 'thermal', 265)
library.store('wood_floor', 'thermal', 264)
library.store('tile', 'thermal', 236)
library.store('glass_wood', 'thermal', 261)
library.store('glass', 'thermal', 267)
library.store('plaster', 'thermal', 265)
library.store('foam', 'thermal', 260)
library.store('polystyrene', 'thermal', 145)
library.store('brick', 'thermal', 268)
library.store('wood', 'thermal', 277)
library.store('air', 'thermal', 259)
library.store('usual', 'thermal', 278)
Site.library = library

h358_office_site = Site('office', 'corridor', 'downstairs')


# corridor wall
door_surface = 80e-2 * 200e-2
door = h358_office_site.add_layered_interface('office', 'corridor', InterfaceType.DOOR, door_surface)
door.add_layer('wood', 5e-3)
door.add_layer('air', 15e-3)
door.add_layer('wood', 5e-3)

glass_surface = 100e-2 * 100e-2
glass: LayeredInterface = h358_office_site.add_layered_interface('office', 'corridor', InterfaceType.GLAZING, glass_surface)
glass.add_layer('glass', 4e-3)

internal_wall_thickness = 13e-3 + 34e-3 + 13e-3
cupboard_corridor_surface: float = (185e-2 + internal_wall_thickness + 34e-2 + 20e-3) * 2.5
corridor_wall_surface: float = (408e-2 + 406e-2 + internal_wall_thickness) * 2.5 - door_surface - glass_surface - cupboard_corridor_surface

cupboard: LayeredInterface = h358_office_site.add_layered_interface('office', 'corridor', InterfaceType.WALL, cupboard_corridor_surface)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('foam', 34e-3)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('air', 50e-2 - 20e-3)
cupboard.add_layer('wood', 20e-3)

plain_corridor_wall: LayeredInterface = h358_office_site.add_layered_interface('office', 'corridor', InterfaceType.WALL, corridor_wall_surface)
plain_corridor_wall.add_layer('plaster', 13e-3)
plain_corridor_wall.add_layer('foam', 34e-3)
plain_corridor_wall.add_layer('plaster', 13e-3)

# outdoor wall
west_glass_surface = 2 * 130e-2 * 52e-2 + 27e-2 * 52e-2 + 72e-2 * 52e-2
east_glass_surface = 36e-2 * 56e-2
windows_surface = west_glass_surface + east_glass_surface
nocavity_surface = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface
cavity_surface = 315e-2 * 2.5 - west_glass_surface

windows: LayeredInterface = h358_office_site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, windows_surface)
windows.add_layer('glass', 4e-3)
windows.add_layer('air', 12e-3)
windows.add_layer('glass', 4e-3)

plain_wall: LayeredInterface = h358_office_site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, nocavity_surface)
plain_wall.add_layer('concrete', 30e-2)

cavity_wall: LayeredInterface = h358_office_site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, cavity_surface)
cavity_wall.add_layer('concrete', 30e-2)
cavity_wall.add_layer('air', 34e-2)
cavity_wall.add_layer('wood', 20e-3)

# slab
slab_effective_thickness = 11.9e-2
slab_surface = (309e-2 + 20e-3 + 34e-2) * (406e-2 + internal_wall_thickness) + 408e-2 * (273e-2 - 60e-2) - 315e-2 * (34e-2 + 20e-3) - (185e-3 + internal_wall_thickness) * 50e-2
slab = h358_office_site.add_layered_interface('office', 'downstairs', InterfaceType.WALL, slab_surface)
slab.add_layer('concrete', slab_effective_thickness)
bridge = h358_office_site.add_component_interface('office', 'outdoor', InterfaceType.BRIDGE, 0.5 * 0.99, 685e-2)  # ThBAT booklet 5, 3.1.1.2, 22B)


net = ThermalNetwork()
net.T('Tcor', CAUSALITY.IN)
net.T('Tout', CAUSALITY.IN)
net.T('Tin', CAUSALITY.OUT)
net.T('Tslab')

net.R('Tcor','Tin', 'Rcor', value=h358_office_site.interface_thermal_resistance('corridor', 'office'))
net.C('Tslab', 'Ci', value=h358_office_site.first_facet_interface_thermal_capacitance('downstairs', 'office')[0])
net.R('Tslab', 'Tin', 'Ri', value=h358_office_site.interface_thermal_resistance('downstairs', 'office')/2)
net.HEAT('Tin', 'Pin')
net.R('Tin', 'Tout', 'Rout', value=h358_office_site.interface_thermal_resistance('office', 'outdoor'))

print(h358_office_site)
state_model = net.state_model()
print(state_model)
net.draw()
plt.show()

# A [[-9.730851245614724e-06]]
# B [[4.3509562457146814e-06, 5.379894999900045e-06]], [[1.318970797523004e-07]]
# C [[0.6578142355374829]]
# D [[0.15300154647352113, 0.18918421798899615]], [[0.004638165965773461]]
