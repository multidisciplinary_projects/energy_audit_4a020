Download the buildingenergy project and unzip it, or clone the project with:
    
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy.git 

Launch "Anaconda Powershell Prompt" application.

Go inside the buildingenergy folder:

cd <PATH_TO_buildingenergy>

pip install -r requirements.txt

python -m notebook

It will open the default Internet browser. If it is Internet Explorer or Safari, close it and copy the URL http://localhost... appearing in the Powershell and paste it to Google Chrome or Firefox

Then open the relevant notebook (extension .ipynb)

To run notebooks online, go to https://bit.ly/3QbtIPK
