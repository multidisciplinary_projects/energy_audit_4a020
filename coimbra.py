from __future__ import annotations
import buildingenergy.thermics
import buildingenergy.solar
import buildingenergy.openweather
import buildingenergy.data

# de Caroline Huguel
# J'ai modifié le code avec mes valeurs pour le bâtiment considéré. Il n'y a pas de portes mais seulement des portes vitrées a priori. Il y a des endroits où je ne savais pas comment adapter le code (lignes 10,12,23, et 53)

room_volume = 2160
co2_breath_production = 7 #à augmenter
body_metabolism = 100
laptop_power_threshold_for_occupancy = 17 #à modifier
concentrationCO2out = 395
heater_power_per_degree = 30


# outdoor wall
west_glass_surface = 20.3
east_glass_surface = 31
north_glass_surface = 17.7
south_glass_surface = 15.2
windows_surface = west_glass_surface + east_glass_surface + north_glass_surface + south_glass_surface
nocavity_surface = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface #je ne sais pas à quoi ça correspond
#cavity_surface = 315e-2 * 2.5 - west_glass_surface #idem
windows = buildingenergy.thermics.Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical')
windows.add_layer('glass', 4e-3)
windows.add_layer('air', 12e-3)
windows.add_layer('glass', 4e-3)
nocavity = buildingenergy.thermics.Composition(first_layer_indoor=False, last_layer_indoor=True, position='vertical')
nocavity.add_layer('brick', 40e-2)
nocavity.add_layer('foam', 15e-2)
nocavity.add_layer('plaster', 13e-3)
#cavity = buildingenergy.thermics.Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical')
#cavity.add_layer('concrete', 30e-2)
#cavity.add_layer('air', 34e-2)
#cavity.add_layer('wood', 20e-3)
external_wall = buildingenergy.thermics.Wall('outdoor')
external_wall.add_composition(windows, windows_surface)
external_wall.add_composition(nocavity, nocavity_surface)
#external_wall.add_composition(cavity, cavity_surface)
external_wall.add_bridge(0.5 * 0.99, 685e-2)  # ThBAT booklet 5, 3.1.1.2, 22B
external_wall.add_infiltration(0.0035)
external_wall.add_max_opening_air_flow(0.007)
print(external_wall, '\n')  # it gives the Ubat


# variables
site_weather_data = buildingenergy.openweather.OpenWeatherMapJsonReader('coimbra.json', from_stringdate='01/01/2019 0:00:00', to_stringdate='01/01/2020 0:00:00', sea_level_in_meter=110, albedo=.2).site_weather_data
#solar_mask = buildingenergy.solar.WindowMask((-86, 60), (20, 68))
building_solar_gain = buildingenergy.solar.System(site_weather_data)
building_solar_gain.add_collector('east', surface=31, exposure_in_deg=100, slope_in_deg=90, solar_factor=0.85, window_mask=None)
building_solar_gain.add_collector('north', surface=17.7, exposure_in_deg=170, slope_in_deg=90, solar_factor=0.85, window_mask=None)
building_solar_gain.add_collector('west', surface=20.3, exposure_in_deg=80, slope_in_deg=90, solar_factor=0.85, window_mask=None)
building_solar_gain.add_collector('south', surface=15.2, exposure_in_deg=-10, slope_in_deg=90, solar_factor=0.85, window_mask=None)
phi_sun, _ = building_solar_gain.solar_gain
Tout = building_solar_gain.site_weather_data.get('temperature')
stringdate_days, average_temperature_days, min_temperature_days, max_temperature_days, dju_days = site_weather_data.day_degrees(18)
building_solar_gain.generate_xls('coimbra', heat_temperature_reference=18,  cool_temperature_reference=26)  # it generates an MS Excel fil