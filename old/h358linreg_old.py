"""ARX model for H358 office.

Author: stephane.ploix@grenoble-inp.fr
"""
import os
import shutil
import sys
import buildingenergy.linreg
import old.h358measurements_old as h358measurements_old
import buildingenergy.data


def arx_estimation(output_variable_name, input_variable_names, training_data_container, validation_data_container, offset, minimum_input_delay, inputs_maximum_delays, ouput_maximum_delay):
    folder_name = 'linreg'
    if os.path.isdir(folder_name):
        shutil.rmtree(folder_name)
    os.mkdir(folder_name)
    original = sys.stdout
    sys.stdout = open(folder_name+"/results.md", 'w')

    print("# Linear Regression")
    print('* offset:', offset)
    print('* minimum_input_delay:', minimum_input_delay)
    print('* inputs_maximum_delays:', inputs_maximum_delays)
    print('* ouput_maximum_delay:', ouput_maximum_delay)

    print('## Training')

    linear_regression = buildingenergy.linreg.LinearRegression(input_labels=input_variable_names, output_label=output_variable_name, minimum_input_delay=minimum_input_delay, inputs_maximum_delays=inputs_maximum_delays, output_maximum_delay=ouput_maximum_delay, offset=offset)

    training_inputs = [training_data_container.data[data_label] for data_label in input_variable_names]
    training_output = training_data_container.data[output_variable_name]
    linear_regression.learn(training_inputs, training_output)

    print('## model')
    print(linear_regression)
    linear_regression.plot_zeros_poles(folder_name=folder_name)

    output_training = linear_regression.simulate(training_inputs, training_output)

    print('## impacts')
    print(linear_regression)
    linear_regression.error_analysis(training_inputs, training_output, output_training, maxlags=10, folder_name=folder_name)

    print('## Testing')

    testing_inputs = [validation_data_container.data[data_label] for data_label in input_variable_names]
    testing_output = validation_data_container.data[output_variable_name]
    output_estimated = linear_regression.simulate(testing_inputs, testing_output)
    validation_data_container.add_external_variable(output_variable_name + '_estimated', output_estimated)


    plot_saver_testing = buildingenergy.data.PlotSaver(validation_data_container)
    plot_saver_testing.time_plot(['%s'%output_variable_name,'%s_estimated'%output_variable_name], folder_name+'/output_testing')

    print('* Estimated %s at testing' % output_variable_name)
    print('![Testing %s](output_testing.png)' % output_variable_name)
    linear_regression.error_analysis(testing_inputs, testing_output, output_estimated, maxlags=10,  folder_name=folder_name)

    sys.stdout.close()
    sys.stdout = original
    return linear_regression

if __name__ == '__main__':

    training_data_container = h358measurements_old.DataContainer('h358data_winter2015-2016.csv', skiprows=0, nrows=24*60)
    validation_data_container = h358measurements_old.DataContainer('h358data_winter2015-2016.csv', skiprows=0, nrows=None)

    ######## tuning parameter zone #######
    offset = True
    minimum_input_delay = 0
    inputs_maximum_delays = (2, 0, 0, 0, 2, 1, 1)
    ouput_maximum_delay = 4
    ######################################

    arx_estimation('Toffice_reference', ('Tout', 'window_opening', 'door_opening', 'total_electric_power', 'phi_sun', 'dT_heat', 'occupancy'), training_data_container, validation_data_container, offset=True, minimum_input_delay=0, inputs_maximum_delays=(2, 0, 0, 0, 2, 1, 1), ouput_maximum_delay=4)

    # input_labels = ('corridor_CO2_concentration', 'window_opening', 'door_opening', 'occupancy')
    # output_label = 'office_CO2_concentration'
