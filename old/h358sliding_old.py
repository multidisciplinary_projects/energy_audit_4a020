from __future__ import annotations

import buildingenergy.linreg
import old.h358measurements_old as h358measurements_old
import buildingenergy.data
import os, sys, shutil

def arx_estimation(output_variable_name, input_variable_names, data_container, offset, minimum_input_delay, inputs_maximum_delays, ouput_maximum_delay, slice_size = 24, minimum_slices = 12, slice_memory = 24 * 7 ):
    folder_name='sliding'
    if os.path.isdir(folder_name):
        shutil.rmtree(folder_name)
    os.mkdir(folder_name)
    original = sys.stdout
    sys.stdout = open(folder_name+"/results.md", 'w')

    print("# Linear Regression")
    print('* offset:', offset)
    print('* minimum_input_delay:', minimum_input_delay)
    print('* inputs_maximum_delays:', inputs_maximum_delays)
    print('* ouput_maximum_delay:', ouput_maximum_delay)

    print('## Training')

    print("# sliding window")

    print('* offset:', offset)
    print('* minimum_input_delay:', minimum_input_delay)
    print('* inputs_maximum_delays:', inputs_maximum_delays)
    print('* ouput_maximum_delay:', ouput_maximum_delay)
    print('* slice_size:', slice_size)  # size of the time slice (default: 24) usually corresponding to one day
    print('* minimum_slices:', minimum_slices)  # the initial number of time slices used to learn parameters. If too small, it will generate a singular matrix error
    print('* slice_memory:', slice_memory)  # None for no slice memory limit

    linear_regression = buildingenergy.linreg.LinearRegression(input_labels=input_variable_names, output_label=output_variable_name, minimum_input_delay=minimum_input_delay, inputs_maximum_delays=inputs_maximum_delays, output_maximum_delay=ouput_maximum_delay, offset=offset)

    sliding_inputs = [data_container.data[data_label] for data_label in input_variable_names]
    sliding_output = data_container.data[output_variable_name]
    output_estimated_sliding = linear_regression.sliding(sliding_inputs, sliding_output, time_slice_size=slice_size, minimum_time_slices=minimum_slices, time_slice_memory=slice_memory)
    data_container.add_external_variable('%s_estimated' % output_variable_name, output_estimated_sliding)

    plot_saver_sliding = buildingenergy.data.PlotSaver(data_container)
    plot_saver_sliding.time_plot(['%s' % output_variable_name, '%s_estimated' % output_variable_name], 'sliding/output_sliding')
    print('* Estimated output at sliding')
    print('![Sliding output](output_sliding.png)')
    linear_regression.error_analysis(sliding_inputs, sliding_output, output_estimated_sliding, folder_name=folder_name)

    sys.stdout.close()
    sys.stdout = original
    return linear_regression

if __name__ == '__main__':

    data_container = h358measurements_old.DataContainer('h358data_winter2015-2016.csv', skiprows=0, nrows=None)

    ######## tuning parameter zone #######
    offset = True
    minimum_input_delay = 0
    inputs_maximum_delays = (2, 0, 0, 0, 2, 1, 1)
    ouput_maximum_delay = 4
    ######################################

    arx_estimation('Toffice_reference', ('Tout', 'window_opening', 'door_opening', 'total_electric_power', 'phi_sun', 'dT_heat', 'occupancy'), data_container, offset=True, minimum_input_delay=0, inputs_maximum_delays=(2, 0, 0, 0, 2, 1, 1), ouput_maximum_delay=4)

    # input_labels = ('corridor_CO2_concentration', 'window_opening', 'door_opening', 'occupancy')
    # output_label = 'office_CO2_concentration'