"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""

import matplotlib.pyplot as plt

from buildingenergy.building import InterfaceType, Site, ComponentInterface

Site.library.store('concrete', 'thermal', 269)
Site.library.store('gypsum', 'thermal', 265)
Site.library.store('wood_floor', 'thermal', 264)
Site.library.store('tile', 'thermal', 236)
Site.library.store('glass_wood', 'thermal', 261)
Site.library.store('glass', 'thermal', 267)
Site.library.store('plaster', 'thermal', 265)
Site.library.store('foam', 'thermal', 260)
Site.library.store('polystyrene', 'thermal', 145)
Site.library.store('brick', 'thermal', 268)
Site.library.store('wood', 'thermal', 277)
Site.library.store('air', 'thermal', 259)
Site.library.store('usual', 'thermal', 278)


site = Site('office', 'corridor', 'downstairs')

# corridor wall
door_surface = 80e-2 * 200e-2
door = site.add_layered_interface('office', 'corridor', InterfaceType.DOOR, door_surface)
door.add_layer('wood', 5e-3)
door.add_layer('air', 15e-3)
door.add_layer('wood', 5e-3)

glass_surface = 100e-2 * 100e-2
glass = site.add_layered_interface('office', 'corridor', InterfaceType.GLAZING, glass_surface)
glass.add_layer('glass', 4e-3)

internal_wall_thickness = 13e-3 + 34e-3 + 13e-3
cupboard_corridor_surface = (185e-2 + internal_wall_thickness + 34e-2 + 20e-3) * 2.5
corridor_wall_surface = (408e-2 + 406e-2 + internal_wall_thickness) * 2.5 - door_surface - glass_surface - cupboard_corridor_surface

cupboard = site.add_layered_interface('office', 'corridor', InterfaceType.WALL, cupboard_corridor_surface)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('foam', 34e-3)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('air', 50e-2 - 20e-3)
cupboard.add_layer('wood', 20e-3)

plain_corridor_wall = site.add_layered_interface('office', 'corridor', InterfaceType.WALL, corridor_wall_surface)
plain_corridor_wall.add_layer('plaster', 13e-3)
plain_corridor_wall.add_layer('foam', 34e-3)
plain_corridor_wall.add_layer('plaster', 13e-3)

# outdoor wall
west_glass_surface = 2 * 130e-2 * 52e-2 + 27e-2 * 52e-2 + 72e-2 * 52e-2
east_glass_surface = 36e-2 * 56e-2
windows_surface = west_glass_surface + east_glass_surface
no_cavity_surface = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface
cavity_surface = 315e-2 * 2.5 - west_glass_surface

windows = site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, windows_surface)
windows.add_layer('glass', 4e-3)
windows.add_layer('air', 12e-3)
windows.add_layer('glass', 4e-3)

plain_wall = site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, no_cavity_surface)
plain_wall.add_layer('concrete', 30e-2)

cavity_wall = site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, cavity_surface)
cavity_wall.add_layer('concrete', 30e-2)
cavity_wall.add_layer('air', 34e-2)
cavity_wall.add_layer('wood', 20e-3)

# slab
slab_effective_thickness = 11.9e-2
slab_surface = (309e-2 + 20e-3 + 34e-2) * (406e-2 + internal_wall_thickness) + 408e-2 * (273e-2 - 60e-2) - 315e-2 * (34e-2 + 20e-3) - (185e-3 + internal_wall_thickness) * 50e-2
slab = site.add_layered_interface('office', 'downstairs', InterfaceType.WALL, slab_surface)
slab.add_layer('concrete', slab_effective_thickness)
slab.add_layer('air', 20e-2)
slab.add_layer('polystyrene', 7e-3)
bridge: ComponentInterface = site.add_component_interface('office', 'outdoor', InterfaceType.BRIDGE, 0.5 * 0.99, 685e-2)  # ThBAT booklet 5, 3.1.1.2, 22B)

print(site)

site.simulated_zone('office', 56)
site.connect_airflow('outdoor', 'office', 1)
#site.connect_airflow('office', 'outdoor', 1)
site.connect_airflow( 'office', 'corridor', 1)
#site.connect_airflow( 'corridor', 'office', 1)
site.connect_airflow('corridor', 'outdoor', 1)
#site.draw_airflow_net()

thermal_model, CO2_model = site.make(order=None, air_flows={'Qoutdoor': 12.6/3600, 'Qcorridor': 12.6/3600})
#site.print_state_model(thermal_model)
site.print_state_model(CO2_model)
#site.print_thermal_characteristics(thermal_model)
site.print_CO2_characteristics(CO2_model)

#thermal_model, CO2_model = site.make(order=3, Qoutdoor=12.6/3600)
state_model = site.assemble(thermal_model, CO2_model)
print(state_model)

site.draw_thermal_net()
site.draw_airflow_net()
plt.show()
