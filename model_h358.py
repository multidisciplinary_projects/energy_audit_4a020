"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

It is an instance of Model for the office H358
"""

from typing import Dict, List, Tuple, TypeVar

import matplotlib.pyplot as plt

import buildingenergy.model
from buildingenergy.building import InterfaceType, Site, StateModel
from buildingenergy.parameters import ParameterSet

Site.library.store('concrete', 'thermal', 269)
Site.library.store('gypsum', 'thermal', 265)
Site.library.store('wood_floor', 'thermal', 264)
Site.library.store('tile', 'thermal', 236)
Site.library.store('glass_wood', 'thermal', 261)
Site.library.store('glass', 'thermal', 267)
Site.library.store('plaster', 'thermal', 265)
Site.library.store('foam', 'thermal', 260)
Site.library.store('polystyrene', 'thermal', 145)
Site.library.store('brick', 'thermal', 268)
Site.library.store('wood', 'thermal', 277)
Site.library.store('air', 'thermal', 259)
Site.library.store('usual', 'thermal', 278)


class H358Model(buildingenergy.model.Model):

    def __init__(self, order: int, parameter_set: ParameterSet):
        super().__init__(order, parameter_set)
        self.param = parameter_set
        self.site = None

    def make_differential_state_model(self, parameter_set: ParameterSet) -> StateModel:
        """
        generate a continuous time state space model: 
        
            d/dt(X(t)) = A(n(t)) X(t) + B(n(t)) U(t)
            Y(t) = C(n(t)) X(t) + D(n(t)) U(t)

        :param parameter_set: set of the parameters, which can be invariant or time-varying (nonlinear inputs). 
        :type parameter_set: ParameterSet
        :param nli_values: _description_
        :type nli_values: Dict[str, float]
        :return: _description_
        :rtype: StateModel
        """
        self.site = Site('office', 'corridor', 'downstairs')
        # corridor wall
        door_surface = 80e-2 * 200e-2
        door = self.site.add_layered_interface('office', 'corridor', InterfaceType.DOOR, door_surface)
        door.add_layer('wood', 5e-3)
        door.add_layer('air', 15e-3)
        door.add_layer('wood', 5e-3)

        glass_surface = 100e-2 * 100e-2
        glass = self.site.add_layered_interface('office', 'corridor', InterfaceType.GLAZING, glass_surface)
        glass.add_layer('glass', 4e-3)

        internal_wall_thickness = 13e-3 + 34e-3 + 13e-3
        cupboard_corridor_surface = (185e-2 + internal_wall_thickness + parameter_set('foam_thickness') + 20e-3) * 2.5
        corridor_wall_surface = (408e-2 + 406e-2 + internal_wall_thickness) * 2.5 - door_surface - glass_surface - cupboard_corridor_surface

        cupboard = self.site.add_layered_interface('office', 'corridor', InterfaceType.WALL, cupboard_corridor_surface)
        cupboard.add_layer('wood', 20e-3)
        cupboard.add_layer('air', 50e-2 - 20e-3)
        cupboard.add_layer('plaster', 13e-3)
        cupboard.add_layer('foam', parameter_set('foam_thickness'))
        cupboard.add_layer('plaster', 13e-3)

        plain_corridor_wall = self.site.add_layered_interface('office', 'corridor', InterfaceType.WALL, corridor_wall_surface)
        plain_corridor_wall.add_layer('plaster', 13e-3)
        plain_corridor_wall.add_layer(parameter_set('foam_thickness'), 34e-3)
        plain_corridor_wall.add_layer('plaster', 13e-3)

        # outdoor wall
        west_glass_surface = 2 * 130e-2 * 52e-2 + 27e-2 * 52e-2 + 72e-2 * 52e-2
        east_glass_surface = 36e-2 * 56e-2
        windows_surface = west_glass_surface + east_glass_surface
        no_cavity_surface = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface
        cavity_surface = 315e-2 * 2.5 - west_glass_surface

        windows = self.site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, windows_surface)
        windows.add_layer('glass', 4e-3)
        windows.add_layer('air', 12e-3)
        windows.add_layer('glass', 4e-3)

        plain_wall = self.site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, no_cavity_surface)
        plain_wall.add_layer('concrete', 30e-2)

        cavity_wall = self.site.add_layered_interface('office', 'outdoor', InterfaceType.WALL, cavity_surface)
        cavity_wall.add_layer('wood', 20e-3)
        cavity_wall.add_layer('air', parameter_set('foam_thickness'))
        cavity_wall.add_layer('concrete', 30e-2)

        # slab
        slab_surface = (309e-2 + 20e-3 + parameter_set('foam_thickness')) * (406e-2 + internal_wall_thickness) + 408e-2 * (273e-2 - 60e-2) - 315e-2 * (parameter_set('foam_thickness') + 20e-3) - (185e-3 + internal_wall_thickness) * 50e-2
        slab = self.site.add_layered_interface('office', 'downstairs', InterfaceType.SLAB, slab_surface)
        slab.add_layer('concrete',  parameter_set('slab_effective_thickness'))
        slab.add_layer('air', 20e-2)
        slab.add_layer('polystyrene', 7e-3)
        #print(slab)
        
        self.site.add_component_interface('office', 'outdoor', InterfaceType.BRIDGE, parameter_set('psi_bridge'), 685e-2)  # ThBAT booklet 5, 3.1.1.2, 22B)

        self.site.simulated_zone('office', parameter_set('room_volume'))
        # self.site.connect_airflow('outdoor', 'office', .5)
        # self.site.connect_airflow('office', 'outdoor', .5)
        # self.site.connect_airflow('office', 'corridor', .5)
        # self.site.connect_airflow( 'corridor', 'office', .5)
        # self.site.connect_airflow('corridor', 'outdoor', 1)
        self.site.simulated_zone('office', 90)
        #self.site.simulated_zone('corridor', 40)
        self.site.connect_airflow('outdoor', 'office', 1)
        self.site.connect_airflow('office', 'outdoor', 1)
        #self.site.connect_airflow('downstairs', 'office', 0)
        self.site.connect_airflow('office', 'corridor', 1)
        self.site.connect_airflow('corridor', 'office', 1)
        #self.site.connect_airflow('corridor', 'outdoor', 1)
        #self.site.connect_airflow('office', 'downstairs', 0)
        thermal_differential_state_model, CO2_differential_state_model = self.site.make(order=self.order, air_flows={'Qcorridor': 10/3600, 'Qoutdoor': 10/3600})
        #print(CO2_differential_state_model)
        global_differential_state_model = self.site.assemble(thermal_differential_state_model, CO2_differential_state_model)
        return global_differential_state_model


if __name__ == '__main__':

    parameter_set = ParameterSet()
    parameter_set.add_time_varying('Qoutdoor', 12/3600, 2/3600, 200/3600)
    parameter_set.add_invariant('Qoutdoor_window', 50/3600, 2/3600, 2000/3600)
    parameter_set.add_invariant('Qoutdoor_door', 50/3600, 2/3600, 2000/3600)
    parameter_set.add_time_varying('Qcorridor', 12/3600, 2/3600, 200/3600)
    parameter_set.add_invariant('Qcorridor_window', 50/3600, 2/3600, 2000/3600)
    parameter_set.add_invariant('Qcorridor_door', 50/3600, 2/3600, 2000/3600)
    parameter_set.add_invariant('slab_effective_thickness', 12e-2, 3e-2, 20e-2)
    parameter_set.add_invariant('room_volume', 56, 45, 70)
    parameter_set.add_invariant('psi_bridge', 0.5 * 0.99, 0.0 * 0.99, 0.5 * 5)
    parameter_set.add_invariant('foam_thickness', 34e-3, 1034e-3, 50e-3)
    
    model = H358Model(order=1, parameter_set=parameter_set)
    model.display_variables()
    model.make_differential_state_model(parameter_set)
    CO2_state_model = model.site.CO2_state_model({'Qcorridor': 2/3600, 'Qoutdoor': 2/3600})
    model.site.print_CO2_characteristics(CO2_state_model)
    model.site.draw_airflow_net()
    plt.show()